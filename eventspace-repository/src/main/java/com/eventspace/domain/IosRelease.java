package com.eventspace.domain;

import com.eventspace.dto.VersionDetailsDto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ios_release")
public class IosRelease implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "version", nullable = false)
    private String version;

    @Column(name = "update_required", nullable = false)
    private Integer updateRequired;

    @Column(name = "release_note", nullable = false)
    private String releaseNote;


    /** The date created. */
    @Column(name="created_at", nullable= false)
    private Date createdDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getUpdateRequired() {
        return updateRequired;
    }

    public void setUpdateRequired(Integer updateRequired) {
        this.updateRequired = updateRequired;
    }

    public String getReleaseNote() {
        return releaseNote;
    }

    public void setReleaseNote(String releaseNote) {
        this.releaseNote = releaseNote;
    }

    @Override
    public VersionDetailsDto build() {
        VersionDetailsDto versionDetailsDto=new VersionDetailsDto();
        versionDetailsDto.setId(id);
        versionDetailsDto.setVersion(version);
        versionDetailsDto.setUpdateRequired(updateRequired);
        versionDetailsDto.setReleaseNote(releaseNote);
        versionDetailsDto.setCreatedDate(createdDate.toString());

        return versionDetailsDto;
    }
}
