/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;

import com.eventspace.dto.PassWordResetDto;
import com.eventspace.dto.UserContext;
import com.eventspace.dto.UserPasswordResetDto;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.CommonService;
import com.eventspace.service.PasswordService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * The Class PasswordController.
 */
@RestController
@RequestMapping(value = "/api")
public class PasswordController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(PasswordController.class);

    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    /**
     * The password service.
     */
    @Autowired
    private PasswordService passwordService;

    @Autowired
    private CommonService commonService;


    /**
     * Forget password.
     *
     * @param email the email
     * @return the boolean
     */
    @GetMapping(value = "/forgetPassword")
    public Boolean forgetPassword(@RequestParam String email, final HttpServletRequest request) {
        LOGGER.info("forgetPassword method in PasswordController-----> get call");
        return passwordService.forgetPassword(email, commonService.getRequestServerName(request));
    }

    /**
     * Reset password.
     *
     * @param key              the key
     * @param passWordResetDto the password reset dto
     * @return the string
     */
    @PostMapping(value = "/reset")
    public Boolean resetPassword(@RequestParam String key, @RequestBody PassWordResetDto passWordResetDto,
                                 final HttpServletRequest request) {
        LOGGER.info("resetPassword method in PasswordController -----> get call");
        return passwordService.resetPassword(key, passWordResetDto);
    }

    @PostMapping(value = "/passwordReset")
    public Boolean resetThePassword(@RequestBody UserPasswordResetDto userPasswordResetDto,
                                    final HttpServletRequest request) throws Exception {
        LOGGER.info("resetThePassword method in PasswordController -----> get call");

        UserContext user = securityFacade.getUserContext(request);
        return passwordService.resetUserPassword(user.getUserId(), userPasswordResetDto);
    }

}
