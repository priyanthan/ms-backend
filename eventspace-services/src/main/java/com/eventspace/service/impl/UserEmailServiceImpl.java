package com.eventspace.service.impl;

import com.eventspace.email.dto.EmailMetaData;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.service.UserEmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserEmailServiceImpl implements UserEmailService {
	
	private static final Log logger = LogFactory.getLog(UserEmailServiceImpl.class);

	@Autowired
	private EventPublisher eventPublisher;

	@Override
	public void sendMail(String template,String toAddress,String subject,String receiverName) {
		logger.info("sendMail method ---> get called");
		eventPublisher.proceedEmailEvent(createEmailMetaData(template,toAddress,subject,receiverName));
	}

	private EmailMetaData createEmailMetaData(String template,String toAddress,String subject, String receiverName) {
		
		logger.info("createEmailMetaData method ---> get called");

		EmailMetaData emailMData = new EmailMetaData();
		emailMData.setToEmailAddresses(toAddress);
		emailMData.setVmFile(template);
		emailMData.setSubject(subject);
		Map<String, Object> data = new HashMap<>();
		data.put("receiverName", receiverName);
		emailMData.setData(data);
		return emailMData;

	}

}
