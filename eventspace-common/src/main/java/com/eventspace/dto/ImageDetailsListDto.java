package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImageDetailsListDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private List<ImageDetailsDto> imageDetails;

    public List<ImageDetailsDto> getImageDetails() {
        return imageDetails;
    }

    public void setImageDetails(List<ImageDetailsDto> imageDetails) {
        this.imageDetails = imageDetails;
    }
}
