package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

public class PayAdvanceEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(PayAdvanceEventActionServiceImpl.class);
    /**
     * @param bookingContextDto
     */
    public PayAdvanceEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    /**
     * @param stateContext
     */
    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action ADVANCE PAID OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.ADVANCE_PAYMENT_DONE.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.PAY_ADVANCE.value());
    }
}