package com.eventspace.dao.Impl;

import com.eventspace.dao.AmenityDao;
import com.eventspace.domain.Amenity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class AmenityDaoImpl extends BaseDaoImpl<Amenity> implements AmenityDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AmenityDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Amenity> getAllAmenities() {
        LOGGER.info("getAllAmenities method in AmenityDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<Amenity> amenitiesList = currentSession.createCriteria(Amenity.class).list();

        return amenitiesList;
    }

    @Override
    public List<Amenity> getLatestAmenities(Date lastUpdateDate) {
        LOGGER.info("getLatestAmenities method in AmenityDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<Amenity> amenitiesList = currentSession.createCriteria(Amenity.class)
                .add(Restrictions.gt("updatedDate", lastUpdateDate))
                .list();

        return amenitiesList;
    }
}
