package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

/**
 * Event space
 * Created by Aux-054 on 1/9/2017.
 */

public class CancelEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(CancelEventActionServiceImpl.class);


    public CancelEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action CANCEL OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.CANCELLED.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.CANCEL.value());
        bookingService.expirePayLater(booking.getId());
        bookingService.removeBookingFromSpaceUnavailablity(booking.getId());

    }
}
