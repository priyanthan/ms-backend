package com.eventspace.service.impl;

import com.eventspace.dao.*;
import com.eventspace.domain.*;
import com.eventspace.dto.*;
import com.eventspace.enumeration.*;
import com.eventspace.exception.EventspaceException;
import com.eventspace.service.CommonService;
import com.eventspace.service.EmailService;
import com.eventspace.service.SpaceService;
import com.eventspace.service.UserManagementService;
import com.eventspace.util.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * The Class SpaceServiceImpl.
 */
@Service
@Transactional
public class SpaceServiceImpl implements SpaceService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(SpaceServiceImpl.class);
    @Value("${cache.enable}")
    public Boolean enableCache;
    /**
     * The space dao.
     */
    @Autowired
    private SpaceDao spaceDao;
    /**
     * The amenity dao.
     */
    @Autowired
    private AmenityDao amenityDao;
    /**
     * The space amenity dao.
     */
    @Autowired
    private SpaceAmenityDao spaceAmenityDao;
    /**
     * The event type dao.
     */
    @Autowired
    private EventTypeDao eventTypeDao;
    /**
     * The space event type dao.
     */
    @Autowired
    private SpaceEventTypeDao spaceEventTypeDao;
    /**
     * The space extra amenity dao.
     */
    @Autowired
    private SpaceExtraAmenityDao spaceExtraAmenityDao;
    /**
     * The booking dao.
     */
    @Autowired
    private BookingDao bookingDao;
    /**
     * The review dao.
     */
    @Autowired
    private ReviewsDao reviewsDao;
    /**
     * The image dao.
     */
    @Autowired
    private ImageDao imageDao;
    @Autowired
    private MenuFilesDao menuFilesDao;
    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;
    @Autowired
    private RulesDao rulesDao;
    @Autowired
    private SpaceTypeDao spaceTypeDao;
    @Autowired
    private SpaceSeatingArrangementDao spaceSeatingArrangementDao;
    @Autowired
    private SeatingArrangementDao seatingArrangementDao;
    /**
     * The space unavailablity dao.
     */
    @Autowired
    private SpaceUnavailabilityDao spaceUnavailabilityDao;
    @Autowired
    private AutoPublishDao autoPublishDao;
    @Autowired
    private BlockAvailablityDao blockAvailablityDao;
    @Autowired
    private MeasurementUnitDao measurementUnitDao;
    @Autowired
    private FeaturedSpacesDao featuredSpacesDao;
    @Autowired
    private PerHourAvailablityDao perHourAvailablityDao;
    @Autowired
    private SpaceAdditionalDetailsDao spaceAdditionalDetailsDao;
    @Autowired
    private EmailService emailService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private ScheduleStartDao scheduleStartDao;
    @Autowired
    private DraftSpaceDao draftSpaceDao;
    @Autowired
    private ImageDetailsDao imageDetailsDao;
    @Autowired
    private PrioritySpacesDao prioritySpacesDao;
    @Autowired
    private SpaceOfWeekDao spaceOfWeekDao;
    @Autowired
    private SpaceContactPersonDao spaceContactPersonDao;
    @Autowired
    private UserManagementService userService;

    @Autowired
    private DiscountDetailsDao discountDetailsDao;

    @Value("${payment.gateway.rate}")
    public Double paymentGatewayRate;
    /**
     * create space
     *
     * @param spaceDetailDto the space detail dto
     * @param user           the user
     * @return success message map
     */
    @Override
    @Transactional
    public Map<String, Object> createSpace(SpaceDetailDto spaceDetailDto, UserContext user) {
        LOGGER.info("createSpace----->\n" + spaceDetailDto.getName());
        List<String> result = new ArrayList<>();

        Space space = new Space();
        if (spaceDetailDto.getLinkSpace() != null)
            spaceDetailDto.setLinkSpace(getLinkedSpace(spaceDetailDto.getLinkSpace()));
        space = Space.build(spaceDetailDto, space, user.getUserId());
        space.setCreatedAt(new Date());
        setAmenity(spaceDetailDto, space, result);
        setImages(spaceDetailDto, space);
        setEventTypes(space, spaceDetailDto, result);
        setRules(spaceDetailDto, space, result);
        setMenuFiles(spaceDetailDto, space);
        setSpaceType(spaceDetailDto, space);
        if (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()))
            setAvailablityBlocks(space, spaceDetailDto);
        if (space.getChargeType().equals(ChargeTypeEnum.HOUR_BASE.value()))
            setPerHourAvalility(space, spaceDetailDto);

        spaceDao.create(space);

        setSpaceAdditionalDetails(spaceDetailDto, space);

        SpaceContactPersonDto spaceContactPersonDto = new SpaceContactPersonDto();
        spaceContactPersonDto.setName(spaceDetailDto.getContactPersonName2());
        spaceContactPersonDto.setMobile(spaceDetailDto.getMobileNumber2());
        spaceContactPersonDto.setEmail(spaceDetailDto.getEmail2());

        setExtraContactPersons(space, spaceContactPersonDto);
        setExtraAmenity(spaceDetailDto, space, result);
        setSeatingArrangement(spaceDetailDto, space, result);
        space.setSearchAddress(setSearchAddress(space));
        if (spaceDetailDto.getImageDetails() != null && !spaceDetailDto.getImageDetails().isEmpty())
            saveImageDetails(spaceDetailDto.getImageDetails());
        spaceDao.merge(space);

        Map<String, Object> map = new HashMap<>();
        map.put("status", "successfully created");
        map.put("responseList", result);

        space = spaceDao.read(space.getId());

        List<SpaceContactPersonDto> hosts = emailService.getASpaceContactPersons(space.getId());
        for (SpaceContactPersonDto host : hosts) {
            emailService.sendSpaceListedSms(host.getMobile(), space.getName());

            if (space.getApproved() == BooleanEnum.TRUE.value()) {
                emailService.sendSpaceApprovedSms(host.getMobile(), space.getName());
                emailService.setDataOfSpacePublish(host.getName(), space.getName());
                emailService.sendMail(host.getEmail());
            }
        }

        return map;
    }

    /**
     * update space
     *
     * @param spaceDetailDto the space detail dto
     * @param userId         the user id
     * @param isAdmin        the is admin
     * @return success message map
     */
    @Override
    public Map<String, Object> updateSpace(int id, SpaceDetailDto spaceDetailDto, Integer userId, boolean isAdmin) {
        LOGGER.info("updateSpace ----->\t" + id);
        List<String> result = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        Space space = spaceDao.read(id);

        if (isAdmin || space.getUser().equals(userId)) {
            Optional.ofNullable(spaceDetailDto.getName()).ifPresent(space::setName);
            Optional.ofNullable(spaceDetailDto.getAddressLine1()).ifPresent(space::setAddressLine1);
            Optional.ofNullable(spaceDetailDto.getAddressLine2()).ifPresent(space::setAddressLine2);
            Optional.ofNullable(spaceDetailDto.getCancellationPolicy()).ifPresent(space::setCancellationPolicy);
            Optional.ofNullable(spaceDetailDto.getDescription()).ifPresent(space::setDescription);
            Optional.ofNullable(spaceDetailDto.getSize()).ifPresent(space::setSize);
            Optional.ofNullable(spaceDetailDto.getParticipantCount()).ifPresent(space::setParticipantCount);
            Optional.ofNullable(spaceDetailDto.getLongitude()).ifPresent(space::setLongitude);
            Optional.ofNullable(spaceDetailDto.getLatitude()).ifPresent(space::setLatitude);
            Optional.ofNullable(spaceDetailDto.getThumbnailImage()).ifPresent(space::setThumbnailImage);
            Optional.ofNullable(spaceDetailDto.getSecurityDeposit()).ifPresent(space::setSecurityDeposite);
            Optional.ofNullable(spaceDetailDto.getNoticePeriod()).ifPresent(space::setNoticePeriod);
            Optional.ofNullable(spaceDetailDto.getBufferTime()).ifPresent(space::setBufferTime);
            Optional.ofNullable(spaceDetailDto.getCalendarEventSize()).ifPresent(space::setCalendarEventSize);
            Optional.ofNullable(spaceDetailDto.getMeasurementUnit()).ifPresent(x -> space.setMeasurementUnit(x.getId()));
            Optional.ofNullable(spaceDetailDto.getMenuFileName()).ifPresent(space::setMenuFileName);
            Optional.ofNullable(spaceDetailDto.getAvailabilityMethod()).ifPresent(x -> space.setChargeType(AvailabilityMethods.valueOf(x).value()));
            Optional.ofNullable(spaceDetailDto.getBlockChargeType()).ifPresent(space::setBlockChargeType);
            Optional.ofNullable(spaceDetailDto.getMinParticipantCount()).ifPresent(space::setMinParticipantCount);
            Optional.ofNullable(spaceDetailDto.getParent()).ifPresent(space::setParent);
            Optional.ofNullable(spaceDetailDto.getLinkSpace()).ifPresent(space::setLinkSpace);
            Optional.ofNullable(spaceDetailDto.getReimbursableOption()).ifPresent(space::setReimbursableOption);
            space.setUpdatedAt(new Date());

            DateFormat df = new SimpleDateFormat(Constants.PATTERN_2);
            Optional.ofNullable(spaceDetailDto.getCalendarStart()).ifPresent(x -> {
                try {
                    space.setCalendarStart(df.parse(x));
                } catch (ParseException e) {
                    LOGGER.error("Calender Start date exception----->{}", e);
                }
            });
            Optional.ofNullable(spaceDetailDto.getCalendarEnd()).ifPresent(x -> {
                try {
                    space.setCalendarEnd(df.parse(x));
                } catch (ParseException e) {
                    LOGGER.error("Calender end date exception----->{}", e);
                }
            });

            setSpaceAdditionalDetails(spaceDetailDto, space);


            SpaceContactPersonDto spaceContactPersonDto = new SpaceContactPersonDto();
            Optional.ofNullable(spaceDetailDto.getContactPersonName2()).ifPresent(spaceContactPersonDto::setName);
            Optional.ofNullable(spaceDetailDto.getMobileNumber2()).ifPresent(spaceContactPersonDto::setMobile);
            Optional.ofNullable(spaceDetailDto.getEmail2()).ifPresent(spaceContactPersonDto::setEmail);
            Optional.ofNullable(spaceContactPersonDto).ifPresent(x -> setExtraContactPersons(space, spaceContactPersonDto));

            Optional.ofNullable(spaceDetailDto.getAmenity()).ifPresent(x -> setAmenity(spaceDetailDto, space, result));
            Optional.ofNullable(spaceDetailDto.getExtraAmenity()).ifPresent(x -> setExtraAmenity(spaceDetailDto, space, result));
            Optional.ofNullable(spaceDetailDto.getEventType()).ifPresent(x -> setEventTypes(space, spaceDetailDto, result));
            Optional.ofNullable(spaceDetailDto.getRules()).ifPresent(x -> setRules(spaceDetailDto, space, result));
            Optional.ofNullable(spaceDetailDto.getImages()).ifPresent(x -> setImages(spaceDetailDto, space));
            Optional.ofNullable(spaceDetailDto.getMenuFiles()).ifPresent(x -> setMenuFiles(spaceDetailDto, space));
            Optional.ofNullable(spaceDetailDto.getSpaceType()).ifPresent(x -> setSpaceType(spaceDetailDto, space));
            Optional.ofNullable(spaceDetailDto.getSeatingArrangements()).ifPresent(x -> setSeatingArrangement(spaceDetailDto, space, result));
            Optional.ofNullable(spaceDetailDto.getLinkSpace()).ifPresent(x -> spaceDetailDto.setLinkSpace(getLinkedSpace(x)));

            Optional.ofNullable(spaceDetailDto.getAvailability()).ifPresent(x -> {
                if (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()))
                    setAvailablityBlocks(space, spaceDetailDto);
                if (space.getChargeType().equals(ChargeTypeEnum.HOUR_BASE.value()))
                    setPerHourAvalility(space, spaceDetailDto);
            });
            spaceDao.merge(space);

            map.put("status", "sucessfully updated");
            map.put("responseList", result);
        } else
            map.put("status", "you are not authorized to update this space");
        return map;
    }

    /**
     * get all space
     *
     * @return list of space details dto
     */
    @Override
    public List<AdminSpaceDetailsDto> getAllSpaces() {
        LOGGER.info("getAllSpaces----->\t");
        return convertSpaces(spaceDao.getAllSpaces(BooleanEnum.TRUE));
    }

    /**
     * get space
     *
     * @param id the id
     * @return space detail dto
     */
    @Override
    public SpaceDetailDto getSpace(Integer id, Integer[] eventId,boolean isAdmin) {
        LOGGER.info("getSpace----->\t" + id);
        Space space = spaceDao.read(id);
        SpaceDetailDto spaceDto = getDataOfSpace(space,isAdmin);
        getExternalDataOfSpace(spaceDto, eventId);
        return spaceDto;
    }


    /**
     * delete space
     *
     * @param id the id
     * @return boolean
     */
    @Override
    public Boolean deleteSpace(int id) {
        LOGGER.info("deleteSpace -----> get\t" + id);
        if (spaceDao.read(id) != null) {
            return spaceDao.deleteSpace(id);
        } else
            return false;
    }


    /**
     * get filtered spaces
     *
     * @param filterDto the filter dto
     * @return list of space detail dto
     */
   /* @Override
    public List<SpaceDetailDto> getFilteredSpaces(FilterDto filterDto) {
        try {
            LOGGER.info("getFilteredSpaces service in SpaceServiceImpl -----> get call");

            List<SpaceDetailDto> spaceDtoList = new ArrayList<>();
            String[] participation = filterDto.getParticipation().split("-");
            String[] ratePerHour = filterDto.getBudget().split("-");
            Integer[] amenities = filterDto.getAmenities();
            Integer[] events = filterDto.getEvents();

            return convertSpaceEntityListToDtoList(spaceDao.filteredSpaces(participation, ratePerHour, amenities, events, filterDto.getLimit()));
        } catch (Exception ex) {
            LOGGER.error("getFilteredSpaces -> Exception : ", ex);
            throw ex;
        }
    }
*/
    /**
     * search by coordinates
     *
     * @param spaceSearchDto the space search dto
     * @return list of space detail dto
     */
   /* @Override
    public List<SpaceDetailDto> searchByCoordinates(SpaceSearchDto spaceSearchDto) {
        try {
            List<Space> spaceList = spaceDao.getCoordinateSpaces(spaceSearchDto.getLatitude(),
                    spaceSearchDto.getLongitude(), spaceSearchDto.getRadius());

            return convertSpaceEntityListToDtoList(spaceList);
        } catch (Exception ex) {
            LOGGER.error("searchByCoordinates -> Exception : ", ex);
            throw ex;
        }
    }*/


    /**
     * is space available wIth in selected time duration
     *
     * @param bookingDto the booking dto
     * @return boolean
     */
    public boolean isSpaceAvailableWIthInSelectedTimeDuration(BookingDto bookingDto) {

        LOGGER.info("isSpaceAvailableWIthInSelectedTimeDuration -----> get\t" + bookingDto.getSpace());
        //dummy user id 1
        Booking booking = Booking.build(bookingDto, 1);
        Space space = spaceDao.read(getLinkedSpace(bookingDto.getSpace()));

        if (space.getApproved() == BooleanEnum.FALSE.value())
            return false;

        boolean result = true;

        try {
            for (BookingSlots slot : booking.getBookingSlots()) {
                if (result) {
                    result = isSpaceHasAvalablity(space.getId(), slot) && slot.getToDate().getTime() > slot.getFromDate().getTime();
                    if (result)
                        result = spaceDao.isAvalableForBook(space.getId(), slot, space.getBufferTime(), space.getNoticePeriod(), commonService.getCurrentTime());
                } else
                    break;
            }
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }

    @Override
    public boolean isNotBlockTime(BookingDto bookingDto) {
        LOGGER.info("isNotBlockTime -----> get\t" + bookingDto.getSpace());
        Booking booking = Booking.build(bookingDto, 1);

        boolean isAvailable = true;
        for (BookingSlots slot : booking.getBookingSlots()) {
            if (isAvailable)
                isAvailable = spaceDao.isBlockTime(bookingDto.getSpace(), slot);
            else
                break;
        }

        return isAvailable;
    }

    @Override
    public boolean isSpaceHasAvalablity(Integer spaceId, BookingSlots bookingSlots) {
        Space space = spaceDao.read(spaceId);
        boolean result = false;
        if (space.getChargeType().equals(ChargeTypeEnum.HOUR_BASE.value())) {
            Integer day = bookingSlots.getToDate().getDay();
            if (day == 0)
                day = 7;

            PerHourAvailablity perHourAvailablity = perHourAvailablityDao.findTheAvailablity(spaceId, day, new Time(bookingSlots.getFromDate().getTime()), new Time(bookingSlots.getToDate().getTime()));
            if (perHourAvailablity != null)
                result = true;
        }
        if (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value())) {
            Integer day = bookingSlots.getToDate().getDay();
            if (day == 0)
                day = 7;
            if (day <= 5)
                day = 8;
            BlockAvailablity blockAvailablity = blockAvailablityDao.findTheAvailablity(spaceId, day, new Time(bookingSlots.getFromDate().getTime()), new Time(bookingSlots.getToDate().getTime()));
            if (blockAvailablity != null)
                result = true;
        }
        return result;
    }

    /**
     * get paginated spaces
     *
     * @param page the page
     * @return list of space details dto
     */
    @Override
    public Map<String, Object> getPaginatedSpaces(int page) {
        LOGGER.info("getPaginatedSpaces----->\t" + page);

        Map<String, Object> result = new HashMap<>();
        result.put("count", spaceDao.getAllSpaces(BooleanEnum.TRUE).size());
        result.put("spaces", convertSpaceEntityListToDtoList(spaceDao.getPaginatedSpaces(page)));
        return result;

    }

    /**
     * get admin panel spaces
     *
     * @param page the page
     * @return list of space details dto
     */
    @Override
    public List<AdminSpaceDetailsDto> getAdminSpaces(int page) {
        LOGGER.info("getAdminSpaces----->" + page);
        List<Space> spaces = spaceDao.getAdminSpaces(page);
        return convertSpaces(spaces);

    }

    private List<AdminSpaceDetailsDto> convertSpaces(List<Space> spaces){
        List<AdminSpaceDetailsDto> spaceDtoList = new ArrayList<>();
        if (spaces != null) {
            for (Space space : spaces) {
                AdminSpaceDetailsDto spaceDto = space.buildAdminSpaceDto();
                spaceDto.setRating(String.valueOf(findSpaceRating(space.getId()).intValue()));
                spaceDto.setHostName(userDao.read(space.getUser()).getName());
                spaceDtoList.add(spaceDto);
            }
        } else
            spaceDtoList = Collections.emptyList();

        return spaceDtoList;
    }

    /**
     * admin approve space
     *
     * @param spaceId the space id
     * @return integer
     */
    //approve to dis approve or dis approve to approve
    @Override
    public int adminApproveSpace(int spaceId) {
        LOGGER.info("adminApproveSpace----->\t" + spaceId);
        Space space = spaceDao.read(spaceId);
        //User user=userDao.read(space.getUser());
        space.setApproved(BooleanEnum.TRUE.value() - space.getApproved());
        spaceDao.update(space);

        if (space.getApproved() == BooleanEnum.TRUE.value() && space.getApprovedDate() == null) {
            List<SpaceContactPersonDto> hosts = emailService.getASpaceContactPersons(space.getId());
            for (SpaceContactPersonDto host : hosts) {
                emailService.sendSpaceApprovedSms(host.getMobile(), space.getName());
                emailService.setDataOfSpacePublish(host.getName(), space.getName());
                emailService.sendMail(host.getEmail());
            }
        }
        return space.getApproved();
    }

    /**
     * search spaces
     *
     * @param searchDto the search dto
     * @return list of space details dto
     */
   /* @Override
    public List<SpaceDetailDto> searchSpaces(SearchDto searchDto) {
        try {
            LOGGER.info("searchSpaces service in SpaceServiceImpl -----> get call");
            Integer[] events = {1, 2, 3, 4, 5, 6, 7};
            if (searchDto.getEvents() != null && searchDto.getEvents().length != 0)
                events = searchDto.getEvents();

                return convertSpaceEntityListToDtoList(spaceDao.getSearchSpaces(events, searchDto.getLocation()));
        } catch (Exception ex) {
            LOGGER.error("searchSpaces -> Exception : ", ex);
            throw ex;
        }

    }*/


    /**
     * get User space
     *
     * @return list of space details dto
     */
    @Override
    public List<SpaceDetailDto> getUserSpaces(Integer userId) {
        LOGGER.info("getUserSpaces  ----->\t" + userId);
        return convertSpaceEntityListToDtoList(spaceDao.getUserSpaces(userId));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> advanceSearch(AdvanceSearchDto advanceSearchDto, Integer page) {
        LOGGER.info("advanceSearch ----->" + page);
        Map<String, Object> spaces = spaceDao.advancedSearch(advanceSearchDto, false, page);
        Map<String, Object> count = spaceDao.advancedSearch(advanceSearchDto, true, null);

        if (spaces.get("spaces") != null) {
            Map<String, Object> result = new HashMap<>();
            result.put("count", count.get("count"));
            result.put("spaces", convertSpaceEntityToBatchSearchDto((List<Space>) spaces.get("spaces")));
           // result.put("spaces", convertSpaceEntityListToDtoList((List<Space>) spaces.get("spaces")));

            if (advanceSearchDto.getIsSocialMedia() != null && advanceSearchDto.getIsSocialMedia().equals(BooleanEnum.TRUE.value()))
                result.put("isSocialMedia", 1);
            return result;
        }
        return null;
    }


    @Override
    @Transactional
    public Integer enableAutoPublish(Boolean enable) {
        LOGGER.info("enableAutoPublish ----->\t" + enable);
        Integer firstRow = 1;
        AutoPublish autoPublish = autoPublishDao.read(firstRow);
        if (autoPublish.getEnable().equals(BooleanEnum.FALSE.value()) && enable) {
            autoPublish.setEnable(BooleanEnum.TRUE.value());
            autoPublishDao.update(autoPublish);
        } else if (autoPublish.getEnable().equals(BooleanEnum.TRUE.value()) && !enable) {
            autoPublish.setEnable(BooleanEnum.FALSE.value());
            autoPublishDao.update(autoPublish);
        }
        return autoPublish.getEnable();
    }


    /**
     * date to iso format
     *
     * @param date the date
     * @return string
     */
    private String dateToIsoFormate(Date date) {
        DateFormat df = new SimpleDateFormat(Constants.PATTERN_7);
        return df.format(date);
    }

    private String getSearchAddress(String lat, String lon) {
        LOGGER.info("getSearchAddress ----->\tlat:" + lat + "long:" + lon);
        RestTemplate restTemplate = new RestTemplate();
        List<MediaType> acceptableMediaTypes = new ArrayList<>();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<GoogleGeoCodeResponseDto> entity = new HttpEntity<>(headers);
        String url;
        ResponseEntity<GoogleGeoCodeResponseDto> result;
        try {
            url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
            url = url + lat + "," + lon + "&sensor=false";
            result = restTemplate.exchange(url, HttpMethod.GET, entity, GoogleGeoCodeResponseDto.class);
        } catch (Exception e) {
            throw new EventspaceException("not able to get the search address of the space from google api ");

        }
        return result.getBody().getSearchAddress();
    }

    /**
     * set event types
     *
     * @param dto    the space detail dto
     * @param space  the space
     * @param result message result
     */
    private void setEventTypes(Space space, SpaceDetailDto dto, List<String> result) {
        LOGGER.info("setEventTypes----->\t" + space.getName());
        Integer[] evntTypes = dto.getEventType();
        Set<EventType> eventSet = new HashSet<>();

        for (Integer evntType : evntTypes) {
            if (eventTypeDao.read(evntType) != null) {
                EventType type = eventTypeDao.read(evntType);
                eventSet.add(type);
            } else {
                result.add("eventType:" + evntType + " not avilable");
                LOGGER.info("there is no eventTypes for id : " + evntType);
            }
            space.setEventTypes(eventSet);

        }

    }

    /**
     * set amenity
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     * @param result         message result
     */
    private void setAmenity(SpaceDetailDto spaceDetailDto, Space space, List<String> result) {
        LOGGER.info("setAmenity----->\t" + space.getName());
        // convert the List of amenity id to set of amenity object
        Integer[] amenityset = spaceDetailDto.getAmenity();
        Set<Amenity> amenities = new HashSet<>(0);

        for (Integer anAmenityset : amenityset) {
            if (amenityDao.read(anAmenityset) != null) {
                Amenity amenity = amenityDao.read(anAmenityset);
                amenities.add(amenity);
                LOGGER.info("amenities" + amenity.getName() + "adding to the space : " + space.getName());
                // result.add("amenity:"+amenityset[i]+"successfully added");
            } else {
                result.add("amenity:" + anAmenityset + " not avilable");
                LOGGER.info("there is no amenities for id : " + anAmenityset);
            }
        }
        space.setAmenity(amenities);
    }

    /**
     * set rules
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     * @param result         message result
     */
    private void setRules(SpaceDetailDto spaceDetailDto, Space space, List<String> result) {
        LOGGER.info("setRules----->\t" + space.getName());
        // convert the List of amenity id to set of amenity object
        Integer[] ruleset = spaceDetailDto.getRules();
        Set<Rules> rules = new HashSet<>(0);

        for (Integer aRuleset : ruleset) {
            if (rulesDao.read(aRuleset) != null) {
                Rules rule = rulesDao.read(aRuleset);
                rules.add(rule);
                LOGGER.info("rule" + rule.getName() + "adding to the space : " + space.getName());
                // result.add("amenity:"+amenityset[i]+"successfully added");
            } else {
                result.add("rule:" + aRuleset + " not avilable");
                LOGGER.info("there is no rule for id : " + aRuleset);
            }
        }
        space.setRules(rules);
    }


    /**
     * set extra amenity
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     * @param result         message result
     */
    private void setExtraAmenity(SpaceDetailDto spaceDetailDto, Space space, List<String> result) {
        LOGGER.info("setExtraAmenity----->\t" + space.getName());
        Set<ExtraAmenityDto> extraAmenityDtoSet = spaceDetailDto.getExtraAmenity();
        Set<SpaceExtraAmenity> spaceExtraAmenities = new HashSet<>(0);
        Set<SpaceExtraAmenity> interSection = new HashSet<>(0);
        Set<SpaceExtraAmenity> existingExtraAmenities = space.getExtraAmenities();

        for (ExtraAmenityDto extraAmenityDto : extraAmenityDtoSet) {
            if (amenityDao.read(extraAmenityDto.getAmenityId()) != null) {
                Amenity amenity = amenityDao.read(extraAmenityDto.getAmenityId());
                SpaceExtraAmenity spaceExtraAmenity = SpaceExtraAmenity.build(space, amenity, extraAmenityDto);
                spaceExtraAmenities.add(spaceExtraAmenity);
                LOGGER.info("extra amenity :" + spaceExtraAmenity.getExtraAmenity().getName()
                        + " setting to the space : " + space.getName());

                // add only updated
                if (existingExtraAmenities != null) {
                    for (SpaceExtraAmenity existingExtraAmenity : existingExtraAmenities) {
                        if (existingExtraAmenity.getExtraRate().doubleValue() == spaceExtraAmenity.getExtraRate()
                                .doubleValue()
                                && existingExtraAmenity.getExtraAmenity() == spaceExtraAmenity.getExtraAmenity()) {
                            spaceExtraAmenities.remove(spaceExtraAmenity);
                            LOGGER.info("extra amenity : " + spaceExtraAmenity.getExtraAmenity().getName()
                                    + " already setted to the space : " + space.getName());
                        }
                        if (existingExtraAmenity.getExtraAmenity() == spaceExtraAmenity.getExtraAmenity()) {
                            interSection.add(existingExtraAmenity);
                        }
                    }
                }
            } else
                result.add("extra amenity:" + extraAmenityDto.getAmenityId() + " not avilable");
        }

        // remove based on update
        if (existingExtraAmenities != null) {
            for (SpaceExtraAmenity existingExtraAmenity : existingExtraAmenities) {
                if (!interSection.contains(existingExtraAmenity)) {
                    spaceExtraAmenityDao.removeExtraAmenity(existingExtraAmenity.getSpace().getId(),
                            existingExtraAmenity.getExtraAmenity().getId());
                    LOGGER.info("already existing extra amenity : " + existingExtraAmenity.getExtraAmenity().getName()
                            + "removing from space : " + space.getName());
                }
            }
        }

        space.setExtraAmenities(spaceExtraAmenities);
    }


    /**
     * set seating arrangement
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     * @param result         message result
     */
    private void setSeatingArrangement(SpaceDetailDto spaceDetailDto, Space space, List<String> result) {
        LOGGER.info("setSeatingArrangement----->\t" + space.getName());
        Set<SeatingArrangementDto> seatingArrangementDtoSet = spaceDetailDto.getSeatingArrangements();
        Set<SpaceSeatingArrangement> spaceSeatingArrangements = new HashSet<>(0);
        Set<SpaceSeatingArrangement> interSection = new HashSet<>(0);
        Set<SpaceSeatingArrangement> existingSeatingArrangements = space.getSpaceSeatingArrangements();

        for (SeatingArrangementDto seatingArrangementDto : seatingArrangementDtoSet) {
            if (seatingArrangementDao.read(seatingArrangementDto.getId()) != null) {
                SeatingArrangement seatingArrangement = seatingArrangementDao.read(seatingArrangementDto.getId());
                SpaceSeatingArrangement spaceSeatingArrangement = SpaceSeatingArrangement.build(space, seatingArrangement, seatingArrangementDto);
                spaceSeatingArrangements.add(spaceSeatingArrangement);
                LOGGER.info("seating arrangement :" + seatingArrangement.getName()
                        + " setting to the space : " + space.getName());

                // add only updated
                if (existingSeatingArrangements != null) {
                    for (SpaceSeatingArrangement existingSeatingArrangement : existingSeatingArrangements) {
                        if (Objects.equals(existingSeatingArrangement.getParticipantCount(), spaceSeatingArrangement.getParticipantCount())
                                && existingSeatingArrangement.getSeatingArrangement() == spaceSeatingArrangement.getSeatingArrangement()) {
                            spaceSeatingArrangements.remove(spaceSeatingArrangement);
                            LOGGER.info("seating arrangement : " + seatingArrangement.getName()
                                    + " already setted to the space : " + space.getName());
                        }
                        if (existingSeatingArrangement.getSeatingArrangement() == spaceSeatingArrangement.getSeatingArrangement()) {
                            interSection.add(existingSeatingArrangement);
                        }
                    }
                }
            } else
                result.add("extra amenity:" + seatingArrangementDto.getId() + " not avilable");
        }

        // remove based on update
        if (existingSeatingArrangements != null) {
            for (SpaceSeatingArrangement existingSeatingArrangement : existingSeatingArrangements) {
                if (!interSection.contains(existingSeatingArrangement)) {
                    spaceSeatingArrangementDao.removeSeatingArrangement(existingSeatingArrangement.getSpace().getId(),
                            existingSeatingArrangement.getSeatingArrangement().getId());
                    LOGGER.info("already existing extra amenity : " + existingSeatingArrangement.getSeatingArrangement().getId()
                            + "removing from space : " + space.getName());
                }
            }
        }

        space.setSpaceSeatingArrangements(spaceSeatingArrangements);
    }

    /**
     * set image
     *
     * @param spaceDetailDto the space detail dto
     * @param space          the space
     */
    private void setImages(SpaceDetailDto spaceDetailDto, Space space) {
        LOGGER.info("setImages ----->\t" + space.getName());
        Set<String> imageUrlSet = spaceDetailDto.getImages();
        Set<Image> images = new HashSet<>(0);
        Set<Image> interSection = new HashSet<>(0);
        Set<Image> existingImages = space.getImages();

        for (String imageurl : imageUrlSet) {
            Image image = Image.build(space, imageurl);
            images.add(image);
            LOGGER.info("image url :" + image.getUrl() + " setting to the space : " + space.getName());

            if (existingImages != null) {
                for (Image existingImage : existingImages) {
                    if (Objects.equals(existingImage.getUrl(), image.getUrl())) {
                        images.remove(image);
                        interSection.add(existingImage);
                        LOGGER.info(
                                "image url :" + image.getUrl() + " already setted to the space : " + space.getName());
                    }
                }
            }
        }
        if (existingImages != null) {
            for (Image existingImage : existingImages) {
                if (!interSection.contains(existingImage)) {
                    imageDao.removeImage(existingImage.getId());
                    LOGGER.info("already existing image url :" + existingImage.getUrl() + " removing from the space : "
                            + space.getName());
                }
            }
        }

        if (!images.isEmpty())
            space.setImages(images);
    }

    private void setMenuFiles(SpaceDetailDto spaceDetailDto, Space space) {
        LOGGER.info("setMenuFiles ----->\t" + space.getName());
        Set<MenuFiles> menuFiles = new HashSet<>();
        Set<MenuFileDto> menuFileDtoSet = spaceDetailDto.getMenuFiles();

        if (menuFileDtoSet != null && !menuFileDtoSet.isEmpty()) {
            for (MenuFileDto menuFileDto : menuFileDtoSet) {
                MenuFiles menuFile = MenuFiles.build(menuFileDto);

                if (menuFilesDao.readByUrl(menuFile.getUrl()) == null)
                    menuFilesDao.create(menuFile);
                else
                    menuFile = menuFilesDao.readByUrl(menuFile.getUrl());
                menuFiles.add(menuFile);
            }

            space.setMenuFiles(menuFiles);
        }

    }

    private void setSpaceType(SpaceDetailDto spaceDetailDto, Space space) {
        LOGGER.info("setMenuFiles ----->\t" + space.getName());
        Set<SpaceType> spaceTypes = new HashSet<>();
        Set<CommonDto> spaceTypeeDtoSet = spaceDetailDto.getSpaceType();

        if (spaceTypeeDtoSet != null && !spaceTypeeDtoSet.isEmpty()) {
            for (CommonDto spaceTypeDto : spaceTypeeDtoSet) {
                SpaceType spaceType = spaceTypeDao.read(spaceTypeDto.getId());
                if (spaceType != null)
                    spaceTypes.add(spaceType);
            }
            space.setSpaceTypes(spaceTypes);
        }

    }

    private void setImages2(SpaceDetailDto spaceDetailDto, Space space) {
        LOGGER.info("setting images to the space : " + space.getName());
        Set<MenuFiles> menuFiles = new HashSet<>();
        Set<MenuFileDto> menuFileDtoSet = spaceDetailDto.getMenuFiles();
        Set<MenuFiles> interSection = new HashSet<>(0);
        Set<MenuFiles> existings = space.getMenuFiles();

        for (MenuFileDto menuFiles1 : menuFileDtoSet) {
            MenuFiles menuFile = MenuFiles.build(menuFiles1);
            menuFiles.add(menuFile);
            LOGGER.info("menu url :" + menuFile.getUrl() + " setting to the space : " + space.getName());

            if (existings != null) {
                for (MenuFiles existing : existings) {
                    if (Objects.equals(existing.getUrl(), menuFile.getUrl())) {
                        menuFiles.remove(menuFile);
                        interSection.add(existing);
                        LOGGER.info(
                                "menu url :" + menuFile.getUrl() + " already setted to the space : " + space.getName());
                    }
                }

                for (MenuFiles existing : existings) {
                    if (!interSection.contains(existing)) {
                        imageDao.removeImage(existing.getId());
                        LOGGER.info("already existing image url :" + menuFile.getUrl() + " removing from the space : "
                                + space.getName());
                    }
                }
            }
        }
        space.setMenuFiles(menuFiles);
    }

    private void setAvailablityBlocks(Space space, SpaceDetailDto spaceDetailDto) {
        LOGGER.info("setAvailablityBlocks ----->\t" + space.getName());
        Integer avarageReate = 0;
        Integer hours = 0;
        Integer isWeekDay = 1;
        Set<AvailablityDetailsDto> availablityBlocksSet = spaceDetailDto.getAvailability();
        Set<BlockAvailablity> blockAvailablityBlocks = new HashSet<>(0);
        Set<BlockAvailablity> interSection = new HashSet<>(0);
        Set<BlockAvailablity> existing = space.getBlockAvailablityBlocks();
        Double min = 0.0;

        for (AvailablityDetailsDto availablityDetailsDto : availablityBlocksSet) {
            if (min.equals(0.0))
                min = availablityDetailsDto.getCharge();
            if (availablityDetailsDto.getCharge() < min)
                min = availablityDetailsDto.getCharge();
            if (availablityDetailsDto.getDay().equals(8))
                isWeekDay = 5;
            else
                isWeekDay = 1;
            if (space.getBlockChargeType() == null || (space.getBlockChargeType() != null && space.getBlockChargeType().equals(BlockChargeTypeEnum.SPACE_ONLY_CHARGE.value())))
                avarageReate = avarageReate + (availablityDetailsDto.getCharge().intValue() * isWeekDay);
            else {
                avarageReate = avarageReate + (availablityDetailsDto.getCharge().intValue() * isWeekDay * space.getMinParticipantCount());
                min = min * space.getMinParticipantCount();
            }
            BlockAvailablity availablityBlock = BlockAvailablity.build(space, availablityDetailsDto);

            Set<MenuFiles> menuFiles = new HashSet<>();
            if (availablityDetailsDto.getMenuFiles() != null) {
                for (MenuFileDto menuFileDto : availablityDetailsDto.getMenuFiles()) {
                    MenuFiles menuFile = menuFilesDao.readByUrl(menuFileDto.getUrl());
                    menuFiles.add(menuFile);
                }
                availablityBlock.setMenuFiles(menuFiles);
            }
            blockAvailablityBlocks.add(availablityBlock);


            hours = hours + ((int) (availablityBlock.getTo().getTime() - availablityBlock.getFrom().getTime()) / (3600 * 1000) * isWeekDay);

            if (existing != null) {
                for (BlockAvailablity existingAvailablityBlock : existing) {
                    if (existingAvailablityBlock.getFrom().equals(availablityBlock.getFrom())
                            && existingAvailablityBlock.getTo().equals(availablityBlock.getTo())
                            && existingAvailablityBlock.getDay().equals(availablityBlock.getDay())
                            && existingAvailablityBlock.getCharge().equals(availablityBlock.getCharge())
                            && existingAvailablityBlock.getMenuFiles().equals(availablityBlock.getMenuFiles())
                    ) {
                        blockAvailablityBlocks.remove(availablityBlock);
                        interSection.add(existingAvailablityBlock);
                    }
                }

                for (BlockAvailablity existingAvailablityBlock : existing) {
                    if (!interSection.contains(existingAvailablityBlock)) {
                        existingAvailablityBlock.setActive(BooleanEnum.FALSE.value());
                        blockAvailablityDao.update(existingAvailablityBlock);
                    } else {
                        existingAvailablityBlock.setActive(BooleanEnum.TRUE.value());
                        blockAvailablityDao.update(existingAvailablityBlock);
                    }

                }

            }
        }
        space.setBlockAvailablityBlocks(blockAvailablityBlocks);
        space.setRatePerHour(min);

    }

    private void setPerHourAvalility(Space space, SpaceDetailDto spaceDetailDto) {
        LOGGER.info("setPerHourAvalility  ----->\t" + space.getName());
        Integer avarageReate = 0;
        Set<AvailablityDetailsDto> availablitySet = spaceDetailDto.getAvailability();
        Set<PerHourAvailablity> perHourAvailablities = new HashSet<>(0);
        Set<PerHourAvailablity> interSection = new HashSet<>(0);
        Set<PerHourAvailablity> existing = space.getPerHourAvailablity();
        Double min = 0.0;

        for (AvailablityDetailsDto availablityDetailsDto : availablitySet) {
            if (availablityDetailsDto.getIsAvailable().equals(BooleanEnum.TRUE.value())) {
                if (min.equals(0.0))
                    min = availablityDetailsDto.getCharge();
                if (availablityDetailsDto.getCharge() < min)
                    min = availablityDetailsDto.getCharge();
                avarageReate = avarageReate + availablityDetailsDto.getCharge().intValue();

                PerHourAvailablity perHourAvailablity = PerHourAvailablity.build(space, availablityDetailsDto);
                perHourAvailablities.add(perHourAvailablity);

                if (existing != null) {
                    for (PerHourAvailablity existingPerHourAvailablity : existing) {
                        if (existingPerHourAvailablity.getFrom().equals(perHourAvailablity.getFrom())
                                && existingPerHourAvailablity.getTo().equals(perHourAvailablity.getTo())
                                && existingPerHourAvailablity.getDay().equals(perHourAvailablity.getDay())
                                && existingPerHourAvailablity.getCharge().equals(perHourAvailablity.getCharge())
                        ) {
                            perHourAvailablities.remove(perHourAvailablity);
                            interSection.add(existingPerHourAvailablity);
                        }
                    }

                    for (PerHourAvailablity existingPerHourAvailablity : existing) {
                        if (!interSection.contains(existingPerHourAvailablity)) {
                            existingPerHourAvailablity.setActive(BooleanEnum.FALSE.value());
                            perHourAvailablityDao.update(existingPerHourAvailablity);
                        } else {
                            existingPerHourAvailablity.setActive(BooleanEnum.TRUE.value());
                            perHourAvailablityDao.update(existingPerHourAvailablity);
                        }
                    }
                }


            }
        }
        space.setPerHourAvailablity(perHourAvailablities);
        space.setRatePerHour(min);

    }

    private SearchAddress setSearchAddress(Space space) {
        SearchAddress searchAddress = new SearchAddress();
        searchAddress.setAddress(getSearchAddress(space.getLatitude(), space.getLongitude()));
        searchAddress.setSpace(space.getId());

        return searchAddress;
    }

    @Override
    public List<SpaceDetailDto> getFeaturedSpaces() {
        LOGGER.info("getFeaturedSpaces----->");
        return convertSpaceEntityListToDtoList(spaceDao.getFeaturedSpaces());
    }

    @Override
    public List<FeaturedSpacesDto> getAdminFeaturedSpaces() {
        return convertFeatureSpacesEntityToDtoList(featuredSpacesDao.getAll());
    }

    private List<FeaturedSpacesDto> convertFeatureSpacesEntityToDtoList(List<FeaturedSpaces> featuredSpaces) {
        List<FeaturedSpacesDto> featuredSpacesDtoList = new ArrayList<>();
        if (featuredSpaces != null) {
            for (FeaturedSpaces featuredSpace : featuredSpaces) {
                FeaturedSpacesDto featuredSpacesDto = featuredSpace.build();
                featuredSpacesDtoList.add(featuredSpacesDto);
            }
            return featuredSpacesDtoList;
        } else
            return Collections.emptyList();
    }

    @Override
    public void unFeaturedSpace(Integer spaceId) {
        FeaturedSpaces featuredSpace = featuredSpacesDao.read(spaceId);
        featuredSpace.setFeatured(0);
        featuredSpacesDao.update(featuredSpace);
    }

    @Override
    public void saveFeaturedSpaces(FeaturedSpacesDto featuredSpacesDto) {
        FeaturedSpaces featuredSpace = FeaturedSpaces.build(featuredSpacesDto);
        featuredSpacesDao.create(featuredSpace);
    }

    private SpaceOwnerDto getSpaceOwnerDetails(Integer userId) {
        User spaceOwner = userDao.read(userId);
        return spaceOwner.buildSpaceOwnerDto();
    }

    private List<AdminSpaceDetailsDto> getSpaceChilds(Integer spaceId) {
        List<Space> childs = spaceDao.getChilds(spaceId);
        List<AdminSpaceDetailsDto> childDto = new ArrayList<>();

        for (Space child : childs)
            childDto.add(child.buildChildSpaceDto());

        return childDto;
    }

    private List<AdminSpaceDetailsDto> getLinkedSpaces(Integer spaceId) {
        Space originSpace = spaceDao.read(getLinkedSpace(spaceId));
        List<Space> linkedSpaces = spaceDao.getLinkedSpaces(originSpace.getId());
        List<AdminSpaceDetailsDto> linkedSpacesDto = new ArrayList<>();

        if (spaceId != originSpace.getId())
            linkedSpacesDto.add(originSpace.buildChildSpaceDto());

        for (Space space : linkedSpaces) {
            if (space.getId() != spaceId)
                linkedSpacesDto.add(space.buildChildSpaceDto());
        }
        return linkedSpacesDto;
    }

    /**
     * get space review details
     *
     * @param spaceId the space id
     * @return list of  review detail dto
     */
    @Override
    public List<ReviewDetailsDto> getSpaceReviewdetails(Integer spaceId) {
        List<ReviewDetailsDto> reviewDetails = new ArrayList<>();

        List<Booking> bookings = bookingDao.getBookingsBySpaceId(spaceId);
        for (Booking booking : bookings) {
            if (reviewsDao.checkReviewByBookingId(booking.getId())) {
                Reviews review = reviewsDao.getReviewByBookingId(booking.getId());
                User user = userDao.read(booking.getUser());

                ReviewDetailsDto reviewDetailsDto = review.build();
                reviewDetailsDto.setUserImg(user.getImageUrl());
                reviewDetailsDto.setUserName(user.getName());
                reviewDetails.add(reviewDetailsDto);
            }
        }
        return reviewDetails;
    }

    /**
     * find space rating
     *
     * @param id the id
     * @return rating
     */
    private Double findSpaceRating(int id) {
        return reviewsDao.getRatingBySpaceId(id);
    }

    /**
     * find similar spaces
     *
     * @param spaceId the space id
     * @return set of similar space detail dto
     */
    @SuppressWarnings("unchecked")
    public Set<SpaceDetailDto> findSimilarSpaces(Integer spaceId, Integer[] eventId, boolean isWeb) {
        LOGGER.info("\n===============\nfindSimilarSpaces ----->\t" + spaceId);
        Space space = spaceDao.read(spaceId);
        SpaceDetailDto spaceDto = space.build();

        //List<Space> spaces=new ArrayList<>();
        HashSet<Space> spaces = new HashSet<Space>();


        AdvanceSearchDto advanceSearchDto = new AdvanceSearchDto();
        if (eventId == null || eventId.length == 0)
            advanceSearchDto.setEvents(spaceDto.getEventType());
        else
            advanceSearchDto.setEvents(eventId);
        Map<String, Object> spacesMap = spaceDao.advancedSearch(advanceSearchDto, false, 0);
        if (spacesMap.get("spaces") != null)
            spaces.addAll((List<Space>) spacesMap.get("spaces"));

       /* advanceSearchDto.setEvents(null);
        advanceSearchDto.setAmenities(spaceDto.getAmenity());
        spacesMap= spaceDao.advancedSearch(advanceSearchDto,false,0);
        if (spacesMap.get("spaces") != null)
            spaces.addAll((List<Space>)spacesMap.get("spaces"));

        advanceSearchDto.setAmenities(null);
        advanceSearchDto.setRules(spaceDto.getRules());
        spacesMap= spaceDao.advancedSearch(advanceSearchDto,false,0);
        if (spacesMap.get("spaces") != null)
            spaces.addAll((List<Space>)spacesMap.get("spaces"));

        advanceSearchDto.setRules(null);
        advanceSearchDto.setParticipation(String.valueOf(spaceDto.getParticipantCount() * 0.7) + "-" + String.valueOf(spaceDto.getParticipantCount() * 1.3));
        spacesMap= spaceDao.advancedSearch(advanceSearchDto,false,0);
        if (spacesMap.get("spaces") != null)
            spaces.addAll((List<Space>)spacesMap.get("spaces"));

        advanceSearchDto.setParticipation(null);
        LocationSearchDto locationSearchDto=new LocationSearchDto();
        locationSearchDto.setLongitude(spaceDto.getLongitude());
        locationSearchDto.setLongitude(spaceDto.getLongitude());
        locationSearchDto.setAddress(spaceDto.getAddressLine1());
        advanceSearchDto.setLocation(locationSearchDto);
        spacesMap= spaceDao.advancedSearch(advanceSearchDto,false,0);
        if (spacesMap.get("spaces") != null)
            spaces.addAll((List<Space>)spacesMap.get("spaces"));*/

        if (spaces.isEmpty() || spaces.size() < 4) {
            spaces.addAll(spaceDao.getPaginatedSpaces(0));
        }

        spaces.add(space);
        spaces.remove(space);

        Set<SpaceDetailDto> spaceDtoSet = new LinkedHashSet<>();
        for (Space space1 : spaces) {
            SpaceDetailDto spaceDetailDto = space1.buildSmilarSpaceDto();
            spaceDetailDto.setRating(findSpaceRating(space1.getId()));
            if (!isWeb) {
                spaceDetailDto = getDataOfSpace(space1,false);
            }
            spaceDtoSet.add(spaceDetailDto);
        }


        LOGGER.info("\n===============\n");
        return spaceDtoSet;
    }

    /**
     * get upcoming paid bookings duration
     *
     * @param spaceId the space id
     * @return list
     */
    @Override
    @Transactional
    public List<Map> getUpcomingPaidBookingsDuration(Integer spaceId) {

        Space space = spaceDao.read(spaceId);
        /**
         if (space.getExternalApiCodes()!=null && !space.getExternalApiCodes().isEmpty()){
         ExternalApiCode apis= space.getExternalApiCodes().iterator().next();
         return callFutureBookedDatesExternalApi(apis);
         }
         */

        List<Map> durations = new ArrayList<>();
        if (spaceUnavailabilityDao.getUnavaliableDurations(getLinkedSpace(spaceId)) != null) {
            List<SpaceUnavailability> datas = spaceUnavailabilityDao.getUnavaliableDurations(getLinkedSpace(spaceId));

            for (SpaceUnavailability data : datas) {
                durations.add(setSpaceUnavailabilityDetails(data));
            }
            return durations;
        } else
            return Collections.emptyList();
    }


    private List<Map> getAllPaidBookingsDuration(Integer spaceId) {

        List<Map> durations = new ArrayList<>();
        if (spaceUnavailabilityDao.getAllUnavaliableDurations(getLinkedSpace(spaceId)) != null) {
            List<SpaceUnavailability> datas = spaceUnavailabilityDao.getAllUnavaliableDurations(getLinkedSpace(spaceId));
            for (SpaceUnavailability data : datas) {
                durations.add(setSpaceUnavailabilityDetails(data));
            }
            return durations;
        } else
            return Collections.emptyList();
    }

    private Map<String, Object> setSpaceUnavailabilityDetails(SpaceUnavailability data) {
        Map<String, Object> duration = new HashMap<>();
        duration.put("id", data.getId());
        if (data.getBookingId() != null)
            duration.put("bookingId", data.getBookingId());
        else
            duration.put("bookingId", 0);
        duration.put("from", dateToIsoFormate(data.getFromDate()));
        duration.put("to", dateToIsoFormate(data.getToDate()));
        if (data.getIsManual().equals(UnavailablityTypeEnum.CHILD_BLOCK_FOR_BOOKING.value()))
            data.setIsManual(UnavailablityTypeEnum.GUEST_BOOKING.value());
        duration.put("isManual", data.getIsManual());
        duration.put("title", data.getTitle());
        duration.put("note", data.getNote());
        duration.put("dateBookingMade", data.getDateBookingMade());
        duration.put("guestName", data.getGuestName());
        duration.put("guestEmail", data.getGuestEmail());
        duration.put("guestContactNumber", data.getGuestContactNumber());
        duration.put("noOfGuests", data.getNoOfGuests());
        if (data.getEventType() != null) {
            duration.put("eventTypeId", data.getEventType().getId());
            duration.put("eventTypeName", data.getEventType().getName());
        }
        duration.put("extrasRequested", data.getExtrasRequested());
        if (data.getSeatingArrangement() != null) {
            duration.put("seatingArrangementId", data.getSeatingArrangement().getId());
            duration.put("seatingArrangementName", data.getSeatingArrangement().getName());
            duration.put("seatingArrangementIcon", data.getSeatingArrangement().getIcon());
        }
        duration.put("note", data.getNote());
        duration.put("cost", data.getCost());

        return duration;
    }

    private List<SpaceDetailDto> convertSpaceEntityListToDtoList(List<Space> spaces) {
        List<SpaceDetailDto> spaceDtoList = new ArrayList<>();
        if (spaces != null) {
            for (Space space : spaces) {
                LOGGER.info("space :" + space.getId());
                SpaceDetailDto spaceDto = getDataOfSpace(space,false);
                spaceDtoList.add(spaceDto);
            }
            return spaceDtoList;
        } else
            return Collections.emptyList();
    }

    private List<SpaceDetailDto> convertSpaceEntityListToDtoListWithLimited(List<Space> spaces) {
        List<SpaceDetailDto> spaceDtoList = new ArrayList<>();
        if (spaces != null) {
            for (Space space : spaces) {
                LOGGER.info("space :" + space.getId());
                SpaceDetailDto spaceDto = space.buildSmilarSpaceDto();
                spaceDto.setRating(findSpaceRating(space.getId()));
                spaceDtoList.add(spaceDto);
            }
            return spaceDtoList;
        } else
            return Collections.emptyList();
    }

    private List<BatchSearchSpaceDto> convertSpaceEntityToBatchSearchDto(List<Space> spaces) {
        List<BatchSearchSpaceDto> spaceDtoList = new ArrayList<>();
        if (spaces != null) {
            for (Space space : spaces) {
                BatchSearchSpaceDto dto = new BatchSearchSpaceDto();
                dto.setId(space.getId());
                dto.setName(space.getName());
                dto.setAddressLine2(space.getAddressLine2());
                dto.setParticipantCount(space.getParticipant_count());
                dto.setThumbnailImage(space.getThumbnailImage());

                Double discount=findDiscountOfSpace(space);
                dto.setDiscount(discount.intValue());
                dto.setPreStaringFrom(space.getRatePerHour());
                dto.setRatePerHour(space.getRatePerHour()-discount);
                spaceDtoList.add(dto);
            }
            return spaceDtoList;
        }
        return Collections.emptyList();
    }

    private void setSpaceAdditionalDetails(SpaceDetailDto spaceDetailDto, Space space) {
        SpaceAdditionalDetails spaceAdditionalDetails = new SpaceAdditionalDetails();
        if (spaceAdditionalDetailsDao.read(space.getId()) != null)
            spaceAdditionalDetails = spaceAdditionalDetailsDao.read(space.getId());
        spaceAdditionalDetails.setSpace(space.getId());

        if (spaceDetailDto.getCommissionPercentage() != null)
            spaceAdditionalDetails.setCommissionPercentage(spaceDetailDto.getCommissionPercentage());
        else
            spaceAdditionalDetails.setCommissionPercentage(20);

        if (spaceDetailDto.getContactPersonName() != null)
            spaceAdditionalDetails.setContactPerson(spaceDetailDto.getContactPersonName());

        if (spaceDetailDto.getMobileNumber() != null)
            spaceAdditionalDetails.setMobilePhone(spaceDetailDto.getMobileNumber());

        if (spaceDetailDto.getCompanyName() != null)
            spaceAdditionalDetails.setCompanyName(spaceDetailDto.getCompanyName());

        if (spaceDetailDto.getCompanyPhone() != null)
            spaceAdditionalDetails.setCompanyPhone(spaceDetailDto.getCompanyPhone());

        if (spaceDetailDto.getAccountHolderName() != null)
            spaceAdditionalDetails.setAccountHolderName(spaceDetailDto.getAccountHolderName());

        if (spaceDetailDto.getAccountNumber() != null)
            spaceAdditionalDetails.setAccountNumber(spaceDetailDto.getAccountNumber());

        if (spaceDetailDto.getBank() != null)
            spaceAdditionalDetails.setBank(spaceDetailDto.getBank());

        if (spaceDetailDto.getBankBranch() != null)
            spaceAdditionalDetails.setBankBranch(spaceDetailDto.getBankBranch());

        if (spaceDetailDto.getHostLogo() != null)
            spaceAdditionalDetails.setHostLogo(spaceDetailDto.getHostLogo());


        space.setSpaceAdditionalDetails(spaceAdditionalDetails);
    }


    @Override
    public void addMiss(SpaceDetailDto spaceDetailDto) {
        Space space = spaceDao.read(spaceDetailDto.getId());
        //setSpaceAdditionalDetails(spaceDetailDto,space);
        if (spaceDetailDto.getThumbnailImage() != null)
            space.setThumbnailImage(spaceDetailDto.getThumbnailImage());
        if (spaceDetailDto.getImages() != null && !spaceDetailDto.getImages().isEmpty())
            setImages(spaceDetailDto, space);
        spaceDao.update(space);
    }

    @Override
    public void disapproveCalenderEndSpaces() {
        Date today = new Date();
        List<Space> spaces = spaceDao.getCalenderEndSpaces(today);
        for (Space space : spaces) {
            LOGGER.info("space :" + space.getId() + " going to disapprove ");
            User user = userDao.read(space.getUser());
            space.setApproved(0);
            spaceDao.update(space);
            emailService.setDataOfSpaceDeactivated(user.getName(), space.getId());
            List<SpaceContactPersonDto> hosts = emailService.getASpaceContactPersons(space.getId());
            for (SpaceContactPersonDto host : hosts)
                emailService.sendMail(host.getEmail());
        }

        today.setTime(today.getTime() + (7 * 24 * 60 * 60 * 1000));
        spaces = spaceDao.getCalenderEndingSpaces(today);
        for (Space space : spaces) {
            LOGGER.info("space :" + space.getId() + " going to end after 1 week ");
            User user = userDao.read(space.getUser());
            emailService.setDataOfUpadetCalenderEndDate(user.getName(), space.getId());
            List<SpaceContactPersonDto> hosts = emailService.getASpaceContactPersons(space.getId());
            for (SpaceContactPersonDto host : hosts)
                emailService.sendMail(host.getEmail());
        }
    }

    @Override
    public Integer getLinkedSpace(Integer spaceId) {
        Space space = spaceDao.read(spaceId);
        if (space.getLinkSpace() != null)
            spaceId = getLinkedSpace(space.getLinkSpace());
        return spaceId;
    }

    @Override
    public void setLinkedSpaces(Integer originSpace, Integer linkSpace) {
        LOGGER.info("setLinkedSpaces ----->\t" + originSpace);
        originSpace = getLinkedSpace(originSpace);
        Space space = spaceDao.read(linkSpace);
        space.setLinkSpace(originSpace);
        spaceDao.update(space);
    }

    @Override
    public void addChilds(AddChildsDto addChildsDto) {
        LOGGER.info("addChilds ----->\t" + addChildsDto.getOriginSpace());
        List<Space> childSpaces = spaceDao.getChilds(addChildsDto.getOriginSpace());
        List<Integer> newChilds = Arrays.asList(addChildsDto.getChilds());

        for (Space child : childSpaces) {
            if (newChilds.contains(child.getId()))
                newChilds.remove(child.getId());
            else {
                child.setParent(null);
                spaceDao.update(child);
            }
        }

        for (Integer child : newChilds) {
            Space space = spaceDao.read(getLinkedSpace(child));
            Space parent = spaceDao.read(getLinkedSpace(addChildsDto.getOriginSpace()));
            if (parent.getParent() != null)
                parent = spaceDao.read(parent.getParent());
            space.setParent(parent.getId());
            spaceDao.update(space);
        }
    }

    private SpaceDetailDto getDataOfSpace(Space space,boolean isAdmin) {
        SpaceDetailDto spaceDto = space.build();
        Double discount=findDiscountOfSpace(space);
        if(Optional.ofNullable(discount).isPresent() && !isAdmin){
            spaceDto.setRatePerHour(space.getRatePerHour()-discount);
            spaceDto.setDiscount(discount);

            spaceDto.getAvailability().stream().forEach(availablityDetailsDto -> {
                Double discountedCharge=availablityDetailsDto.getCharge()-spaceDto.getDiscount();
                availablityDetailsDto.setCharge(discountedCharge);
            });
        }

        spaceDto.setRating(findSpaceRating(space.getId()));
        spaceDto.setChildSpaces(getSpaceChilds(space.getId()));
        spaceDto.setLinkedSpaces(getLinkedSpaces(space.getId()));
        return spaceDto;
    }


    public Double findDiscountOfSpace(Space space){
        DiscountDetails discountDetails=discountDetailsDao.getCurrentDiscounts();
        Double response;

        if(Optional.ofNullable(discountDetails).isPresent()
                && space.getEventTypes().contains(discountDetails.getEventType())
                && Optional.ofNullable(space.getSpaceAdditionalDetails()).isPresent()
                && (space.getChargeType().equals(ChargeTypeEnum.HOUR_BASE.value()) ||
                (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value())
                        && space.getBlockChargeType().equals(BlockChargeTypeEnum.SPACE_ONLY_CHARGE.value())))){
            Double discount = space.getRatePerHour() * ((Optional.ofNullable(space.getSpaceAdditionalDetails().getCommissionPercentage()).orElse(20)) / 100.0);

            if (Optional.ofNullable(space.getSpaceAdditionalDetails().getAdvanceOnlyEnable()).isPresent()
                    && space.getSpaceAdditionalDetails().getAdvanceOnlyEnable().equals(BooleanEnum.TRUE.value())
                    && Optional.ofNullable(space.getSpaceAdditionalDetails().getDiscount()).isPresent()){
                response=space.getSpaceAdditionalDetails().getDiscount();
            }
            else {
                Double discountedStaringFrom = space.getRatePerHour() - discount;
                Double roundStartingFrom = discountedStaringFrom;
                if (discountedStaringFrom % 50 != 0) {
                    roundStartingFrom = discountedStaringFrom + (50 - discountedStaringFrom % 50);
                }
                response = space.getRatePerHour() - roundStartingFrom;
            }
            return response;
        }
        return 0.0;
    }

    private void getExternalDataOfSpace(SpaceDetailDto spaceDetailDto, Integer[] eventId) {
        spaceDetailDto.setSpaceOwnerDto(getSpaceOwnerDetails(spaceDetailDto.getUser()));
        spaceDetailDto.setReviewDetails(getSpaceReviewdetails(spaceDetailDto.getId()));
        spaceDetailDto.setSimilarSpaces(findSimilarSpaces(spaceDetailDto.getId(), eventId, false));
        spaceDetailDto.setFutureBookingDates(getUpcomingPaidBookingsDuration(spaceDetailDto.getId()));

    }

    @Override
    public SpaceDetailDto spaceCalendarDetails(Integer spaceId) {
        Space space = spaceDao.read(spaceId);
        SpaceDetailDto spaceDetailDto = space.buildCalendarDetailsDto();
        spaceDetailDto.setFutureBookingDates(getUpcomingPaidBookingsDuration(spaceId));
        return spaceDetailDto;
    }

    @Override
    public SpaceDetailDto hostCalendarDetails(Integer spaceId) {
        Space space = spaceDao.read(spaceId);
        SpaceDetailDto spaceDetailDto = space.buildCalendarDetailsDto();
        spaceDetailDto.setBookingDates(getAllPaidBookingsDuration(spaceId));
        return spaceDetailDto;
    }

    @Override
    public void enableScheduler() {
        ScheduleStart scheduleStart = scheduleStartDao.read(1);
        scheduleStart.setIsStarted(0);
        scheduleStartDao.update(scheduleStart);
    }

    @Override
    public void disableScheduler() {
        ScheduleStart scheduleStart = scheduleStartDao.read(1);
        scheduleStart.setIsStarted(1);
        scheduleStartDao.update(scheduleStart);
    }

    @Override
    public Long getAllSpacesCount() {
        return spaceDao.getAllSpacesCount();
    }

    @Override
    public ResponseDto draftSpace(SpaceDetailDto spaceDetailDto, Integer userId) {
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        DraftSpace draftSpace = new DraftSpace();
        ResponseDto responseDto = new ResponseDto();
        spaceDetailDto.setUser(userId);
        try {
            draftSpace.setSpace(mapper.writeValueAsString(spaceDetailDto));
            draftSpace.setUser(userId);
            draftSpace.setCreatedAt(new Date());
            draftSpace.setUpdatedAt(new Date());
            draftSpace.setActive(BooleanEnum.TRUE.value());
            draftSpaceDao.create(draftSpace);
            responseDto.setMessage("success");
        } catch (Exception e) {
            responseDto.setMessage("something went wrong");
        }
        return responseDto;

    }

    @Override
    public ResponseDto updateDraftSpace(SpaceDetailDto spaceDetailDto, Integer userId, Integer id) {
        ResponseDto responseDto = new ResponseDto();
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        //User user=userDao.read(userId);
        try {
            DraftSpace draftSpace = draftSpaceDao.read(id);
            if (draftSpace.getUser().equals(userId)) {
                draftSpace.setSpace(mapper.writeValueAsString(spaceDetailDto));
                draftSpace.setUpdatedAt(new Date());
                draftSpaceDao.update(draftSpace);
                responseDto.setMessage("success");
            } else
                responseDto.setMessage("you are not authorized");
        } catch (Exception e) {
            responseDto.setMessage("something went wrong");
        }
        return responseDto;
    }

    @Override
    public ResponseDto removeDraftSpace(Integer id, Integer userId) {
        ResponseDto responseDto = new ResponseDto();

        try {
            DraftSpace draftSpace = draftSpaceDao.read(id);
            if (draftSpace.getUser().equals(userId)) {
                draftSpace.setActive(0);
                draftSpace.setUpdatedAt(new Date());
                draftSpaceDao.update(draftSpace);
                responseDto.setMessage("success");
            } else
                responseDto.setMessage("you are not authorized");
        } catch (Exception e) {
            responseDto.setMessage("something went wrong");
        }
        return responseDto;
    }

    @Override
    public List<DraftSpaceDto> getDraftSpaces(Integer userId) {
        User user = userDao.read(userId);
        List<DraftSpaceDto> draftSpaces = new ArrayList<>();
        for (DraftSpace draftSpace : user.getDraftSpaces()) {
            DraftSpaceDto draftSpaceDto = new DraftSpaceDto();
            draftSpaceDto.setId(draftSpace.getId());
            draftSpaceDto.setCreatedAt(draftSpace.getCreatedAt().getTime());
            draftSpaceDto.setUpdatedAt(draftSpace.getUpdatedAt().getTime());
            try {
                draftSpaceDto.setSpace(new ObjectMapper().readValue(draftSpace.getSpace(), SpaceDetailDto.class));
            } catch (IOException e) {
                LOGGER.error(e);
            }
            draftSpaces.add(draftSpaceDto);
        }
        return draftSpaces;
    }

    @Override
    public Map<String, Object> saveImageDetails(List<ImageDetailsDto> imageDetails) {
        Map<String, Object> result = new HashMap<>();
        for (ImageDetailsDto imageDetailsDto : imageDetails) {
            ImageDetails imageDetail = ImageDetails.build(imageDetailsDto);
            imageDetailsDao.create(imageDetail);
        }
        result.put("responce", "success");
        return result;
    }

    @Override
    public ImageDetailsDto getImageDetails(String url) {
        return imageDetailsDao.getByUrl(url).build();
    }

    @Override
    public PrioritySpacesDto getPrioritySpaces() {
        List<PrioritySpaces> prioritySpaces = prioritySpacesDao.getAllSpaces();
        List<PrioritySpaceDto> prioritySpaceDtoList = new ArrayList<>();
        PrioritySpacesDto prioritySpacesDto = new PrioritySpacesDto();

        for (PrioritySpaces prioritySpace : prioritySpaces) {
            prioritySpaceDtoList.add(prioritySpace.build());

        }

        prioritySpacesDto.setSpaces(prioritySpaceDtoList);
        return prioritySpacesDto;
    }

    @Override
    public Map<String, Object> savePrioritySpaces(List<PrioritySpaceDto> prioritySpaces) {
        Map<String, Object> response = new HashMap<>();

        for (PrioritySpaceDto prioritySpaceDto : prioritySpaces) {
            PrioritySpaces prioritySpace = new PrioritySpaces();
            prioritySpace.setSpace(spaceDao.read(prioritySpaceDto.getSpace()));
            prioritySpacesDao.create(prioritySpace);
        }

        response.put("response", "success");
        return response;
    }

    @Override
    public Map<String, Object> updatePrioritySpaces(List<PrioritySpaceDto> prioritySpaces) {
        Map<String, Object> response = new HashMap<>();

        for (PrioritySpaceDto prioritySpaceDto : prioritySpaces) {
            PrioritySpaces prioritySpace = prioritySpacesDao.read(prioritySpaceDto.getId());
            prioritySpace.setSpace(spaceDao.read(prioritySpaceDto.getSpace()));
            prioritySpacesDao.create(prioritySpace);
        }

        response.put("response", "success");
        return response;
    }


    @Override
    public Map<String, Object> changeHostOfSpace(HostChangeDto hostChangeDto) {
        Map<String, Object> response = new HashMap<>();
        User user = userService.getUserByEmailPri(hostChangeDto.getUser().getEmail());
        if (user == null) {
            userService.create(hostChangeDto.getUser(), "");
            user = userService.getUserByEmailPri(hostChangeDto.getUser().getEmail());
        }
        Space space = spaceDao.read(hostChangeDto.getSpace());
        space.setUser(user.getId());
        spaceDao.update(space);
        response.put("response", "success");
        return response;
    }


    @Override
    public SpaceOfTheWeekDto getSpaceOfTheWeek() {
        SpaceOfWeek spaceOfWeek = spaceOfWeekDao.getSpaceOfTheWeek();
        if (spaceOfWeek != null)
            return spaceOfWeek.build();
        else
            return null;
    }

    @Override
    public List<SpaceContactPersonDto> getOtherSpaceContactPersons(Integer spaceId) {
        Space space = spaceDao.read(spaceId);
        Set<SpaceContactPerson> spaceContactPersons = space.getSpaceContactPersons();
        List<SpaceContactPersonDto> spaceContactPersonDtos = new ArrayList<>();

        for (SpaceContactPerson spaceContactPerson : spaceContactPersons) {
            spaceContactPersonDtos.add(spaceContactPerson.build());
        }

        return spaceContactPersonDtos;
    }

    private void setExtraContactPersons(Space space, SpaceContactPersonDto spaceContactPersonDto) {
        SpaceContactPerson spaceContactPerson = new SpaceContactPerson();
        if ((spaceContactPersonDto.getName() != null && !spaceContactPersonDto.getName().equals("")) ||
                (spaceContactPersonDto.getMobile() != null && !spaceContactPersonDto.getMobile().equals("")) ||
                (spaceContactPersonDto.getEmail() != null && !spaceContactPersonDto.getEmail().equals(""))
        ) {
            Set<SpaceContactPerson> hosts = new HashSet<>();
            spaceContactPerson.setContactPersonName(spaceContactPersonDto.getName());
            spaceContactPerson.setContactPersonEmail(spaceContactPersonDto.getEmail());
            spaceContactPerson.setContactPersonMobile(spaceContactPersonDto.getMobile());
            spaceContactPerson.setSpace(space.getId());
            hosts.add(spaceContactPerson);
            space.setSpaceContactPersons(hosts);
        }
    }


    @Override
    public Map<String, Object> isSpaceAvailable(BookingDto bookingDto) {
        Map<String, Object> result = new HashMap<>();
        Space space = spaceDao.read(bookingDto.getSpace());

        /**
         * if It has external api then call & return

         if (space.getExternalApiCodes()!=null && !space.getExternalApiCodes().isEmpty()){
         ExternalApiCode apis= space.getExternalApiCodes().iterator().next();
         return callAvialableCheckExternalApi(apis,bookingDto);
         }

         */

        if (isSpaceAvailableWIthInSelectedTimeDuration(bookingDto))
            result.put("response", true);
        else {
            result.put("response", false);
            if (isNotBlockTime(bookingDto))
                result.put("reason", "Contact the MillionSpaces call centre on 0117811811 for short notice bookings.");
            else
                result.put("reason", "booking not available");

        }
        return result;
    }


    @Override
    public Map<Integer, Object> batchAdvanceSearch(List<AdvanceSearchDto> advanceSearchDtos) {
        Map<Integer, Object> response = new HashMap<>();

        for (AdvanceSearchDto advanceSearchDto : advanceSearchDtos) {
            Map<String, Object> spaces = spaceDao.advancedSearch(advanceSearchDto, false, 0);
            response.put(advanceSearchDto.getEvents()[0], convertSpaceEntityToBatchSearchDto((List<Space>) spaces.get("spaces")));
        }
        return response;
    }

    /**
     * private List<Map> callFutureBookedDatesExternalApi(ExternalApiCode apis){
     * Optional<String> subUrlOptional=apis.getExternalApiCodeHasApiDetails().stream().filter(api->api.getApiDetails().equals(1)).findFirst().map(api2->api2.getApi_url());
     * if (subUrlOptional.isPresent()) {
     * String absolutePath = "https://" + apis.getRootUrl() + subUrlOptional.get();
     * <p>
     * RestTemplate restTemplate = new RestTemplate();
     * List<MediaType> acceptableMediaTypes = new ArrayList<>();
     * acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
     * HttpHeaders headers = new HttpHeaders();
     * headers.setAccept(acceptableMediaTypes);
     * HttpEntity<List> entity = new HttpEntity<>(headers);
     * ResponseEntity<List> result;
     * try {
     * result = restTemplate.exchange(absolutePath, HttpMethod.GET, entity, List.class);
     * } catch (Exception e) {
     * throw new EventspaceException("not able to get the search address of the space from google api ");
     * <p>
     * }
     * return result.getBody();
     * }
     * return null;
     * }
     * <p>
     * <p>
     * private Map<String,Object> callAvialableCheckExternalApi(ExternalApiCode apis,BookingDto bookingDto){
     * Optional<ExternalApiCodeHasApiDetails> externalApiCodeHasApiDetailOptional=apis.getExternalApiCodeHasApiDetails().stream().filter(api->api.getApiDetails().equals(2)).findFirst();
     * if (externalApiCodeHasApiDetailOptional.isPresent()) {
     * ExternalApiCodeHasApiDetails externalApiCodeHasApiDetails=externalApiCodeHasApiDetailOptional.get();
     * String absolutePath = "https://" + apis.getRootUrl() + externalApiCodeHasApiDetails.getApi_url();
     * bookingDto.setSpace(externalApiCodeHasApiDetails.getExternalSpaceId());
     * <p>
     * RestTemplate restTemplate = new RestTemplate();
     * List<MediaType> acceptableMediaTypes = new ArrayList<>();
     * acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
     * HttpHeaders headers = new HttpHeaders();
     * headers.setAccept(acceptableMediaTypes);
     * HttpEntity<?> entity = new HttpEntity<Object>(bookingDto, headers);
     * ResponseEntity<Map> result;
     * try {
     * result = restTemplate.exchange(absolutePath, HttpMethod.POST, entity, Map.class);
     * } catch (Exception e) {
     * throw new EventspaceException("not able to get the search address of the space from google api ");
     * <p>
     * }
     * return result.getBody();
     * }
     * return null;
     * }
     */


    @Override
    public Map<String, Object> changeExpireDateOfSpace(HostChangeDto hostChangeDto) {
        Map<String, Object> response = new HashMap<>();
        Space space = spaceDao.read(hostChangeDto.getSpace());
        space.setCalendarEnd(hostChangeDto.getCalendarEnd());
        spaceDao.update(space);
        response.put("response", "success");
        return response;
    }

    @Override
    @Transactional
    public Map<String, Object> addPrimaryEventType(AddPrimaryEventTypeDto addPrimaryEventTypeDto) {
        Map<String, Object> response = new HashMap<>();
        String responseStr = "success";
        Space space = spaceDao.read(addPrimaryEventTypeDto.getSpaceId());

        if (space != null) {
            EventType eventType = eventTypeDao.read(addPrimaryEventTypeDto.getEventTypeId());
            if (eventType != null) {
                space.setPrimaryEventType(eventType);
                spaceDao.update(space);
            } else
                responseStr = "invalid event-type id";
        } else
            responseStr = "invalid space id";

        response.put("response", responseStr);
        return response;
    }

    @Override
    public HostChangeDto spaceHostDetails(Integer spaceId) {
        Space space=spaceDao.read(spaceId);
        User user=userDao.read(space.getUser());

        HostChangeDto hostChangeDto=new HostChangeDto();
        hostChangeDto.setSpace(spaceId);
        hostChangeDto.setUser(user.build());
        return hostChangeDto;
    }

    @Override
    public List<AdminSpaceDetailsDto> getAdminFilterSpaces(int page, Integer isActive) {
        return convertSpaces(spaceDao.getAdminFilterSpaces(page,isActive));
    }

    @Override
    public Long getAdminFilterSpacesCount(Integer isActive) {
        return spaceDao.getAdminFilterSpacesCount(isActive);
    }

    @Override
    public List<AdminSpaceDetailsDto> getSearchSpaceData() {
        List<Space> spaces=spaceDao.getAllSpaces(BooleanEnum.TRUE);
        List<AdminSpaceDetailsDto> response=new ArrayList<>();
        spaces.stream().forEach(space -> response.add(space.buildSpaceSearchData()));
        return response;
    }

    @Override
    public List<ReviewDetailsDto> getMSReviews() {
        List<ReviewDetailsDto> reviewDetailsDtoList=new ArrayList<>();
        List<Reviews> reviews=reviewsDao.getMSReviews();
        if (reviews.size()>0){
             reviews.stream().forEach(reviews1 -> reviewDetailsDtoList.add(reviews1.buildMsReview()));
        }
        return reviewDetailsDtoList;
    }

    @Override
    public BookingSpaceDto getBookingSpace(Integer spaceId) {
        Space space=spaceDao.read(spaceId);
        BookingSpaceDto spaceDto=space.buildBookingSpaceDetails();
        spaceDto.setSpaceOwnerDto(getSpaceOwnerDetails(space.getUser()));
        return spaceDto;
    }
}
