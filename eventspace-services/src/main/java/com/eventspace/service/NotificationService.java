package com.eventspace.service;

import com.eventspace.dto.NotificationDto;

import java.util.Map;

public interface NotificationService {

    Object sendNotification(NotificationDto notificationDto);
}
