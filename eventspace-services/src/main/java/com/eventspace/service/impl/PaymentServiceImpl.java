/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.service.impl;


import au.com.gateway.client.GatewayClient;
import au.com.gateway.client.component.Redirect;
import au.com.gateway.client.component.TransactionAmount;
import au.com.gateway.client.config.ClientConfig;
import au.com.gateway.client.enums.TransactionType;
import au.com.gateway.client.ex.GatewayClientException;
import au.com.gateway.client.payment.PaymentCompleteRequest;
import au.com.gateway.client.payment.PaymentCompleteResponse;
import au.com.gateway.client.payment.PaymentInitRequest;
import au.com.gateway.client.payment.PaymentInitResponse;
import com.eventspace.dao.BookingDao;
import com.eventspace.dao.BookingHistoryDao;
import com.eventspace.dao.SpaceDao;
import com.eventspace.domain.Booking;
import com.eventspace.domain.BookingHistory;
import com.eventspace.domain.BookingSlots;
import com.eventspace.domain.Space;
import com.eventspace.dto.*;
import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.service.BookingService;
import com.eventspace.service.PaymentService;
import com.eventspace.service.SpaceService;
import com.eventspace.util.Constants;
import com.eventspace.util.LogMessages;
import com.google.gson.JsonObject;
import fr.opensagres.xdocreport.document.json.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The Class PaymentServiceImpl.
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(PaymentServiceImpl.class);

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private SpaceDao spaceDao;

    @Autowired
    private BookingHistoryDao bookingHistoryDao;


    @Value("${million.spaces.url}")
    private String webUrl;

    @Value("${million.spaces.payment.url}")
    private String paymentServerUrl;

    @Value("${million.spaces.api.url}")
    private String apiUrl;

    @Value("${order.id.prefix}")
    private String orderIdPrefix;

    @Value("${order.id.digit}")
    private String orderIdDigit;

    @Value("${pay.crop.service.endpoint}")
    public String payCropServiceEndpoint;

    @Value("${pay.crop.hmac.secret}")
    public String payCropHmacSecret;

    @Value("${pay.crop.auth.token}")
    public String payCropAutToken;

    @Value("${pay.crop.client.id}")
    public int payCropClientId;

    @Value("${pay.crop.currency}")
    public String payCropCurrency;

    @Value("${pay.crop.return.api}")
    public String payCropReturnApi;

    @Value("${pay.crop.return.web}")
    public String payCropReturnWeb;



    /**
     * payments.
     *
     * @param encryptedString the encrypted string
     * @return payment details
     */
    @Override
    public PaymentDetailsDto payments(String encryptedString) {
            LOGGER.info("payments method -----> get call");
            PaymentDetailsDto dto = new PaymentDetailsDto();

            if (decryption(encryptedString) != null) {
                dto.setResponse("OK");
                return dto;
            } else {
                dto.setResponse("Wrong Input");
                return dto;
            }
    }

    /**
     * encrypt.
     *
     * @param data the data
     * @return decrypted string
     */
    @Override
    @Transactional
    public String encrypt(String data) {
        try {
            String key = "t2s67p1a345o7rzm";
            String iv = "abcdefghabcdefgd";

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return DatatypeConverter.printBase64Binary(encrypted);

        } catch (Exception e) {
            LOGGER.error("encrypt exception----->{}",e);
            return null;
        }
    }

    /**
     * decrypt.
     *
     * @param encryptedString the encrypted string
     * @return original string
     */
    @Override
    @Transactional
    public String decryption(String encryptedString) {

        try {
            String key = "t2s67p1a345o7rzm";
            String iv = "abcdefghabcdefgd";

            byte[] encrypted1 = DatatypeConverter.parseBase64Binary(encryptedString);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            LOGGER.info("The original String is : " + new String(original));
            return new String(original);
        } catch (Exception e) {
            LOGGER.error("decryption exception----->{}",e);
            return null;
        }
    }

    @Override
    public PaymentSummaryDto getPaymentSummary(Integer bookingId) {
        Booking booking=bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        PaymentSummaryDto paymentSummaryDto=new PaymentSummaryDto();
        //DateFormat df = new SimpleDateFormat("dd MMMMM yyyy EEEEE");
        DateFormat df2 = new SimpleDateFormat(Constants.PATTERN_9);

        BookingHistory bookingHistory=bookingHistoryDao.getBookingHistory(booking.getId(), StatesEnum.INITIATED.value());
        Set<CommonDto> slots = new HashSet<>(0);
        for (BookingSlots bookingSlot : booking.getBookingSlots()) {
            CommonDto slot = new CommonDto();
            slot.setFromDate(df2.format(bookingSlot.getFromDate()));
            slot.setToDate(df2.format(bookingSlot.getToDate()));
            slots.add(slot);
        }

        Date eventDate=new Date();
        Boolean isSet=true;

        for (BookingSlots bookingSlots:booking.getBookingSlots()){
            if (isSet) {
                eventDate = bookingSlots.getFromDate();
                isSet=false;
            }
            if (eventDate.getTime()>bookingSlots.getFromDate().getTime())
                eventDate=bookingSlots.getFromDate();
        }

        Long diff=(eventDate.getTime()-bookingHistory.getCreatedAt().getTime())/(24 * 60 * 60 * 1000);

        paymentSummaryDto.setBookingId(booking.getId());
        paymentSummaryDto.setReferenceId(booking.getOrderId());
        paymentSummaryDto.setSpace(space.getName());
        paymentSummaryDto.setOrganization(space.getAddressLine2());
        paymentSummaryDto.setAddress(space.getAddressLine1());
        paymentSummaryDto.setTotal(booking.getBookingCharge().intValue());
        paymentSummaryDto.setReservationTime(slots);
        paymentSummaryDto.setDate(eventDate.getTime());
        paymentSummaryDto.setDateDiff(diff);
        return paymentSummaryDto;
    }

    @Override
    public Map<String, Object> initiatedPaycorp(CommonDto bookingDto) {

        LOGGER.info("Service | intiateIpgPayCorp get called");

        ClientConfig config = new ClientConfig();
        config.setServiceEndpoint(payCropServiceEndpoint);
        config.setHmacSecret(payCropHmacSecret);
        config.setAuthToken(payCropAutToken);
        GatewayClient client = new GatewayClient(config);


        Booking booking=bookingDao.read(bookingDto.getId());


        // builds payment-init request
        PaymentInitRequest paymentInitRequest = new PaymentInitRequest();
        paymentInitRequest.setClientId(payCropClientId);
        paymentInitRequest.setTransactionType(TransactionType.PURCHASE);
        paymentInitRequest.setTokenize(false);
        TransactionAmount transactionAmount = new TransactionAmount();

        double ipgAmount = booking.getBookingCharge().intValue() * 100;

        if (Optional.ofNullable(booking.getAdvance()).isPresent() && booking.getAdvance().intValue()>0){
            ipgAmount = booking.getAdvance().intValue() * 100;
            transactionAmount.setPaymentAmount(booking.getAdvance().intValue() * 100);
        }else {
            transactionAmount.setPaymentAmount(booking.getBookingCharge().intValue() * 100);
        }

        LOGGER.info(String.format("IPG | payment set [%s]", "" + ipgAmount));
        LOGGER.info(String.format("IPG | payment sent [%s]", "" + transactionAmount.getPaymentAmount()));

        transactionAmount.setCurrency(payCropCurrency);
        paymentInitRequest.setTransactionAmount(transactionAmount);
        Redirect redirect = new Redirect();

        redirect.setReturnUrl(apiUrl+payCropReturnApi);
        redirect.setReturnMethod("POST");
        paymentInitRequest.setRedirect(redirect);
        paymentInitRequest.setComment("");
        paymentInitRequest.setClientRef(booking.getOrderId());
        paymentInitRequest.setTokenReference("");

        Map<String,Object> res=new HashMap<>();
        try {
            PaymentInitResponse paymentInitResponse  = client.payment().init(paymentInitRequest);
            res.put("status",200);
            res.put("message","success");
            res.put("url",paymentInitResponse.getPaymentPageUrl());
        } catch (GatewayClientException exp) {
            res.put("status",500);
            res.put("message","failed");
        }



        return res;
    }

    @Override
    public void completePaycorp(HttpServletRequest request, HttpServletResponse response) {
        Integer x=1;


        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setServiceEndpoint(payCropServiceEndpoint);
        clientConfig.setHmacSecret(payCropHmacSecret);
        clientConfig.setAuthToken(payCropAutToken);
        GatewayClient gatewayClient = new GatewayClient(clientConfig);


        PaymentCompleteRequest paymentCompleteRequest = new PaymentCompleteRequest();
        paymentCompleteRequest.setReqid(request.getParameter("reqid"));
        paymentCompleteRequest.setClientId(payCropClientId);

        try {//Submit Payment Complete Request to Paycorp to confirm Payment Settlement
            PaymentCompleteResponse paymentCompleteResponse = gatewayClient.payment().complete(paymentCompleteRequest);
            LOGGER.info(String.format(LogMessages.PS_PAYCORP_RESPONSE,paymentCompleteResponse));

            request.setAttribute("paymentCompleteRequest", paymentCompleteRequest);
            request.setAttribute("paymentCompleteResponse", paymentCompleteResponse);

            Integer bookingId = bookingService.getBookingIdFromOrderId(paymentCompleteResponse.getClientRef());

            LOGGER.info(String.format(LogMessages.PS_PAYCORP_BOOKING,bookingId));

            BookingActionDto bookingActionDto = new BookingActionDto();
            bookingActionDto.setBooking_id(bookingId);
            bookingActionDto.setEvent(EventsEnum.PAY.value());
            bookingActionDto.setMethod("IPG");
            bookingActionDto.setIpgName("PAYCORP");
            bookingActionDto.setIpgResponseCode(paymentCompleteResponse.getResponseCode());
            if (paymentCompleteResponse.getResponseCode().equals("00")) {
                bookingActionDto.setPaymentSuccessCode(1);
            }else{
                bookingActionDto.setPaymentSuccessCode(0);
            }

            bookingService.bookingActions(bookingActionDto, true);
            String url=paymentServerUrl+payCropReturnWeb+"?code="+paymentCompleteResponse.getResponseCode()+"&description="+paymentCompleteResponse.getResponseText();
            LOGGER.info(String.format(LogMessages.PS_PAYCORP_REDIRECT_URL,url));
            response.sendRedirect(url);

        } catch (GatewayClientException exp) {
            LOGGER.info(exp.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void completeUpay(UpayResponseDto upayResponseDto, HttpServletResponse response) {

        LOGGER.info(String.format(LogMessages.PS_UPAY_RESPONSE,upayResponseDto.toString()));
        Integer orderId = bookingService.getBookingIdFromOrderId(upayResponseDto.getMerchant_txn_ref_no());

        LOGGER.info(String.format(LogMessages.PS_UPAY_BOOKING,upayResponseDto.getMerchant_txn_ref_no()));

        BookingActionDto bookingActionDto = new BookingActionDto();
        bookingActionDto.setBooking_id(orderId);
        bookingActionDto.setEvent(EventsEnum.PAY.value());
        bookingActionDto.setMethod("IPG");
        bookingActionDto.setIpgName("UPAY");
        bookingActionDto.setIpgResponseCode(upayResponseDto.getTxn_status_code());
        if (upayResponseDto.getTxn_status_code().equals("1")) {
            bookingActionDto.setPaymentSuccessCode(1);
        }else{
            bookingActionDto.setPaymentSuccessCode(0);
        }

        try {
            bookingService.bookingActions(bookingActionDto, true);
            String url=paymentServerUrl+payCropReturnWeb+"?code="+upayResponseDto.getTxn_status_code()+"&description="+upayResponseDto.getTxn_status_message();
            LOGGER.info(String.format(LogMessages.PS_UPAY_REDIRECT_URL,url));
            response.sendRedirect(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void completeFrimi(FrimiResponseDto frimiResponseDto, HttpServletResponse response) {
        LOGGER.info(String.format(LogMessages.PS_FRIMI_RESPONSE,frimiResponseDto.toString()));

        JSONObject frimiBody=new JSONObject(URLDecoder.decode(frimiResponseDto.getBody()));
        Integer orderId = bookingService.getBookingIdFromOrderId(frimiBody.getString("frimi_txn_ref_no"));

        LOGGER.info(String.format(LogMessages.PS_FRIMI_BOOKING,frimiBody.getString("frimi_txn_ref_no")));

        BookingActionDto bookingActionDto = new BookingActionDto();
        bookingActionDto.setBooking_id(orderId);
        bookingActionDto.setEvent(EventsEnum.PAY.value());
        bookingActionDto.setMethod("IPG");
        bookingActionDto.setIpgName("FRIMI");
        bookingActionDto.setIpgResponseCode(frimiBody.getString("frimi_txn_ref_no"));
        if (frimiBody.getString("frimi_txn_ref_no").equals("00")) {
            bookingActionDto.setPaymentSuccessCode(1);
        }else{
            bookingActionDto.setPaymentSuccessCode(0);
        }

        try {
            bookingService.bookingActions(bookingActionDto, true);
            String url=paymentServerUrl+payCropReturnWeb+"?code="+frimiBody.getString("frimi_txn_ref_no")+"&description="+frimiBody.getString("description");
            LOGGER.info(String.format(LogMessages.PS_FRIMI_REDIRECT_URL,url));
            response.sendRedirect(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
