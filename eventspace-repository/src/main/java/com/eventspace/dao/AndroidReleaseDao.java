package com.eventspace.dao;

import com.eventspace.domain.AndroidRelease;

public interface AndroidReleaseDao extends BaseDao<AndroidRelease>  {

    AndroidRelease getLatestRelease();
}
