/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.enumeration;

import java.util.HashMap;

public enum UserCreateEnum {

    EXISTING(0), CREATED(1);

    private static final HashMap<Integer, UserCreateEnum> typeValueMap = new HashMap<>();

    static {
        for (UserCreateEnum type : UserCreateEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    UserCreateEnum(final int value) {
        this.value = value;
    }

    public static UserCreateEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
