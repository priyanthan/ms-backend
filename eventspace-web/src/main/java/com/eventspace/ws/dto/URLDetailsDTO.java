package com.eventspace.ws.dto;


public class URLDetailsDTO {

    private String uri;
    private String method;

    public URLDetailsDTO(){}

    public URLDetailsDTO(String url, String method) {
        this.uri = url;
        this.method = method;
    }

    public String getUri() {
        return uri;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        URLDetailsDTO that = (URLDetailsDTO) o;

        if (!uri.equals(that.uri)) return false;
        return method.equals(that.method);
    }

    @Override
    public int hashCode() {
        int result = uri.hashCode();
        result = 31 * result + method.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s %s", method, uri);
    }
}
