/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.dao;

import com.eventspace.domain.BookingSlots;
import com.eventspace.domain.Space;
import com.eventspace.dto.AdvanceSearchDto;
import com.eventspace.enumeration.BooleanEnum;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The Interface UserDao.
 */
public interface SpaceDao extends BaseDao<Space> {

    List<Space> getAllSpaces(BooleanEnum isActive);

    List<Space> filteredSpaces(String[] participation, String[] ratePerHour, Integer[] amenities, Integer[] events, Integer limit);

    List<Space> getCoordinateSpaces(String latitude, String longitude, Integer radius);

    Boolean deleteSpace(Integer id);

    boolean isAvalableForBook(Integer spaceId, BookingSlots solt, Integer bufferTime, Integer noticePeriod, Long currentTime);

    boolean isBlockTime(Integer spaceId, BookingSlots solt);

    List<Space> getPaginatedSpaces(int page);

    List<Space> getAdminSpaces(int page);

    List<Space> getSearchSpaces(Integer[] events, String location);

    List<Space> getUserSpaces(Integer userId);

    Map<String, Object> advancedSearch(AdvanceSearchDto advanceSearchDto, boolean isCount, Integer page);

    List<Space> getFeaturedSpaces();

    List<Space> getChilds(Integer spaceId);

    List<Space> getLinkedSpaces(Integer spaceId);

    List<Space> getCalenderEndingSpaces(Date date);

    List<Space> getCalenderEndSpaces(Date date);

    Long getAllSpacesCount();

    List<Space> getAdminFilterSpaces(int page,Integer isActive);

    Long getAdminFilterSpacesCount(Integer isActive);
}
