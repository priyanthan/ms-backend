package com.eventspace.service.impl;

import com.eventspace.dao.*;
import com.eventspace.domain.*;
import com.eventspace.dto.*;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.email.sender.EmailSender;
import com.eventspace.enumeration.*;
import com.eventspace.eventaction.dto.BookingContextDto;
import com.eventspace.eventaction.dto.MailCriteriaComponents;
import com.eventspace.eventaction.impl.*;
import com.eventspace.service.*;
import com.eventspace.util.Constants;
import com.eventspace.util.LogMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Aux-054 on 12/15/2016.
 */

/**
 * The Class BookingServiceImpl.
 */
@Service
@Transactional
public class BookingServiceImpl implements BookingService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(BookingServiceImpl.class);
    /**
     * The mail criteria components.
     */
    @Autowired
    private MailCriteriaComponents mailCriteriaComponents;

    /**
     * The application context.
     */
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * The booking dao.
     */
    @Autowired
    private BookingDao bookingDao;

    /**
     * The review dao.
     */
    @Autowired
    private ReviewsDao reviewsDao;

    /**
     * The space dao.
     */
    @Autowired
    private SpaceDao spaceDao;

    /**
     * The amenity dao.
     */
    @Autowired
    private AmenityDao amenityDao;

    /**
     * The user dao.
     */
    @Autowired
    private UserDao userDao;

    /**
     * The event type dao.
     */
    @Autowired
    private EventTypeDao eventTypeDao;

    /**
     * The booking space extra amenity dao.
     */
    @Autowired
    private BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao;

    @Autowired
    private BookingExpireDetailsDao bookingExpireDetailsDao;

    /**
     * The event publisher.
     */
    @Autowired
    private EventPublisher eventPublisher;

    /**
     * The email sender.
     */
    @Autowired
    private EmailSender emailSender;

    /**
     * The reservation status dao.
     */
    @Autowired
    private ReservationStatusDao reservationStatusDao;

    /**
     * The booking history dao.
     */
    @Autowired
    private BookingHistoryDao bookingHistoryDao;

    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    /**
     * The email service.
     */
    @Autowired
    private EmailService emailService;

    /**
     * The space unavailablity dao.
     */
    @Autowired
    private SpaceUnavailabilityDao spaceUnavailabilityDao;

    /**
     * The payment history dao.
     */
    @Autowired
    private PaymentHistoryDao paymentHistoryDao;

    @Autowired
    private SeatingArrangementDao seatingArrangementDao;

    @Autowired
    private CancellationPolicyDao cancellationPolicyDao;

    @Autowired
    private AmenityUnitsDao amenityUnitsDao;

    @Autowired
    private PromoDetailsDao promoDetailsDao;

    @Autowired
    private BookingPayLaterDao bookingPayLaterDao;

    @Autowired
    private CommonService commonService;

    @Autowired
    private BookingHasPromoDao bookingHasPromoDao;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private BlockAvailablityDao blockAvailablityDao;

    @Autowired
    private PerHourAvailablityDao perHourAvailablityDao;

    @Autowired
    private  SpaceExtraAmenityDao spaceExtraAmenityDao;

    @Autowired
    private CryptographyService cryptographyService;

    /**
     * The login url.
     */
    @Value("${payment.redirect.url}")
    private String paymentRedirectUrl;

    @Value("${million.spaces.url}")
    private String webUrl;

    @Value("${million.spaces.payment.url}")
    private String paymentServerUrl;

    @Value("${million.spaces.api.url}")
    private String apiUrl;

    @Value("${order.id.prefix}")
    private String orderIdPrefix;

    @Value("${host.order.prefix}")
    private String hostOrderIdPrefix;

    @Value("${order.id.ipg.prefix}")
    private String orderIPGPrefix;

    @Value("${order.id.digit}")
    private String orderIdDigit;

    @Value("${ipg.concat.char}")
    private String IpgConcatChar;

    @Value("${cache.enable}")
    public  Boolean enableCache;

    @Value("${admin.promo}")
    public  String adminPromo;

    @Value("${payment.gateway.rate}")
    public Double paymentGatewayRate;

    @Autowired
    private PaymentVerifyDao paymentVerifyDao;

    @Autowired
    private SpacePromotionDao spacePromotionDao;

    @Autowired
    private CorporatePromoDao corporatePromoDao;

    @Autowired
    private DiscountDetailsDao discountDetailsDao;

    @Autowired
    private ReportsDao reportsDao;

    /**
     * Add review.
     *
     * @param reviewDto the review dto
     * @param userId    the user id
     * @return the string
     */
    @Override
    @Transactional
    public String addReview(ReviewDto reviewDto, Integer userId) {
        LOGGER.info(String.format(LogMessages.BS_REVIEW_ADD,reviewDto.getBookingId(),userId));
        Reviews review = Reviews.build(reviewDto);
        String response = "review successfully added";
        if (userId.equals(bookingDao.read(review.getBookingId()).getUser())) {
            reviewsDao.create(review);
            emailService.sendViewReviewEmail(review.getBookingId());
            LOGGER.info(String.format(LogMessages.BS_REVIEW_SUCCESS,reviewDto.getBookingId(),userId));
        }
        else {
            response = "you are not authorized to review this space";
            LOGGER.info(String.format(LogMessages.BS_REVIEW_FAILED,reviewDto.getBookingId(),userId));
        }
        return response;
    }

    @Override
    @Transactional
    public Map<String,Object> addReviewByEmail(String key,ReviewDto reviewDto){
        LOGGER.info(String.format(LogMessages.BS_REVIEW_BY_EMAIL_ADD,reviewDto.getBookingId()));
        Map<String,Object> response=new HashMap<>();
        Integer bookingId=cryptographyService.decryption(key);
        Reviews review = Reviews.build(reviewDto);
        if (bookingId.equals(review.getBookingId())){
            reviewsDao.create(review);
            emailService.sendViewReviewEmail(review.getBookingId());
            response.put("response","success");
            LOGGER.info(String.format(LogMessages.BS_REVIEW_BY_EMAIL_SUCCESS,reviewDto.getBookingId()));
        }
        else {
            LOGGER.info(String.format(LogMessages.BS_REVIEW_BY_EMAIL_FAILED,reviewDto.getBookingId()));
            response.put("response", "failed");
        }
        return response;
    }
    /**
     * Book space.
     *
     * @param bookingDto the booking dto
     * @param id         the id
     * @return the integer
     */
    @Override
    @Transactional
    public Booking bookSpace(BookingDto bookingDto, Integer id) {
        LOGGER.info(String.format(LogMessages.BS_BOOK_SPACE,bookingDto.getSpace(),id));
        boolean isAvailable = spaceService.isSpaceAvailableWIthInSelectedTimeDuration(bookingDto);
        boolean blockTheSlot=true;
        ChargeDetailsDto chargeDetailsDto=calculateBookingCharge(bookingDto);
        LOGGER.info(String.format(LogMessages.BS_BOOKING_CHARGE_CALCULATE,bookingDto.getBookingCharge(),chargeDetailsDto.getBookingCharge()));

        if (isAvailable) {
            bookingDto.setReservationStatus(StatesEnum.INITIATED.value());
            Booking booking = Booking.build(bookingDto, id);
            booking.setOriginalCharge(chargeDetailsDto.getSlotCharge()+chargeDetailsDto.getExtraAmenityCharge());
            Space space=spaceDao.read(booking.getSpace());
            booking.setBookingCharge(Double.valueOf(chargeDetailsDto.getBookingCharge()));
            if (!chargeDetailsDto.getDiscount().equals(0)) {
                LOGGER.info(String.format(LogMessages.BS_BOOKING_REDUCE_DISCOUNT,chargeDetailsDto.getDiscount()));
                booking.setDiscount(Double.valueOf(chargeDetailsDto.getDiscount()));
            }
            if (Optional.ofNullable(bookingDto.getAdvanceOnly()).isPresent() && bookingDto.getAdvanceOnly() && chargeDetailsDto.getAdvanceOnlyEnable()){
                LOGGER.info(String.format(LogMessages.BS_BOOKING_ADD_ADVANCE,bookingDto.getAdvance()));
                booking.setAdvance(Double.valueOf(chargeDetailsDto.getAdvance()));
            }

            bookingDao.create(booking);// booking created
            booking.setOrderId(generateOrderId(booking.getId()));
            bookingDao.update(booking);

            if (bookingDto.getPromoCode()!=null){
                LOGGER.info(String.format(LogMessages.BS_BOOKING_ADD_PROMO,bookingDto.getPromoCode()));
                PromoDetails promoDetails=promoDetailsDao.getPromoByCode(bookingDto.getPromoCode());
                BookingHasPromo bookingHasPromo=new BookingHasPromo();
                bookingHasPromo.setBooking(booking.getId());
                bookingHasPromo.setPromoDetails(promoDetails.getId());
                bookingHasPromoDao.create(bookingHasPromo);

                if (promoDetails.getIsTimeBlock()!=null && promoDetails.getIsTimeBlock().equals(BooleanEnum.FALSE.value())) {
                    LOGGER.info(String.format(LogMessages.BS_BOOKING_NON_BLOCK_PROMO,booking.getOrderId()));
                    blockTheSlot = false;
                }

                if (bookingDto.getPromoCode().equals(adminPromo) ){
                    LOGGER.info(String.format(LogMessages.BS_BOOKING_ADMIN_PROMO,booking.getOrderId()));
                    markAsPaymentDone(booking.getId());
                }
            }

            if (Optional.ofNullable(space.getSpaceAdditionalDetails()).isPresent() &&
                    Optional.ofNullable(space.getSpaceAdditionalDetails().getAllowedBookings()).isPresent()){
                blockTheSlot=false;
                SimpleDateFormat sdf = new SimpleDateFormat(Constants.PATTERN_5);
                for (BookingSlots slots:booking.getBookingSlots()){

                    if (space.getSpaceAdditionalDetails().getAllowedBookings()<=((BigInteger) reportsDao.procedureUniqueResult(
                            String.format("count_same_bookings(%s,'%s','%s')",space.getId(),sdf.format(slots.getFromDate()),sdf.format(slots.getToDate()))).get("bookings")).intValue()) {
                        blockTheSlot = true;
                        LOGGER.info("NOT BLOCK THE TIME  ");
                    }
                }
            }

            bookSpaceWithExtraAmenity(bookingDto, booking);

            if (isBookingSlotHasPromotion(booking.getSpace(),booking.getBookingSlots())){
                /** for the avengers promotion - booking mark as paid with 1st step & same slot will be */
                //bookPromotedSpace(booking.getId());
            }else {
                if (blockTheSlot)
                    addBookingToSpaceUnavailablity(booking.getId(), booking.getBookingSlots(), booking.getSpace());
            }

            return booking;
        } else
            return null;

    }

    /**
     * Book space with extra amenity.
     *
     * @param bookingDto the booking dto
     * @param booking1   the booking1
     */
    private void bookSpaceWithExtraAmenity(BookingDto bookingDto,
                                           Booking booking1) {
        LOGGER.info(String.format(LogMessages.BS_BOOKING_ADD_EXTRA_AMENITY,booking1.getOrderId()));
        Set<BookingWithExtraAmenityDto> bookingWithExtraAmenityDtoSet = bookingDto
                .getBookingWithExtraAmenityDtoSet();
        Set<BookingSpaceExtraAmenity> bookingSpaceExtraAmenities = new HashSet<>(
                0);

        for (BookingWithExtraAmenityDto extraAmenityDto : bookingWithExtraAmenityDtoSet) {
            Set<SpaceExtraAmenity> spaceExtraAmenitySet = new HashSet<>();
            Amenity amenity = amenityDao.read(extraAmenityDto.getAmenityId());
            Space space = spaceDao.read(bookingDto.getSpace());
            Set<SpaceExtraAmenity> spaceExtraAmenities = space
                    .getExtraAmenities();

            for (SpaceExtraAmenity spaceExtraAmenity : spaceExtraAmenities) {
                if (Objects.equals(spaceExtraAmenity.getPk().getAmenity().getId(), extraAmenityDto
                        .getAmenityId())) {
                    spaceExtraAmenitySet.add(spaceExtraAmenity);

                    BookingSpaceExtraAmenity bookingSpaceExtraAmenity = BookingSpaceExtraAmenity
                            .build(extraAmenityDto, space, amenity, booking1,
                                    spaceExtraAmenitySet);
                    bookingSpaceExtraAmenities.add(bookingSpaceExtraAmenity);
                    if (bookingSpaceExtraAmenity.getNumber() != 0) {
                        bookingSpaceExtraAmenityDao
                                .create(bookingSpaceExtraAmenity);
                    }

                    if (bookingSpaceExtraAmenity.getNumber() != 0)
                        bookingSpaceExtraAmenityDao
                                .create(bookingSpaceExtraAmenity);
                }
            }
            booking1.setBookingSpaceExtraAmenities(bookingSpaceExtraAmenities);
        }

    }

    /**
     * Booking action.
     *
     * @param bookingActionDto the booking action dto
     * @return the map<string,object>
     * @throws Exception the exception
     */
    @Override
    @Transactional
    public Map<String, Object> bookingActions(BookingActionDto bookingActionDto, boolean isAdmin)
            throws Exception {
        Map<String, Object> response = new HashMap<>();
        String message = "";
        Booking booking = bookingDao.read(bookingActionDto.getBooking_id());
        LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION,booking.getOrderId()));
        BookingContextDto bookingContextDto = buildBookingContext(booking,
                bookingActionDto);

        StatesEnum status = StatesEnum.getInstanceFromValue(booking
                .getReservationStatus());
        EventsEnum event = EventsEnum.getInstanceFromValue(bookingActionDto
                .getEvent());

        StateMachine<StatesEnum, EventsEnum> stateMachine = getStateMachine(
                status, bookingContextDto);


        Boolean isAllowed = false;

        /**check whether user is authorize for this action or not*/
        if (booking.getReservationStatus().equals(StatesEnum.DISCARDED.value())){
            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_DISCARD,booking.getOrderId()));
            message = "true : booking discarded";
        }else  if (booking.getReservationStatus().equals(StatesEnum.EXPIRED.value())){
            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_EXPIRE,booking.getOrderId()));
            message = "true : booking expired";
        }
        else if (((event.equals(EventsEnum.CANCEL) || event.equals(EventsEnum.PAY))
                && booking.getUser().equals(bookingActionDto.getUserId()))
                ||
                isAdmin
                ) {
            /**check is this PAY*/
            if (event.equals(EventsEnum.PAY)) {
                switch (bookingActionDto.getMethod()) {
                    case "IPG": {
                        LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_IPG,booking.getOrderId()));

                        PaymentHistory paymentHistory = new PaymentHistory();
                        paymentHistory.setBookingId(bookingActionDto.getBooking_id());
                        paymentHistory.setResponse(bookingActionDto.getPaymentSuccessCode());
                        paymentHistory.setProvider(bookingActionDto.getIpgName());
                        paymentHistory.setResponseCode(bookingActionDto.getIpgResponseCode());
                        paymentHistoryDao.create(paymentHistory);

                        if (bookingActionDto.getPaymentSuccessCode().equals(BooleanEnum.TRUE.value())) {
                            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_IPG_SUCCESS,booking.getOrderId()));
                            isAllowed = true;
                            if (Optional.ofNullable(booking.getAdvance()).isPresent()){
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_IPG_ADVANCE_SUCCESS,booking.getOrderId()));
                                event=EventsEnum.PAY_ADVANCE;
                                message = "true : your are successfully paid the advance";
                            }else{
                                event=EventsEnum.PAY;
                                message = "true : your are successfully paid";
                            }

                        } else {
                            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_IPG_FAILED,booking.getOrderId()));
                            message = "false : your gave wrong  details for " + paymentHistoryDao.getWrongCardUsesForABooking(bookingActionDto.getBooking_id()) + " times";
                        }
                        break;
                    }
                    case "MANUAL": {
                        LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_MANUAL,booking.getOrderId()));
                        if (bookingActionDto.getStatus().equals(PaySubEventEnum.BANK_PAY.value()) && bookingDao.isUserAllowForBooking(booking.getUser())) {
                            isAllowed = true;
                            /** wait for advance payment */
                            if (Optional.ofNullable(booking.getAdvance()).isPresent()){
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_MANUAL_PENDING_ADVANCE,booking.getOrderId()));
                                event = EventsEnum.WAIT_FOR_PAY_ADVANCE;
                                message = "Payment instructions have been emailed to you. You have 24 hours to make the payment.";
                            }else {
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_MANUAL_PENDING,booking.getOrderId()));
                                event = EventsEnum.WAIT_FOR_PAY;
                                message = "Payment instructions have been emailed to you. You have 24 hours to make the payment.";
                            }
                        }
                        /** bank payment */
                        else if (bookingActionDto.getStatus().equals(PaySubEventEnum.BANK_PAY.value()) && !bookingDao.isUserAllowForBooking(booking.getUser())) {
                            message = "false : you had another pending payment";
                            BookingPayLater bookingPayLater=booking.getBookingPayLater();
                            if (bookingPayLater!=null && !bookingPayLater.getExpire()) {
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_BANK,booking.getOrderId()));
                                selectPaymentOptionAfterPayLater(booking.getId());
                                message = "true : you should make your payment within 24 hours";
                            }
                        }
                        /** corporate promos - booking charge is 0 */
                        else  if (bookingActionDto.getStatus().equals(PaySubEventEnum.PROMOTION.value())) {
                            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_PROMOTION,booking.getOrderId()));
                            PromoDetails promoDetails=promoDetailsDao.read(booking.getPromoDetails().getPromoDetails());
                            if (booking.getBookingCharge().intValue()==0 || Optional.ofNullable(promoDetails.getCorporatePromo()).isPresent() && promoDetails.getCorporatePromo().getActive()) {
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_CORPORATE_PROMOTION,booking.getOrderId()));
                                isAllowed = true;
                            }
                        }
                        /** avengers promos-booking price will be 0 & slot will not be block */
                        else  if (bookingActionDto.getStatus().equals(PaySubEventEnum.SPACE_PROMOTION.value())) {
                            isAllowed = false;
                            if (isBookingSlotHasPromotion(booking.getSpace(),booking.getBookingSlots())){
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_SPACE_PROMOTION,booking.getOrderId()));
                                bookPromotedSpace(booking.getId());
                                BookingSlots bookingSlot=booking.getBookingSlots().stream().findFirst().get();
                                SpacePromotion spacePromotion=spacePromotionDao.getSpacePromotion(booking.getSpace(),bookingSlot.getFromDate(),bookingSlot.getToDate());
                                response.put("promotion",Optional.ofNullable(spacePromotion).isPresent());
                            }
                        }
                        /** pay latter - within 4 days*/
                        else if (bookingActionDto.getStatus().equals(PaySubEventEnum.PAY_LATER.value()) && isPayLaterEnabled(booking.getId())){
                            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_PAY_LATER,booking.getOrderId()));
                            isAllowed = true;
                            event = EventsEnum.WAIT_FOR_PAY;
                            message = "true : you should make your payment within 4 days";
                        }
                        /** manual payment received*/
                        else if (bookingActionDto.getStatus().equals(PaySubEventEnum.PAY.value()) && isAdmin) {
                            isAllowed = true;
                            message = "true : your are successfully paid";
                            if (bookingActionDto.getProofFromGuest()!=null) {
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_MANUAL_PAID,booking.getOrderId()));
                                PaymentVerifyDto paymentVerify = new PaymentVerifyDto();
                                paymentVerify.setBookingId(booking.getId());
                                paymentVerify.setProofFromGuest(bookingActionDto.getProofFromGuest());
                                verifyPaymentReceiveFromGuest(paymentVerify);
                            }
                            if (status.equals(StatesEnum.ADVANCE_PAYMENT_PENDING)){
                                LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_MANUAL_ADVANCE_PAID,booking.getOrderId()));
                                event=EventsEnum.PAY_ADVANCE;
                                message = "true : your are successfully paid advance";
                            }
                        }
                        if (bookingActionDto.getStatus().equals(PaySubEventEnum.UNDO_PAY.value()) && isAdmin) {
                            LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_MANUAL_UNDO_PAY,booking.getOrderId()));
                            event = EventsEnum.UNDO_PAYMENT;
                            isAllowed = true;
                            message = "true : your are successfully unpaid";
                        }

                        break;
                    }
                    default:
                        break;
                }

            } else {
                if (event.equals(EventsEnum.CANCEL)) {
                    int refund=findRefund(booking.getId());
                    response.put("refund", refund);
                    LOGGER.info(String.format(LogMessages.BS_BOOKING_ACTION_CANCEL,booking.getOrderId(),bookingActionDto.getRefund(),refund));
                }
                isAllowed = true;
            }

            if (isAllowed) {
                stateMachine.sendEvent(event);
            }
        } else
            message = "false : you are not authorize to do this action ";


        response.put("old_state", status);
        response.put("new_state", stateMachine.getState().getId().toString());
        if (bookingActionDto.getUserId() != null)
            response.put("action_taker", bookingActionDto.getUserId());
        response.put("id",booking.getId());
        response.put("message", message);

        return response;
    }


    /**
     * Get state machine.
     *
     * @param state             the state
     * @param bookingContextDto the booking context dto
     * @return the stateMachine<stateEnum,eventEnum>
     * @throws Exception the exception
     */
    private StateMachine<StatesEnum, EventsEnum> getStateMachine(
            StatesEnum state, BookingContextDto bookingContextDto)
            throws Exception {
        LOGGER.info(String.format(LogMessages.BS_STATE_MACHINE_GET));
        StateMachineBuilder.Builder<StatesEnum, EventsEnum> builder = StateMachineBuilder
                .builder();

        builder.configureConfiguration()
                .withConfiguration()
                .beanFactory(applicationContext.getAutowireCapableBeanFactory())
                .taskExecutor(new SyncTaskExecutor())
                .taskScheduler(new ConcurrentTaskScheduler())
                .listener(
                        new StateMachineListenerAdapter<StatesEnum, EventsEnum>() {
                            @Override
                            public void stateChanged(
                                    State<StatesEnum, EventsEnum> from,
                                    State<StatesEnum, EventsEnum> to) {
                                if (from != null && to != null)
                                    LOGGER.info(LogMessages.BS_STATE_CHANGED, from.getId(), to.getId());
                            }
                        }).autoStartup(true);

        builder.configureTransitions().withExternal()
                .source(StatesEnum.INITIATED)
                .target(StatesEnum.PAYMENT_DONE).event(EventsEnum.PAY)
                .action(new PaymentEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.INITIATED)
                .target(StatesEnum.PENDING_PAYMENT).event(EventsEnum.WAIT_FOR_PAY)
                .action(new ManualBookingEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.INITIATED)
                .target(StatesEnum.ADVANCE_PAYMENT_PENDING).event(EventsEnum.WAIT_FOR_PAY_ADVANCE)
                .action(new PendingAdvanceActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.INITIATED)
                .target(StatesEnum.ADVANCE_PAYMENT_DONE).event(EventsEnum.PAY_ADVANCE)
                .action(new PayAdvanceEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.ADVANCE_PAYMENT_PENDING)
                .target(StatesEnum.ADVANCE_PAYMENT_DONE).event(EventsEnum.PAY_ADVANCE)
                .action(new PayAdvanceEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PENDING_PAYMENT)
                .target(StatesEnum.PAYMENT_DONE).event(EventsEnum.PAY)
                .action(new PaymentEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.INITIATED)
                .target(StatesEnum.DISCARDED).event(EventsEnum.DISCARD)
                .action(new DiscardEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.INITIATED)
                .target(StatesEnum.DISCARDED).event(EventsEnum.CANCEL)
                .action(new DiscardEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PENDING_PAYMENT)
                .target(StatesEnum.CANCELLED).event(EventsEnum.CANCEL)
                .action(new CancelEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.ADVANCE_PAYMENT_PENDING)
                .target(StatesEnum.CANCELLED).event(EventsEnum.CANCEL)
                .action(new CancelEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.ADVANCE_PAYMENT_DONE)
                .target(StatesEnum.CANCELLED).event(EventsEnum.CANCEL)
                .action(new CancelEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PAYMENT_DONE)
                .target(StatesEnum.CANCELLED).event(EventsEnum.CANCEL)
                .action(new CancelEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PENDING_PAYMENT)
                .target(StatesEnum.EXPIRED).event(EventsEnum.EXPIRE)
                .action(new ExpireEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.ADVANCE_PAYMENT_PENDING)
                .target(StatesEnum.EXPIRED).event(EventsEnum.EXPIRE)
                .action(new ExpireEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PAYMENT_DONE)
                .target(StatesEnum.CONFIRMED).event(EventsEnum.CONFIRM)
                .action(new ComfirmEventActionServiceImpl(bookingContextDto))
                .and().withExternal().source(StatesEnum.PAYMENT_DONE)
                .target(StatesEnum.PENDING_PAYMENT).event(EventsEnum.UNDO_PAYMENT)
                .action(new PaymentUndoEventActionServiceImpl(bookingContextDto));

        builder.configureStates().withStates().initial(state)
                .states(EnumSet.allOf(StatesEnum.class));
        return builder.build();
    }

    /**
     * Get bookings.
     *
     * @param user the user
     * @return the list<booking>
     */

    /**
     * Get booking context.
     *
     * @param booking          the booking
     * @param bookingActionDto the booking action dto
     * @return the Booking context dto
     */
    private BookingContextDto buildBookingContext(Booking booking,
                                                  BookingActionDto bookingActionDto) {
        BookingContextDto bookingContextDto = new BookingContextDto();
        bookingContextDto.setBookingDao(bookingDao);
        bookingContextDto.setBooking(booking);
        bookingContextDto.setSpaceDao(spaceDao);
        bookingContextDto.setUserDao(userDao);
        bookingContextDto.setEventPublisher(eventPublisher);
        bookingContextDto.setBookingActionDto(bookingActionDto);
        bookingContextDto.setBookingSpaceExtraAmenityDao(bookingSpaceExtraAmenityDao);
        bookingContextDto.setAmenityDao(amenityDao);
        bookingContextDto.setBookingService(this);
        bookingContextDto.setEmailSender(emailSender);
        bookingContextDto.setEmailService(emailService);

        return bookingContextDto;

    }

    /**
     * Get hiring charge.
     *  NOT USING METHOD NEED TO BE REMOVED
     * @param bookingId the booking id
     * @return the double
     */
    @Override
    @Transactional
    public Double getHiringCharge(Integer bookingId) {
        LOGGER.info("getHiringCharge method in BookingServiceImpl-----> get call");
        Booking booking = bookingDao.read(bookingId);
        Space space = spaceDao.read(booking.getSpace());
        Set<BookingSpaceExtraAmenity> bookingExtraAmenities = booking.getBookingSpaceExtraAmenities();
        Set<SpaceExtraAmenity> spaceExtraAmenities = space.getExtraAmenities();

        //  Long duration= TimeUnit.MILLISECONDS.toHours(booking.getToDate().getTime()- booking.getFromDate().getTime());
        Long duration = 100000L;
        Double base = duration * space.getRatePerHour();
        Double extra = 0.0;
        for (BookingSpaceExtraAmenity bookingExtraAmenity : bookingExtraAmenities) {
            for (SpaceExtraAmenity spaceExtraAmenity : spaceExtraAmenities) {
                if (bookingExtraAmenity.getPk1().getAmenity().getId().equals(spaceExtraAmenity.getPk().getAmenity().getId())) {
                    if (spaceExtraAmenity.getAmenityUnit().equals(AmenityUnitEnum.PER_PERSON.value()) || spaceExtraAmenity.getAmenityUnit().equals(AmenityUnitEnum.PER_UNIT.value()))
                        extra = extra + bookingExtraAmenity.getNumber() * bookingExtraAmenity.getRate();
                    else
                        extra = extra + bookingExtraAmenity.getNumber() * bookingExtraAmenity.getRate() * duration;
                }
            }
        }


        //Double charge = bookingDao.getBookingCharge(bookingId);
        return base + extra;
    }


    /**
     * is cancelled after paid.
     *
     * @param bookingId the booking id
     * @return the boolean
     */
    @Override
    @Transactional
    public Boolean isCancelledAfterPaid(Integer bookingId) {
        LOGGER.info(String.format(LogMessages.BS_IS_CANCEL_AFTER_PAID,bookingId));
        return bookingDao.isCancelledAfterPaid(bookingId);
    }


    /**
     * get all reservation ststus.
     *
     * @return the list<reservation status>
     */
    @Override
    @Transactional
    public List<ReservationStatusDto> getAllreservationStatus() {
        LOGGER.info(String.format(LogMessages.BS_RESERVATION_GET_ALL));
        List<ReservationStatusDto> reservationStatusDtoList = new ArrayList<>();
        List<ReservationStatus> reservationStatuses = reservationStatusDao.getReservationSatues();
        if (reservationStatuses != null) {
            for (ReservationStatus reservationStatus : reservationStatuses) {
                reservationStatusDtoList.add(reservationStatus.build());
            }
            return reservationStatusDtoList;
        }
        return Collections.emptyList();
    }

    /**
     * mark as seen notification.
     *
     * @param bookingId         the booking id
     * @param reservationStatus the reservation status
     * @return the string
     */
    public String markAsSeenNotification(Integer bookingId, Integer reservationStatus) {
        LOGGER.info(String.format(LogMessages.BS_MARK_AS_SEEN_NOTIFICATION,bookingId,reservationStatus));
        BookingHistory bookingHistory = bookingHistoryDao.getBookingHistory(bookingId, reservationStatus);
        bookingHistory.setNotificationSeen(BooleanEnum.TRUE.value());
        bookingHistoryDao.update(bookingHistory);
        return "success";
    }


    /**
     * get user logs.
     *
     * @param userId the user id
     * @return the list of user logs
     */
    @Override
    @Transactional
    public List<UserLogDetails> getUserLogs(Integer userId) {
        List<UserLogDetails> userLogDetailsList = new ArrayList<>();

        List<BookingHistory> bookingHistorys = bookingHistoryDao.getHisttoryByUserId(userId);
        for (BookingHistory bookingHistory : bookingHistorys) {
            if (!bookingHistory.getReservationStatus().equals(StatesEnum.INITIATED.value())) {
                Booking booking = bookingDao.read(bookingHistory.getBookingId());
                User user = userDao.read(booking.getUser());
                Space space = spaceDao.read(booking.getSpace());
                User owner = userDao.read(space.getUser());
                String events[] = {"request", "PAY", "CANCEL", "CONFIRM", "EXPIRE"};
                UserLogDetails userLogDetail = new UserLogDetails();
                userLogDetail.setBookingId(bookingHistory.getBookingId());
                userLogDetail.setUser(user.getName());
                userLogDetail.setOwner(owner.getName());
                userLogDetail.setOwned(Objects.equals(userId, owner.getId()));
                userLogDetail.setEvent(events[bookingHistory.getReservationStatus() - 2]);
                userLogDetail.setSpaceName(space.getName());
                userLogDetail.setTime(bookingHistory.getCreatedAt().toString());
                userLogDetailsList.add(userLogDetail);
            }
        }
        return userLogDetailsList;
    }

    /**
     * add host manual booking details.
     *
     * @param spaceUnavailabilityDto the space unavailability dto
     * @param userId                 the user id
     * @param isAdmin                the is admin
     * @return the list of user logs
     */
    public Integer addHostManualBookingDetails(SpaceUnavailabilityDto spaceUnavailabilityDto, Integer userId, boolean isAdmin) {
        LOGGER.info(String.format(LogMessages.BS_MANUAL_BOOK_ADD,spaceUnavailabilityDto.getSpace()));
        spaceUnavailabilityDto.setIsManual(UnavailablityTypeEnum.MANUAL_BOOKING.value());
        SpaceUnavailability spaceUnavailability = SpaceUnavailability.build(spaceUnavailabilityDto);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        BookingDto bookingDto=new BookingDto();
        bookingDto.setSpace(spaceUnavailability.getSpace());
        Set<CommonDto> bookingSlots=new HashSet<>();
        CommonDto   bookingSlot=new CommonDto();
        bookingSlot.setFromDate(df.format(spaceUnavailability.getFromDate()));
        bookingSlot.setToDate(df.format(spaceUnavailability.getToDate()));
        bookingSlots.add(bookingSlot);
        bookingDto.setDates(bookingSlots);

//&& spaceUnavailability.getFromDate().getTime()>commonService.getCurrentTime()
        if (isUserAuthorized(userId, spaceUnavailabilityDto.getSpace(), isAdmin)  && spaceService.isNotBlockTime(bookingDto) && spaceUnavailability.getToDate().getTime()>spaceUnavailability.getFromDate().getTime()) {
                spaceUnavailabilityDao.create(spaceUnavailability);
                handleParentChildUnavailablity(spaceUnavailability.getSpace(), spaceUnavailability, BooleanEnum.TRUE.value());
                return spaceUnavailability.getId();

        } else
            return 0;
    }


    /**
     * add host manual booking details.
     *
     * @param spaceUnavailabilityDto the space unavailability dto
     * @param userId                 the user id
     * @param isAdmin                the is admin
     * @return the list of user logs
     */
    public Integer editHostManualBookingDetails(SpaceUnavailabilityDto spaceUnavailabilityDto, Integer userId, boolean isAdmin) {

        try {
            LOGGER.info(String.format(LogMessages.BS_MANUAL_BOOK_UPDATE,spaceUnavailabilityDto.getId()));
            DateFormat df = new SimpleDateFormat(com.eventspace.util.Constants.PATTERN_1);
            SpaceUnavailability spaceUnavailability = spaceUnavailabilityDao.read(spaceUnavailabilityDto.getId());
            spaceUnavailability.setIsBlocked(BooleanEnum.FALSE.value());
            spaceUnavailabilityDao.update(spaceUnavailability);

            DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            BookingDto bookingDto=new BookingDto();
            bookingDto.setSpace(spaceUnavailability.getSpace());
            Set<CommonDto> bookingSlots=new HashSet<>();
            CommonDto   bookingSlot=new CommonDto();
            bookingSlot.setFromDate(df2.format(spaceUnavailability.getFromDate()));
            bookingSlot.setToDate(df2.format(spaceUnavailability.getToDate()));
            bookingSlots.add(bookingSlot);
            bookingDto.setDates(bookingSlots);

            if (isUserAuthorized(userId, spaceUnavailability.getSpace(), isAdmin) && spaceUnavailability.getIsManual().equals(BooleanEnum.TRUE.value()) && spaceService.isNotBlockTime(bookingDto) && spaceUnavailability.getToDate().getTime()>spaceUnavailability.getFromDate().getTime()) {


                spaceUnavailability.setFromDate(df.parse(spaceUnavailabilityDto.getFromDate()));
                spaceUnavailability.setToDate(df.parse(spaceUnavailabilityDto.getToDate()));
                spaceUnavailability.setCost(spaceUnavailabilityDto.getCost());
                spaceUnavailability.setDateBookingMade(spaceUnavailabilityDto.getDateBookingMade());
                spaceUnavailability.setExtrasRequested(spaceUnavailabilityDto.getExtrasRequested());
                spaceUnavailability.setGuestContactNumber(spaceUnavailabilityDto.getGuestContactNumber());
                spaceUnavailability.setGuestEmail(spaceUnavailabilityDto.getGuestEmail());
                spaceUnavailability.setGuestName(spaceUnavailabilityDto.getGuestName());
                spaceUnavailability.setNoOfGuests(spaceUnavailabilityDto.getNoOfGuests());
                spaceUnavailability.setNote(spaceUnavailabilityDto.getNote());
                spaceUnavailability.setTitle(spaceUnavailabilityDto.getEventTitle());
                spaceUnavailability.setRemarks(spaceUnavailabilityDto.getRemark());

                if (spaceUnavailabilityDto.getEventTypeId() != null) {
                    EventType eventType = new EventType();
                    eventType.setId(spaceUnavailabilityDto.getEventTypeId());
                    spaceUnavailability.setEventType(eventType);
                }

                if (spaceUnavailabilityDto.getSeatingArrangementId() != null) {
                    SeatingArrangement seatingArrangement = new SeatingArrangement();
                    seatingArrangement.setId(Integer.parseInt(spaceUnavailabilityDto.getSeatingArrangementId()));
                    spaceUnavailability.setSeatingArrangement(seatingArrangement);
                }

                if (spaceUnavailabilityDto.getReservationStatus()!=null){
                    ReservationStatus reservationStatus=new ReservationStatus();
                    reservationStatus.setId(spaceUnavailabilityDto.getReservationStatus().getId());
                    spaceUnavailability.setReservationStatus(reservationStatus);
                }

                spaceUnavailability.setIsBlocked(BooleanEnum.TRUE.value());
                spaceUnavailabilityDao.update(spaceUnavailability);

                handleParentChildUnavailablity(spaceUnavailability.getSpace(), spaceUnavailability, BooleanEnum.FALSE.value());
                handleParentChildUnavailablity(spaceUnavailability.getSpace(), spaceUnavailability, BooleanEnum.TRUE.value());

                return BooleanEnum.TRUE.value();
            } else
                return BooleanEnum.FALSE.value();
        } catch (ParseException ex) {
            return 0;
        }
    }

    @Override
    public SpaceUnavailabilityDto getManualBooking(Integer id, Integer userId, boolean isAdmin) {
        LOGGER.info(String.format(LogMessages.BS_MANUAL_BOOK_GET,id));
        SpaceUnavailability spaceUnavailability = spaceUnavailabilityDao.read(id);
        Space space=spaceDao.read(spaceUnavailability.getSpace());
        if (isUserAuthorized(userId, spaceUnavailability.getSpace(), isAdmin)) {
            SpaceUnavailabilityDto spaceUnavailabilityDto=spaceUnavailability.build();
            spaceUnavailabilityDto.setCancellationPolicy(cancellationPolicyDao.read(space.getCancellationPolicy()).build());
            List<AmenityDto> amenityIdList = new ArrayList<>();
            for (Amenity amenity : space.getAmenity()) {
                amenityIdList.add(amenity.buildneeded());
            }
            spaceUnavailabilityDto.setAmenities(amenityIdList);
            return spaceUnavailabilityDto;
        } else
            return null;
    }

    /**
     * add host manual booking details.
     *
     * @param manualBookingId the space event id
     * @param userId  the user id
     * @param isAdmin the is admin
     * @return the list of user logs
     */
    public Integer deleteHostManualBookingDetails(Integer manualBookingId, Integer userId, boolean isAdmin) {

        LOGGER.info(String.format(LogMessages.BS_MANUAL_BOOK_DELETE,manualBookingId));
        SpaceUnavailability spaceUnavailability = spaceUnavailabilityDao.read(manualBookingId);
        if (isUserAuthorized(userId, spaceUnavailability.getSpace(), isAdmin) && spaceUnavailability.getIsManual().equals(UnavailablityTypeEnum.MANUAL_BOOKING.value())) {
            spaceUnavailability.setIsBlocked(0);
            spaceUnavailabilityDao.update(spaceUnavailability);

            handleParentChildUnavailablity(spaceUnavailability.getSpace(), spaceUnavailability, BooleanEnum.FALSE.value());

            return BooleanEnum.TRUE.value();
        } else
            return BooleanEnum.FALSE.value();
    }

    private List<MenuFileDto> getBookingMenus(Set<BlockAvailablity> blockAvailablities, Set<BookingSlots> slots) {
        List<MenuFileDto> menus = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        if (!blockAvailablities.isEmpty() && !slots.isEmpty()) {
            for (BlockAvailablity blockAvailablity : blockAvailablities) {
                for (BookingSlots slot : slots) {
                    Integer day = slot.getFromDate().getDay();
                    if (day <= 5)
                        day = 8;

                    if (day.equals(blockAvailablity.getDay()) && df.format(slot.getFromDate()).equals(blockAvailablity.getFrom().toString()) &&
                            df.format(slot.getToDate()).equals(blockAvailablity.getTo().toString()) && !blockAvailablity.getMenuFiles().isEmpty()){
                            for (MenuFiles menuFiles:blockAvailablity.getMenuFiles())
                                menus.add(menuFiles.build());
                        }


                }
            }
        }

        return menus;
    }

    /**
     * is user authorized.
     *
     * @param userId  the user id
     * @param spaceId the space id
     * @param isAdmin the is admin
     * @return the list of user logs
     */
    private boolean isUserAuthorized(Integer userId, Integer spaceId, boolean isAdmin) {
        Space space = spaceDao.read(spaceId);
        User user=userDao.read(userId);
        return isAdmin || space.getUser().equals(userId) || user.getRole().equals("ADMIN");
    }

    private boolean isUserAuthorizedForBooking(Integer userId, Integer bookingId,boolean isManual, boolean isAdmin) {
        boolean response;
        if (isManual) {
            SpaceUnavailability spaceUnavailability = spaceUnavailabilityDao.read(bookingId);
            Space space=spaceDao.read(spaceUnavailability.getSpace());
            User user=userDao.read(userId);
            response= isAdmin || space.getUser().equals(userId)  || user.getRole().equals("ADMIN");

        } else {
            Booking booking = bookingDao.read(bookingId);
            Space space = spaceDao.read(booking.getSpace());
            User user = userDao.read(userId);
            response= isAdmin || space.getUser().equals(userId) || booking.getUser().equals(userId) || user.getRole().equals("ADMIN");
        }
        return response;
    }

    public void expireBooking(Integer bookingId) {
        LOGGER.info(String.format(LogMessages.BS_BOOKING_EXPIRE,bookingId));
        BookingExpireDetails bookingExpireDetails = bookingExpireDetailsDao.getByBookingId(bookingId);
        bookingExpireDetails.setExpired(BooleanEnum.TRUE.value());
        bookingExpireDetailsDao.update(bookingExpireDetails);
        LOGGER.info(String.format(LogMessages.BS_BOOKING_EXPIRED,bookingId));
    }

    @Override
    public String paymentCallback(PaymentCallbackDto paymentCallbackDto, boolean isMobile) {
        Integer orderId = getBookingIdFromOrderId(paymentCallbackDto.getOrderID());
        Integer responseCode = paymentCallbackDto.getResponseCode();
        Integer reasonCode=paymentCallbackDto.getReasonCode();

        LOGGER.info(String.format(LogMessages.BS_PAYMENT_CALLBACK,paymentCallbackDto.getOrderID()));

        BookingActionDto bookingActionDto = new BookingActionDto();
        bookingActionDto.setBooking_id(orderId);
        bookingActionDto.setEvent(EventsEnum.PAY.value());
        bookingActionDto.setMethod("IPG");
        bookingActionDto.setPaymentSuccessCode(responseCode);

        try {
            bookingActions(bookingActionDto, true);
        } catch (Exception e) {
            //exception
        }
        String url = "";

        if (isMobile)
            url = apiUrl+paymentRedirectUrl + "?code="+responseCode+"&reasonCode="+reasonCode+"&orderId="+orderId+"&status=";
        else
            url = paymentServerUrl+"/api/payment/response" + "?code="+responseCode+"&reasonCode="+reasonCode+"&orderId="+orderId+"&status=";
        url = url + URLEncoder.encode(paymentCallbackDto.getReasonCodeDesc());
        LOGGER.info(String.format(LogMessages.BS_PAYMENT_CALLBACK_URL,url));
        return url;
    }

    @Override
    public void addBookingToSpaceUnavailablity(Integer bookingId,Set<BookingSlots> bookingSlotsSet, Integer spaceId) {
        LOGGER.info("addBookingToSpaceUnavailablity method ---> get called");

        spaceId=spaceService.getLinkedSpace(spaceId);

        for (BookingSlots bookingSlots : bookingSlotsSet) {
            SpaceUnavailability spaceUnavailability = new SpaceUnavailability();
            spaceUnavailability.setSpace(spaceId);
            spaceUnavailability.setBookingId(bookingId);
            spaceUnavailability.setToDate(bookingSlots.getToDate());
            spaceUnavailability.setFromDate(bookingSlots.getFromDate());
            spaceUnavailability.setIsBlocked(BooleanEnum.TRUE.value());
            spaceUnavailability.setIsManual(UnavailablityTypeEnum.GUEST_BOOKING.value());
            spaceUnavailabilityDao.create(spaceUnavailability);

            handleParentChildUnavailablity(spaceId, spaceUnavailability, BooleanEnum.TRUE.value());

        }


    }

    @Override
    public void removeBookingFromSpaceUnavailablity(Integer bookingId) {
        Booking booking = bookingDao.read(bookingId);

        booking.setSpace(spaceService.getLinkedSpace(booking.getSpace()));

        if (!booking.getBookingSlots().isEmpty()) {
            for (BookingSlots bookingSlots : booking.getBookingSlots()) {
                SpaceUnavailability spaceUnavailability = spaceUnavailabilityDao.findSpaceUnavailability(booking.getSpace(), bookingSlots.getFromDate(), bookingSlots.getToDate());
                if (spaceUnavailability!=null) {
                    spaceUnavailability.setIsBlocked(BooleanEnum.FALSE.value());
                    spaceUnavailabilityDao.update(spaceUnavailability);

                    handleParentChildUnavailablity(booking.getSpace(), spaceUnavailability, BooleanEnum.FALSE.value());
                }
            }
            LOGGER.info("booking {} has been discarded", bookingId);
        }
    }

    //o-update 1-create
    public void handleParentChildUnavailablity(Integer spaceId, SpaceUnavailability spaceUnavailability, Integer block) {
        Space space = spaceDao.read(spaceId);
        Integer parent = space.getParent();
        List<Space> childs = spaceDao.getChilds(spaceId);

        if (parent != null) {

            Space parentSpace=spaceDao.read(spaceService.getLinkedSpace(parent));
            switch (block) {
                //unblock
                case 0:
                    SpaceUnavailability newSpaceUnavailability1 = spaceUnavailabilityDao.findSpaceUnavailability(parentSpace.getId(), spaceUnavailability.getFromDate(), spaceUnavailability.getToDate());
                        if (!Optional.ofNullable(newSpaceUnavailability1).isPresent())
                            BeanUtils.copyProperties(spaceUnavailability, newSpaceUnavailability1);
                        newSpaceUnavailability1.setIsBlocked(BooleanEnum.FALSE.value());
                        if (newSpaceUnavailability1.getIsManual().equals(UnavailablityTypeEnum.CHILD_BLOCK_FOR_BOOKING.value()))
                            spaceUnavailabilityDao.update(newSpaceUnavailability1);
                    break;
                    //block
                case 1:
                    SpaceUnavailability newSpaceUnavailability2 = new SpaceUnavailability();
                    BeanUtils.copyProperties(spaceUnavailability, newSpaceUnavailability2);
                    newSpaceUnavailability2.setSpace(parentSpace.getId());
                    newSpaceUnavailability2.setIsManual(UnavailablityTypeEnum.CHILD_BLOCK_FOR_BOOKING.value());
                    spaceUnavailabilityDao.create(newSpaceUnavailability2);
                    break;
                default:
                    break;
            }
        }

        if (!childs.isEmpty()) {
            switch (block) {
                //unblock
                case 0:
                    for (Space child : childs) {
                        Integer childId=spaceService.getLinkedSpace(child.getId());
                        SpaceUnavailability newSpaceUnavailability = spaceUnavailabilityDao.findSpaceUnavailability(childId, spaceUnavailability.getFromDate(), spaceUnavailability.getToDate());
                            if (newSpaceUnavailability==null)
                                BeanUtils.copyProperties(spaceUnavailability, newSpaceUnavailability);
                            newSpaceUnavailability.setIsBlocked(BooleanEnum.FALSE.value());
                            if (newSpaceUnavailability.getIsManual().equals(UnavailablityTypeEnum.CHILD_BLOCK_FOR_BOOKING.value()))
                                spaceUnavailabilityDao.update(newSpaceUnavailability);

                    }
                    break;
                    //block
                case 1:
                    for (Space child : childs) {
                        Integer childId=spaceService.getLinkedSpace(child.getId());
                        SpaceUnavailability newSpaceUnavailability = new SpaceUnavailability();
                        BeanUtils.copyProperties(spaceUnavailability, newSpaceUnavailability);
                        newSpaceUnavailability.setSpace(childId);
                        newSpaceUnavailability.setIsManual(UnavailablityTypeEnum.CHILD_BLOCK_FOR_BOOKING.value());
                        spaceUnavailabilityDao.create(newSpaceUnavailability);
                    }
                    break;
                default:
                    break;


            }


        }
    }


    private String generateOrderId(Integer bookingId) {
        return orderIdPrefix + String.format(orderIdDigit, bookingId);
    }

    private String generateManualOrderId(Integer bookingId) {
        return hostOrderIdPrefix + String.format(orderIdDigit, bookingId);
    }

    public Integer getBookingIdFromOrderId(String orderId) {
        if (orderId.contains("-")){
            return Integer.valueOf(orderId.substring(0,orderId.indexOf(IpgConcatChar)).replace(orderIPGPrefix,""));}
        else if(orderId.contains(orderIPGPrefix)){
            return Integer.valueOf(orderId.replace(orderIPGPrefix, ""));
        }else{
            return Integer.valueOf(orderId.replace(orderIdPrefix, ""));
        }
    }

    @Override
    public Map<String, Object> addRemark(RemarkDto remarkDto, Integer userId) {
        Map<String, Object> res=new HashMap<>();
        if (remarkDto.getIsManual()){
          SpaceUnavailability  spaceUnavailability=spaceUnavailabilityDao.read(remarkDto.getBookingId());
            spaceUnavailability.setRemarks(remarkDto.getRemark());
            spaceUnavailabilityDao.update(spaceUnavailability);
            res.put("response",true);

        }
        else if(!remarkDto.getIsManual()){
            Booking booking = bookingDao.read(remarkDto.getBookingId());
            booking.setRemarks(remarkDto.getRemark());
            bookingDao.update(booking);
            res.put("response",true);
        }
        else
            res.put("response",false);
        return res;
    }

    /**
     * get all bookings.
     *
     * @param page the page
     * @return the list<booking>
     */
    public List<AdminBookingDetailsDto> getAllBookings(int page) {
        LOGGER.info("getAllBookings method in BookingServiceImpl-----> get call");
        List<AdminBookingDetailsDto> bookings = new ArrayList<>();
        List<Booking> bookingList = bookingDao.getAllBooking(page);

        for (Booking booking : bookingList) {
            AdminBookingDetailsDto bookingDto = generateAdminBookingDto(booking.getId());
            bookings.add(bookingDto);
        }
        return bookings;
    }


    @Override
    @Transactional
    public List<BookingDetailsDto> getBookings(Integer user) {
        LOGGER.info("getBookings method in BookingServiceImpl-----> get call");
        List<BookingDetailsDto> dtoList = new ArrayList<>();
        List<Booking> dom = bookingDao.getBookings(user);
        List<SpaceUnavailability> manualBookings = spaceUnavailabilityDao.getHostManualBookings(user);
        for (Booking booking : dom) {
            BookingDetailsDto dto = generateBookingDetails(booking.getId());
            dtoList.add(dto);
        }
        for (SpaceUnavailability spaceUnavailability : manualBookings) {
            BookingDetailsDto dto = generateManualBookingDetails(spaceUnavailability.getId());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public List<BookingDetailsDto> getUserBooking(Integer userId) {
        List<Booking> bookings = bookingDao.getBookingByUserId(userId);
        List<BookingDetailsDto> dtoList = new ArrayList<>();

        for (Booking booking:bookings){
            BookingDetailsDto bookingDetailsDto=generateSimpleBookingDetails(booking.getId());
            dtoList.add(bookingDetailsDto);
        }
        return dtoList;
    }

    @Override
    public List<BookingDetailsDto> getHostBooking(Integer userId) {
        List<Booking> bookings = bookingDao.getBookingByHostId(userId);
        List<SpaceUnavailability> manualBookings = spaceUnavailabilityDao.getHostManualBookings(userId);
        List<BookingDetailsDto> dtoList = new ArrayList<>();

        for (Booking booking:bookings){
            BookingDetailsDto bookingDetailsDto=generateSimpleBookingDetails(booking.getId());
            dtoList.add(bookingDetailsDto);
        }
        for (SpaceUnavailability spaceUnavailability : manualBookings) {
            BookingDetailsDto dto = generateSimpleManualBookingDetails(spaceUnavailability.getId());
            dtoList.add(dto);
        }
        return dtoList;
    }

    @Override
    public List<BookingDetailsDto> getSpaceBooking(Integer spaceId,Integer userId) {
        List<Booking> bookings = bookingDao.getBookingsBySpaceId(spaceId);
        List<SpaceUnavailability> manualBookings = spaceUnavailabilityDao.getSpaceManualBookings(spaceId);
        List<BookingDetailsDto> dtoList = new ArrayList<>();

        if (isUserAuthorized(userId,spaceId,false)) {
            for (Booking booking : bookings) {
                BookingDetailsDto bookingDetailsDto = generateSimpleBookingDetails(booking.getId());
                dtoList.add(bookingDetailsDto);
            }
            for (SpaceUnavailability spaceUnavailability : manualBookings) {
                BookingDetailsDto dto = generateSimpleManualBookingDetails(spaceUnavailability.getId());
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

    @Override
    public BookingDetailsDto getABooking(BookingCommonDto bookingCommonDto,Integer userId) {
        if (isUserAuthorizedForBooking(userId,bookingCommonDto.getBookingId(),bookingCommonDto.getIsManual(),false)) {
            if (bookingCommonDto.getIsManual())
                return generateManualBookingDetails(bookingCommonDto.getBookingId());
            else
                return generateBookingDetails(bookingCommonDto.getBookingId());
        }
        else
            return null;

    }

    /**
     * Get booking.
     *
     * @param bookingId the booking id
     * @param userId    the user id
     * @return the booking details dto
     */
    @Override
    @Transactional
    public BookingDetailsDto getBooking(Integer bookingId, Integer userId) {
        LOGGER.info("getBooking method in BookingServiceImpl-----> get call");
        BookingDetailsDto dto =generateBookingDetails(bookingId);
        //dto.setIsOwned(userId != booking.getUser().intValue());
        // else
        //   dto.setBookingCharge(String.format("%.2f",getHiringCharge(bookingId)));
        //dto.setIsReviewed(reviewsDao.checkReviewByBookingId(bookingId));

         /*   if (bookingDao.isCancelledAfterPaid(bookingId))
                dto.setCancelledDate(bookingDao.getCancelledDate(bookingId).toString());
            else
                dto.setCancelledDate(null);

            dto.setActionTaker(booking.getActionTaker());
            dto.setSpace(spaceService.getSpace(booking.getSpace()));
            */
        return dto;
    }

    @Override
    public BookingDetailsDto generateSimpleBookingDetails(Integer bookingId){
        BookingDetailsDto bookingDetailsDto=new BookingDetailsDto();
        Booking booking = bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        User user=userDao.read(booking.getUser());
        BookingPayLater bookingPayLater=booking.getBookingPayLater();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        bookingDetailsDto.setId(bookingId);
        bookingDetailsDto.setOrderId(generateOrderId(bookingId));
        bookingDetailsDto.setISManual(false);
        bookingDetailsDto.setIsBlock(space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()));
        bookingDetailsDto.setIsReviewed(reviewsDao.checkReviewByBookingId(bookingId));
        Reviews reviews=reviewsDao.getReviewByBookingId(bookingId);
        if (reviews!=null)
            bookingDetailsDto.setReview(reviews.build());
        if (booking.getBookingCharge() != null)
            bookingDetailsDto.setBookingCharge(String.format("%.2f", booking.getBookingCharge()));

        if (booking.getReservationStatus().equals(StatesEnum.ADVANCE_PAYMENT_PENDING.value())) {
            booking.setReservationStatus(StatesEnum.PENDING_PAYMENT.value());
        }else if (booking.getReservationStatus().equals(StatesEnum.PAYMENT_DONE.value())){
            booking.setReservationStatus(StatesEnum.PAYMENT_DONE.value());
        }

        bookingDetailsDto.setReservationStatus(reservationStatusDao.read(booking.getReservationStatus()).build());
        if (Optional.ofNullable(bookingPayLater).isPresent() && !bookingPayLater.getExpire()){
            bookingDetailsDto.setIsPayLater(true);
        }

        Set<BookingSlots> bookingSlots = booking.getBookingSlots();
        Set<CommonDto> slots = new HashSet<>(0);
        for (BookingSlots bookingSlot : bookingSlots) {
            CommonDto slot = new CommonDto();
            slot.setFromDate(df.format(bookingSlot.getFromDate()));
            slot.setToDate(df.format(bookingSlot.getToDate()));
            slots.add(slot);
        }
        bookingDetailsDto.setDates(slots);

        BookingHistory bookingHistory=bookingHistoryDao.getBookingHistory(booking.getId(),StatesEnum.INITIATED.value());
        if (bookingHistory!=null)
            bookingDetailsDto.setBookedDate(df.format(bookingHistory.getCreatedAt()));

        Map<String, Object> spaceDetails = new HashMap<>();
        spaceDetails.put("id", space.getId());
        spaceDetails.put("name", space.getName());
        spaceDetails.put("addressLine1", space.getAddressLine1());
        spaceDetails.put("addressLine2", space.getAddressLine2());
        spaceDetails.put("cancellationPolicy", cancellationPolicyDao.read(space.getCancellationPolicy()).build());
        if (!space.getImages().isEmpty())
            spaceDetails.put("image", space.getImages().iterator().next().getUrl());
        bookingDetailsDto.setSpace(spaceDetails);

        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("name", user.getName());
        bookingDetailsDto.setUser(userDetails);

        return bookingDetailsDto;
    }

    private BookingDetailsDto generateSimpleManualBookingDetails(Integer manualBookingId){
        BookingDetailsDto dto = new BookingDetailsDto();
        SpaceUnavailability spaceUnavailability=spaceUnavailabilityDao.read(manualBookingId);
        Space space = spaceDao.read(spaceUnavailability.getSpace());

        dto.setId(spaceUnavailability.getId());
        dto.setOrderId(generateManualOrderId(manualBookingId));
        dto.setISManual(true);
        dto.setIsBlock(space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()));
        dto.setBookedDate(spaceUnavailability.getDateBookingMade());
        if (spaceUnavailability.getReservationStatus()!=null)
           dto.setReservationStatus(reservationStatusDao.read(spaceUnavailability.getReservationStatus().getId()).build());

        if (spaceUnavailability.getCost() != null && !spaceUnavailability.getCost().equals(""))
            dto.setBookingCharge(spaceUnavailability.getCost());
        Set<CommonDto> slots = new HashSet<>(0);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        CommonDto slot = new CommonDto();
        slot.setFromDate(df.format(spaceUnavailability.getFromDate()));
        slot.setToDate(df.format(spaceUnavailability.getToDate()));
        slots.add(slot);
        dto.setDates(slots);
        dto.setNote(spaceUnavailability.getNote());
        dto.setRemarks(spaceUnavailability.getRemarks());

        Map<String, Object> user = new HashMap<>();
        String guest=spaceUnavailability.getGuestName();
        if (guest==null || guest.equals(""))
            guest="HOST";
        user.put("name", guest);
        dto.setUser(user);


        Map<String, Object> spaceDetails = new HashMap<>();
        spaceDetails.put("id", space.getId());
        spaceDetails.put("name", space.getName());
        spaceDetails.put("addressLine1", space.getAddressLine1());
        spaceDetails.put("addressLine2", space.getAddressLine2());
        spaceDetails.put("cancellationPolicy", cancellationPolicyDao.read(space.getCancellationPolicy()).build());
        if (!space.getImages().isEmpty())
            spaceDetails.put("image", space.getImages().iterator().next().getUrl());
        dto.setSpace(spaceDetails);

        return dto;
    }

    private BookingDetailsDto generateBookingDetails(Integer bookingId){
        BookingDetailsDto bookingDetailsDto=generateSimpleBookingDetails(bookingId);
        Booking booking = bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        User user=userDao.read(booking.getUser());
        BookingPayLater bookingPayLater=booking.getBookingPayLater();

        bookingDetailsDto.setIsBlock(space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()));
        bookingDetailsDto.setOrderId(booking.getOrderId());
        bookingDetailsDto.setRefund(booking.getRefund());
        bookingDetailsDto.setGuestCount(booking.getGuestCount());
        bookingDetailsDto.setAdvance(booking.getAdvance());
        bookingDetailsDto.setDiscount(booking.getDiscount());

        if (booking.getEventType() != null)
            bookingDetailsDto.setEventTypedetail(eventTypeDao.read(booking.getEventType()).build());
        if (booking.getSeatingArrangement() != null)
            bookingDetailsDto.setSeatingArrangement(seatingArrangementDao.read(booking.getSeatingArrangement()).build());
        if (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value()))
            bookingDetailsDto.setMenu(getBookingMenus(space.getBlockAvailablityBlocks(), booking.getBookingSlots()));
        Reviews reviews=reviewsDao.getReviewByBookingId(bookingId);
        if (reviews!=null)
            bookingDetailsDto.setReview(reviews.build());
        if (Optional.ofNullable(bookingPayLater).isPresent() && !bookingPayLater.getExpire()){
            bookingDetailsDto.setIsPayLater(true);
        }

        Map<String, Object>  userDetails= new HashMap<>();
        userDetails.put("name", user.getName());
        userDetails.put("id", user.getId());
        userDetails.put("email", user.getEmail());
        userDetails.put("mobile", user.getPhone());
        bookingDetailsDto.setUser(userDetails);

        bookingDetailsDto.setRemarks(booking.getRemarks());

        Map<String, Object> spaceDetails = new HashMap<>();
        spaceDetails.put("id", space.getId());
        spaceDetails.put("name", space.getName());
        if (!space.getImages().isEmpty())
            spaceDetails.put("image", space.getImages().iterator().next().getUrl());
        spaceDetails.put("addressLine1", space.getAddressLine1());
        spaceDetails.put("addressLine2", space.getAddressLine2());
        spaceDetails.put("cancellationPolicy", cancellationPolicyDao.read(space.getCancellationPolicy()).build());

        List<AmenityDto> amenityIdList = new ArrayList<>();
        for (Amenity amenity : space.getAmenity()) {
            amenityIdList.add(amenity.buildneeded());
        }
        spaceDetails.put("amenities",amenityIdList);
        bookingDetailsDto.setSpace(spaceDetails);

        Set<BookingSpaceExtraAmenity> bookingExtraAmenities = booking.getBookingSpaceExtraAmenities();
        Set<SpaceExtraAmenity> spaceExtraAmenities = spaceDao.read(booking.getSpace()).getExtraAmenities();
        List<Map> extraAmenityList = new ArrayList<>();
        for (BookingSpaceExtraAmenity bookingSpaceExtraAmenity : bookingExtraAmenities) {
            Map<String, Object> extraAmenity = new HashMap<>();
            extraAmenity.put("id", bookingSpaceExtraAmenity.getPk1().getAmenity().getId());
            extraAmenity.put("name", bookingSpaceExtraAmenity.getPk1().getAmenity().getName());
            extraAmenity.put("count", bookingSpaceExtraAmenity.getNumber());
            extraAmenity.put("rate", bookingSpaceExtraAmenity.getRate());
            for (SpaceExtraAmenity extra : spaceExtraAmenities) {
                if (extra.getPk().getAmenity().getId().equals(bookingSpaceExtraAmenity.getPk1().getAmenity().getId())) {
                    extraAmenity.put("amenityUnit", extra.getAmenityUnit());
                    extraAmenity.put("amenityUnitName", amenityUnitsDao.read(extra.getAmenityUnit()).getName());
                }
            }
            extraAmenity.put("icon", bookingSpaceExtraAmenity.getPk1().getAmenity().getIcon());

            extraAmenityList.add(extraAmenity);
        }
        bookingDetailsDto.setExtraAmenityList(extraAmenityList);



        return bookingDetailsDto;
    }


    private BookingDetailsDto generateManualBookingDetails(Integer manualBookingId) {
        BookingDetailsDto dto = generateSimpleManualBookingDetails(manualBookingId);
        SpaceUnavailability spaceUnavailability=spaceUnavailabilityDao.read(manualBookingId);
        Space space = spaceDao.read(spaceUnavailability.getSpace());

        if (spaceUnavailability.getEventType() != null)
            dto.setEventTypedetail(spaceUnavailability.getEventType().build());
        if (spaceUnavailability.getSeatingArrangement() != null)
            dto.setSeatingArrangement(spaceUnavailability.getSeatingArrangement().build());
        if (spaceUnavailability.getNoOfGuests() != null && !spaceUnavailability.getNoOfGuests().equals(""))
            dto.setGuestCount(Integer.valueOf(spaceUnavailability.getNoOfGuests()));

        String guest=spaceUnavailability.getGuestName();
        if (guest==null || guest.equals(""))
            guest="HOST";

        Map<String, Object> user = new HashMap<>();
        user.put("name", guest);
        user.put("email", spaceUnavailability.getGuestEmail());
        user.put("mobile", spaceUnavailability.getGuestContactNumber());
        dto.setUser(user);


        Map<String, Object> spaceDetails = new HashMap<>();
        spaceDetails.put("id", space.getId());
        spaceDetails.put("name", space.getName());
        if (!space.getImages().isEmpty())
            spaceDetails.put("image", space.getImages().iterator().next().getUrl());
        spaceDetails.put("addressLine1", space.getAddressLine1());
        spaceDetails.put("addressLine2", space.getAddressLine2());
        spaceDetails.put("cancellationPolicy", cancellationPolicyDao.read(space.getCancellationPolicy()).build());
        List<AmenityDto> amenityIdList = new ArrayList<>();
        for (Amenity amenity : space.getAmenity()) {
            amenityIdList.add(amenity.buildneeded());
        }
        spaceDetails.put("amenities",amenityIdList);
        spaceDetails.put("extrasRequested",spaceUnavailability.getExtrasRequested());
        dto.setSpace(spaceDetails);

        dto.setRemarks(spaceUnavailability.getRemarks());
        dto.setBookingCharge(spaceUnavailability.getCost());
        return dto;
    }


    public ChargeDetailsDto calculateBookingCharge(BookingDto bookingDto){
        ChargeDetailsDto chargeDetailsDto=new ChargeDetailsDto();
        Booking booking=Booking.build(bookingDto,1);
        Space space=spaceDao.read(bookingDto.getSpace());
        Double slotCharge=0.00;
        Double amenityCharge=0.00;
        /** discount based on space event type*/
        Double discount=0.00;
        Integer noOfHour=0;
        /** discount based on promo code*/
        Double promotionDiscount=0.00;
        Integer noOfSlots=0;

        if (space.getChargeType().equals(ChargeTypeEnum.HOUR_BASE.value())){
            for (BookingSlots bookingSlots:booking.getBookingSlots()){
                Integer slotHour=bookingSlots.getToDate().getHours()-bookingSlots.getFromDate().getHours();
                noOfHour=noOfHour+slotHour;
                Integer day=bookingSlots.getToDate().getDay();
                if(day==0)
                    day=7;

                PerHourAvailablity perHourAvailablity=perHourAvailablityDao.findTheAvailablity(booking.getSpace(),day,new Time(bookingSlots.getFromDate().getTime()),new Time(bookingSlots.getToDate().getTime()));
                if (perHourAvailablity!=null) {
                    slotCharge = slotCharge + slotHour * perHourAvailablity.getCharge();
                    noOfSlots++;
                }
            }
        }

        else if (space.getChargeType().equals(ChargeTypeEnum.BLOCK_BASE.value())){

            for (BookingSlots bookingSlots:booking.getBookingSlots()){
                Integer day=bookingSlots.getToDate().getDay();
                if (day==0)
                    day=7;
                if (day<=5)
                    day=8;


                BlockAvailablity blockAvailablity=blockAvailablityDao.findTheAvailablity(booking.getSpace(),day,new Time(bookingSlots.getFromDate().getTime()),new Time(bookingSlots.getToDate().getTime()));
                if (blockAvailablity!=null) {
                    noOfSlots++;
                    Integer slotHour = blockAvailablity.getTo().getHours() - blockAvailablity.getFrom().getHours();
                    noOfHour = noOfHour + slotHour;

                    if (space.getBlockChargeType().equals(BlockChargeTypeEnum.SPACE_ONLY_CHARGE.value())) {
                        slotCharge = slotCharge + blockAvailablity.getCharge();
                    } else if (space.getBlockChargeType().equals(BlockChargeTypeEnum.PER_QUEST_BASED_CHARGE.value())) {
                        Integer guest=space.getMinParticipantCount();
                        if(guest<booking.getGuestCount())
                            guest=booking.getGuestCount();
                        slotCharge = slotCharge + blockAvailablity.getCharge()*guest;
                    }
                }
            }
        }

        LOGGER.info("slot charge{}",slotCharge);
        for (BookingWithExtraAmenityDto bookingExtraAmenity: bookingDto.getBookingWithExtraAmenityDtoSet()){
            SpaceExtraAmenity spaceExtraAmenity=spaceExtraAmenityDao.findSpaceExtraAmenity(booking.getSpace(),bookingExtraAmenity.getAmenityId());
            if (spaceExtraAmenity!=null && bookingExtraAmenity.getNumber()!=0)
                amenityCharge=amenityCharge+bookingExtraAmenity.getNumber()*spaceExtraAmenity.getExtraRate();

        }

        Double slotAndAmenityCharge=slotCharge+amenityCharge;
        discount=spaceService.findDiscountOfSpace(space)*noOfSlots;

        if (bookingDto.getPromoCode()!=null){
            PromoDetails promoDetails=promoDetailsDao.getPromoByCode(bookingDto.getPromoCode());
            if(promoDetails.getPromoCode().equals(adminPromo))
                discount=0.00;
            else if (promoDetails.getIsFlat().equals(BooleanEnum.TRUE.value()) && slotAndAmenityCharge>promoDetails.getDiscount())
                promotionDiscount=(double)promoDetails.getDiscount();
            else if  (promoDetails.getIsFlat().equals(BooleanEnum.TRUE.value()) && slotAndAmenityCharge<=promoDetails.getDiscount()){
                promotionDiscount=slotAndAmenityCharge;
            }
            else  if (promoDetails.getIsFlat().equals(BooleanEnum.FALSE.value()))
                promotionDiscount=((slotAndAmenityCharge/100)*promoDetails.getDiscount());
        }


        chargeDetailsDto.setAdvanceOnlyEnable(false);

        if (Optional.ofNullable(space.getSpaceAdditionalDetails().getAdvanceOnlyEnable()).isPresent() &&
                space.getSpaceAdditionalDetails().getAdvanceOnlyEnable().equals(BooleanEnum.TRUE.value())){

            Double advance=slotAndAmenityCharge*(Optional.ofNullable(space.getSpaceAdditionalDetails().getCommissionPercentage()).orElse(20)/100.0) -discount;
            //advance=spaceService.findDiscountOfSpace(space)-discount;

            chargeDetailsDto.setAdvanceOnlyEnable(true);
            chargeDetailsDto.setPayableAdvance(advance.intValue());

                if( Optional.ofNullable(bookingDto.getAdvanceOnly()).isPresent() &&
                    bookingDto.getAdvanceOnly().equals(true)){
                    chargeDetailsDto.setAdvance(advance.intValue());
                }
        }



        Double totalCharge=slotAndAmenityCharge-discount-promotionDiscount;

        chargeDetailsDto.setSlotCharge(slotCharge.intValue());
        chargeDetailsDto.setExtraAmenityCharge(amenityCharge.intValue());
        chargeDetailsDto.setDiscount(discount.intValue());
        chargeDetailsDto.setPromotionDiscount(promotionDiscount.intValue());
        chargeDetailsDto.setBookingCharge(totalCharge.intValue());
        chargeDetailsDto.setCharge(totalCharge.intValue());
        return chargeDetailsDto;
    }

      public int findRefund(Integer bookingId){
        Booking booking=bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        CancellationPolicy cancellationPolicy=cancellationPolicyDao.read(space.getCancellationPolicy());
        Double refund=0.00;
        Date eventDate=new Date();
        Boolean isSet=true;

        for (BookingSlots bookingSlots:booking.getBookingSlots()){
            if (isSet) {
                eventDate = bookingSlots.getFromDate();
                isSet=false;
            }
            if (eventDate.getTime()>bookingSlots.getFromDate().getTime())
                eventDate=bookingSlots.getFromDate();
        }

        Long diff=(eventDate.getTime()-commonService.getCurrentTime())/(24 * 60 * 60 * 1000);
        if (cancellationPolicy.getId().equals(CancellationPolicyEnum.MODERATE.value()) && diff>=7)
            refund=booking.getBookingCharge()*0.5;
        else if (cancellationPolicy.getId().equals(CancellationPolicyEnum.FLEXIBLE.value()) && diff>=2)
            refund=booking.getBookingCharge();

        refund=refund+0.50;
        return refund.intValue();
    }
    @Override
    @Transactional
    public Map<String,Object> checkPromoCode(Integer spaceId,String promoCode,UserContext user) {
        Map<String,Object> responce=new HashMap<>();
        Space space=spaceDao.read(spaceId);
        Date date=new Date(commonService.getCurrentTime());
        PromoDetails promoDetails=promoDetailsDao.getPromoByCode(promoCode);
        if (promoDetails==null){
            responce.put("status",2);
            responce.put("message","Code is invalid");
            LOGGER.info("promo not existing");
        }
        else if (!promoDetails.getSpaces().contains(space) && promoDetails.getToAllSpaces().equals(BooleanEnum.FALSE.value())){
            responce.put("status",2);
            responce.put("message","Code is invalid");
            LOGGER.info("promo not for all spaces");
        }
        else if(Optional.ofNullable(promoDetails.getUserCount()).isPresent() && Optional.ofNullable(user).isPresent() && getAUserBookingUserAPromoCode(user.getUserId(),promoDetails.getId())>=promoDetails.getUserCount())
        {
            responce.put("status",2);
            responce.put("message","Code is invalid");
            LOGGER.info("user already booked:"+getAUserBookingUserAPromoCode(user.getUserId(),promoDetails.getId()));
        }
        else if (promoDetails.getStartDate().getTime()<=date.getTime() && promoDetails.getEndDate().getTime()>=date.getTime()){
            responce.put("status",1);
            responce.put("message","Promo code successfully applied");
            responce.put("promoDetails",promoDetails.build());
        }
        else if (promoDetails.getEndDate().getTime()<date.getTime()){
            responce.put("status",3);
            responce.put("message","The code is invalid due to expiry of the promo code");
            LOGGER.info("expired");

        }
            else{
            responce.put("status",2);
            responce.put("message","Code is invalid");
        }

        return responce;
    }

    @Override
    public PromoDetailDto getPromoDetails(String promoCode) {
        return promoDetailsDao.getPromoByCode(promoCode).build();
    }

    @Override
    public Boolean isValidPromo(Integer spaceId, String promoCode,Integer userId) {
        Space space=spaceDao.read(spaceId);
        Date date=new Date(commonService.getCurrentTime());
        PromoDetails promoDetails=promoDetailsDao.getPromoByCode(promoCode);
        if ((promoDetails.getSpaces().contains(space) || promoDetails.getToAllSpaces().equals(BooleanEnum.TRUE.value())) && promoDetails.getStartDate().getTime()<=date.getTime() && promoDetails.getEndDate().getTime()>=date.getTime()
                && (!Optional.ofNullable(promoDetails.getUserCount()).isPresent() || (Optional.ofNullable(promoDetails.getUserCount()).isPresent() && getAUserBookingUserAPromoCode(userId,promoDetails.getId()) < promoDetails.getUserCount()))
                && (!Optional.ofNullable(promoDetails.getTotalCount()).isPresent() || (Optional.ofNullable(promoDetails.getTotalCount()).isPresent() && getBookingCountUseAPromoCode(promoDetails.getId()) < promoDetails.getTotalCount()))
                )
            return true;
        return false;
    }

    @Override
    public void confirmBookings() {
        List<Booking> bookings = bookingDao.getBookingsEventsHeldToday();
        for (Booking booking : bookings) {
            BookingActionDto bookingActionDto = new BookingActionDto();
            bookingActionDto.setBooking_id(booking.getId());
            bookingActionDto.setEvent(EventsEnum.CONFIRM.value());
            try {
                bookingActions(bookingActionDto, true);
            } catch (Exception e) {

            }
            LOGGER.info(booking.getId() + ":Confirmed");
        }
    }

    @Override
    public Long getAllBookingCount() {
        return bookingDao.getAllBookingCount();
    }

    @Override
    public List<AdminBookingDetailsDto> getAllTentitiveBookings(Integer page) {
        List<Booking> bookings=bookingDao.getAllTentitiveBookings(page);
        List<AdminBookingDetailsDto> adminBookingDetailsDtos=new ArrayList<>();
        for(Booking booking:bookings){
            AdminBookingDetailsDto bookingDto=generateAdminBookingDtoTentitive(booking.getId());
            adminBookingDetailsDtos.add(bookingDto);
        }
        return adminBookingDetailsDtos;
    }


    private AdminBookingDetailsDto generateAdminBookingDtoTentitive(Integer bookingId){
        AdminBookingDetailsDto bookingDto = new AdminBookingDetailsDto();
        Booking booking = bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        User user=userDao.read(booking.getUser());
        //User host=userDao.read(space.getUser());

        bookingDto.setId(booking.getId());
        bookingDto.setOrderId(booking.getOrderId());
        bookingDto.setOrganizationName(space.getAddressLine2());
        bookingDto.setName(space.getName());
        bookingDto.setTotal(booking.getBookingCharge());
        bookingDto.setGuestName(user.getName());
        bookingDto.setGuestEmail(user.getEmail());
        bookingDto.setGuestMobile(user.getPhone());

        Set<BookingSlots> bookingSlots = booking.getBookingSlots();
        Set<CommonDto> slots = new HashSet<>(0);
        for (BookingSlots bookingSlot : bookingSlots) {
            CommonDto slot = new CommonDto();
            bookingDto.setEventDate(commonService.convertEpoch(bookingSlot.getFromDate()));
            slot.setFromDate(String.valueOf(bookingSlot.getFromDate().getTime()));
            slot.setToDate(String.valueOf(bookingSlot.getToDate().getTime()));
            slots.add(slot);
        }
        bookingDto.setDates(slots);

        DateFormat date = new SimpleDateFormat(Constants.PATTERN_2);
        BookingHistory bookingHistory=bookingHistoryDao.getBookingHistory(booking.getId(),StatesEnum.INITIATED.value());
        if (bookingHistory!=null)
            bookingDto.setBookedDate(date.format(bookingHistory.getCreatedAt()));

        return bookingDto;
    }

    public AdminBookingDetailsDto generateAdminBookingDto(Integer bookingId){
        AdminBookingDetailsDto bookingDto = new AdminBookingDetailsDto();
        Booking booking = bookingDao.read(bookingId);
        if (Optional.ofNullable(booking).isPresent()) {
            Space space = spaceDao.read(booking.getSpace());
            User user = userDao.read(booking.getUser());
            User host = userDao.read(space.getUser());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            DateFormat date = new SimpleDateFormat(Constants.PATTERN_2);

            bookingDto.setId(booking.getId());
            bookingDto.setOrderId(booking.getOrderId());
            bookingDto.setSpaceId(booking.getSpace());
            bookingDto.setOrganizationName(space.getAddressLine2());
            bookingDto.setName(spaceDao.read(booking.getSpace()).getName());
            bookingDto.setReservatationStatus(booking.getReservationStatus());
            bookingDto.setReservatationStatusName(reservationStatusDao.read(booking.getReservationStatus()).getLabel());
            bookingDto.setGuestName(user.getName());
            bookingDto.setGuestEmail(user.getEmail());
            bookingDto.setGuestMobile(user.getPhone());
            bookingDto.setHostName(host.getName());
            bookingDto.setHostEmail(host.getEmail());
            bookingDto.setHostMobile(host.getPhone());
            bookingDto.setTotal(booking.getBookingCharge());
            Reviews reviews = reviewsDao.getReviewByBookingId(bookingId);
            if (reviews != null)
                bookingDto.setReview(reviews.build());


            Set<BookingSlots> bookingSlots = booking.getBookingSlots();
            Set<CommonDto> slots = new HashSet<>(0);
            for (BookingSlots bookingSlot : bookingSlots) {
                CommonDto slot = new CommonDto();
                bookingDto.setDate(date.format(bookingSlot.getFromDate()));
                slot.setFromDate(df.format(bookingSlot.getFromDate()));
                slot.setToDate(df.format(bookingSlot.getToDate()));
                slots.add(slot);
            }
            bookingDto.setDates(slots);

            BookingHistory bookingHistory = bookingHistoryDao.getBookingHistory(booking.getId(), StatesEnum.INITIATED.value());
            if (bookingHistory != null)
                bookingDto.setBookedDate(date.format(bookingHistory.getCreatedAt()));


            return bookingDto;
        }else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public boolean isUserAllowForManualPayment(Integer user) {
        return bookingDao.isUserAllowForBooking(user);
    }

    @Override
    public boolean isTimeAllowForManualPayment(Set<BookingSlots>  bookingSlots) {
        boolean result=true;
        Long currentTime=commonService.getCurrentTime();
        try {
            for (BookingSlots slot : bookingSlots) {
                if (result){
                    result=(slot.getFromDate().getTime()-currentTime)>24*60*60*1000;
                        }else
                    break;
            }
        }catch (Exception ex){
            result=false;
        }
        return result;
    }

    @Override
    public List<AdminPaidBookingDetails> getAdminPaidBookings(Integer page) {
        List<Booking> bookings=bookingDao.getPaidBookings(page);
        List<AdminPaidBookingDetails> bookingDtos=new ArrayList<>();
        for (Booking booking:bookings){
            bookingDtos.add(setAdminPaidBookingDetails(booking.getId()));
        }
        return bookingDtos;
    }

    @Override
    public List<AdminBookingFinanceDto> getFinanceDetails(Integer page) {
        List<Booking> bookings=bookingDao.getPaidBookings(page);
        List<AdminBookingFinanceDto> bookingDtos=new ArrayList<>();
        for (Booking booking:bookings){
            bookingDtos.add(setFinanceDetails(booking.getId()));
        }
        return bookingDtos;
    }

    public AdminBookingFinanceDto setFinanceDetails(Integer bookingId) {
      AdminBookingFinanceDto financeDto=setAdminPaidBookingDetails(bookingId).build();
        Booking booking=bookingDao.read(bookingId);
        Space space = spaceDao.read(booking.getSpace());
        PaymentHistory paymentHistory=paymentHistoryDao.getPaymentHistory(bookingId);
        if (space.getSpaceAdditionalDetails() != null && space.getSpaceAdditionalDetails().getCommissionPercentage() != null) {
                financeDto.setAccountHolderName(space.getSpaceAdditionalDetails().getAccountHolderName());
                financeDto.setAccountNumber(space.getSpaceAdditionalDetails().getAccountNumber());
                financeDto.setBank(space.getSpaceAdditionalDetails().getBank());
                financeDto.setBranch(space.getSpaceAdditionalDetails().getBankBranch());
        }

        financeDto.setCommission(0.0);

        financeDto.setNotes(booking.getRemarks());
        financeDto.setBooking(booking.getBookingCharge());
        financeDto.setAdvance(booking.getAdvance());
        financeDto.setDiscount(booking.getDiscount());
        financeDto.setIsAdvance(Optional.ofNullable(booking.getAdvance()).isPresent());
        if (Optional.ofNullable(paymentHistory).isPresent()) {
            financeDto.setPaymentMethod(paymentHistory.getProvider());
        }

        return financeDto;
    }
    public AdminPaidBookingDetails setAdminPaidBookingDetails(Integer bookingId){
        Booking booking=bookingDao.read(bookingId);
        if (booking!=null) {
            Space space = spaceDao.read(booking.getSpace());
            User host = userDao.read(booking.getUser());
            User guest=userDao.read(booking.getUser());
            BookingHistory bookingHistory = bookingHistoryDao.getBookingHistory(booking.getId(), StatesEnum.INITIATED.value());
            AdminPaidBookingDetails adminPaidBookingDetails = new AdminPaidBookingDetails();
            adminPaidBookingDetails.setId(booking.getId());
            adminPaidBookingDetails.setOrderId(booking.getOrderId());
            adminPaidBookingDetails.setBookingMade(bookingHistory.getCreatedAt().getTime());
            adminPaidBookingDetails.setSpaceName(space.getName());
            adminPaidBookingDetails.setHostName(host.getName());
            adminPaidBookingDetails.setGuestName(guest.getName());
            adminPaidBookingDetails.setGuestMobile(guest.getPhone());
            adminPaidBookingDetails.setGuestEmail(guest.getEmail());
            adminPaidBookingDetails.setCancellationPolicy(cancellationPolicyDao.read(space.getCancellationPolicy()).getName());
            Integer commision = 20;
            if (space.getSpaceAdditionalDetails() != null) {
                if (space.getSpaceAdditionalDetails().getCommissionPercentage() != null)
                    commision = space.getSpaceAdditionalDetails().getCommissionPercentage();
            }
            adminPaidBookingDetails.setOrganizationName(space.getAddressLine2());
            adminPaidBookingDetails.setCommissionPercentage(commision);
            adminPaidBookingDetails.setMillionspacesCharge((booking.getBookingCharge() *commision)/ 100);
            adminPaidBookingDetails.setHostCharge((booking.getBookingCharge() *(100 - commision))/ 100);
            adminPaidBookingDetails.setTotal(booking.getBookingCharge());

            Date eventDate=new Date();
            Boolean isSet=true;

            for (BookingSlots bookingSlots:booking.getBookingSlots()){
                if (isSet) {
                    eventDate = bookingSlots.getFromDate();
                    isSet=false;
                }
                if (eventDate.getTime()>bookingSlots.getFromDate().getTime())
                    eventDate=bookingSlots.getFromDate();
            }
            Integer diff=0;
            if (space.getCancellationPolicy().equals(CancellationPolicyEnum.MODERATE.value()))
                diff=7;
            else if (space.getCancellationPolicy().equals(CancellationPolicyEnum.FLEXIBLE.value()))
                diff=2;

            adminPaidBookingDetails.setEventDate(commonService.convertEpoch(eventDate));
            Integer diffDay=0;
            if (eventDate.getDay()<4)
                diffDay=4-eventDate.getDay();
            else
                diffDay=11-eventDate.getDay();
            adminPaidBookingDetails.setPaymentDueDate(commonService.convertEpoch(eventDate)+1000*60*60*24*(diffDay));
            adminPaidBookingDetails.setCancellationCutOffDate(commonService.convertEpoch(eventDate)-(diff-diffDay)*1000*60*60*24);

            if (booking.getPaymentVerify() != null && booking.getPaymentVerify().getHostReceivedProof()!=null) {
                adminPaidBookingDetails.setPaymentVerified(BooleanEnum.TRUE.value());
                adminPaidBookingDetails.setPdf(booking.getPaymentVerify().getHostReceivedProof());
            } else
                adminPaidBookingDetails.setPaymentVerified(BooleanEnum.FALSE.value());

            return adminPaidBookingDetails;
        }else
            throw new NoSuchElementException();
    }

    @Override
    public Long getBookingsCount(Integer bookingStatus) {
        return bookingDao.getCount(bookingStatus);
    }

    @Override
    public Integer markAsPaymentReceive(PaymentVerifyDto paymentVerifyDto) {
        Booking booking=bookingDao.read(paymentVerifyDto.getBookingId());
        Integer responce=BooleanEnum.FALSE.value();

        if (booking!=null && (booking.getReservationStatus().equals(StatesEnum.PAYMENT_DONE.value()) || booking.getReservationStatus().equals(StatesEnum.CONFIRMED.value()))) {
            PaymentVerify paymentVerify = paymentVerifyDao.read(booking.getId());
            if (paymentVerify != null) {
                if (paymentVerifyDto.getVerify()!=null)
                     paymentVerify.setVerified(paymentVerifyDto.getVerify());
                if (paymentVerifyDto.getPdf()!=null)
                    paymentVerify.setPdf(paymentVerifyDto.getPdf());

                paymentVerifyDao.update(paymentVerify);
            } else {
                paymentVerify = new PaymentVerify();
                paymentVerify.setBookingId(booking.getId());
                paymentVerify.setVerified(BooleanEnum.TRUE.value());
                if (paymentVerifyDto.getPdf()!=null)
                    paymentVerify.setPdf(paymentVerifyDto.getPdf());
                paymentVerifyDao.create(paymentVerify);
            }

            responce=paymentVerify.getVerified();
        }
        return responce;
    }


    @Override
    public Integer verifyPaymentReceiveFromGuest(PaymentVerifyDto paymentVerifyDto) {
        Booking booking=bookingDao.read(paymentVerifyDto.getBookingId());
        Integer responce=BooleanEnum.TRUE.value();

            if (booking != null && booking.getReservationStatus().equals(StatesEnum.PENDING_PAYMENT.value())) {
                PaymentVerify paymentVerify = paymentVerifyDao.read(booking.getId());
                if (paymentVerify != null) {
                    paymentVerify.setGuestSentProof(paymentVerifyDto.getProofFromGuest());
                    paymentVerifyDao.update(paymentVerify);
                } else {
                    paymentVerify = new PaymentVerify();
                    paymentVerify.setBookingId(booking.getId());
                    paymentVerify.setGuestSentProof(paymentVerifyDto.getProofFromGuest());
                    paymentVerifyDao.create(paymentVerify);
                }
            }
        else{
            responce=BooleanEnum.FALSE.value();
        }
        return responce;
    }

    @Override
    public Integer verifyPaymentSendToHost(PaymentVerifyDto paymentVerifyDto) {
        Booking booking=bookingDao.read(paymentVerifyDto.getBookingId());
        Integer responce=BooleanEnum.TRUE.value();


            if (booking!=null && (booking.getReservationStatus().equals(StatesEnum.PAYMENT_DONE.value()) || booking.getReservationStatus().equals(StatesEnum.CONFIRMED.value()))) {
                PaymentVerify paymentVerify = paymentVerifyDao.read(booking.getId());
                if (paymentVerify != null) {
                    paymentVerify.setHostReceivedProof(paymentVerifyDto.getProofToHost());
                    paymentVerifyDao.update(paymentVerify);
                } else {
                    paymentVerify = new PaymentVerify();
                    paymentVerify.setBookingId(booking.getId());
                    paymentVerify.setHostReceivedProof(paymentVerifyDto.getProofToHost());
                    paymentVerifyDao.create(paymentVerify);
                }
            }
        else{
            responce=BooleanEnum.FALSE.value();
        }
        return responce;
    }

    @Override
    public Map<String, Object> getAUserBookingWithPagination(Integer userId, Integer page) {
        List<Booking> bookings = bookingDao.getAUserBookingWithPagination(userId,page);
        List<BookingDetailsDto> dtoList = new ArrayList<>();

        for (Booking booking:bookings){
            BookingDetailsDto bookingDetailsDto=generateSimpleBookingDetails(booking.getId());
            dtoList.add(bookingDetailsDto);
        }
        Map<String,Object>  result=new HashMap<>();
        result.put("bookings",dtoList);
        result.put("count",bookingDao.getAUserBookingCount(userId).intValue());

        return result;
    }

    private void bookPromotedSpace(Integer bookingId){
        Booking booking=bookingDao.read(bookingId);
        booking.setReservationStatus(StatesEnum.PAYMENT_DONE.value());
        bookingDao.update(booking);
        LOGGER.info("promotion  booking  :{}",bookingId);
        emailService.sendMailToPromotedSpaceBooking(bookingId);
    }


    private boolean isSpaceHasPromotion(Integer spaceId,Date startDate,Date endDate){
        SpacePromotion spacePromotion=spacePromotionDao.getSpacePromotion(spaceId,startDate,endDate);
        return Optional.ofNullable(spacePromotion).isPresent();
    }

    private boolean isBookingSlotHasPromotion(Integer spaceId,Set<BookingSlots> bookingSlots){
        boolean responce=true;
        for(BookingSlots bookingSlot:bookingSlots){
            if (responce)
                responce=isSpaceHasPromotion(spaceId,bookingSlot.getFromDate(),bookingSlot.getToDate());
            else
                break;
        }

        return responce;
    }

    @Override
    public Long getVerifiedBookingCount() {
        return paymentVerifyDao.paymentVerifiedBookingCount();
    }


    @Override
    public Integer getAUserBookingUserAPromoCode(Integer userId, Integer promoId) {
        Integer val= bookingDao.getNoOfBookingByUserUsingAPromo(userId,promoId);
        LOGGER.info("user promo bookings:"+val);
        return val;
    }


    private Integer getBookingCountUseAPromoCode(Integer promoId) {
        Integer val= bookingDao.getNoOFBookingsForAPromo(promoId);
        LOGGER.info("user promo bookings:"+val);
        return val;
    }

    @Override
    public Boolean isPayLaterEnabled(Integer bookingId) {
        Booking booking=bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        boolean result=true;
        Long currentTime=commonService.getCurrentTime();
        try {
            for (BookingSlots slot : booking.getBookingSlots()) {
                if (result){
                    result=(slot.getFromDate().getTime()-currentTime)>(((3*24)+space.getNoticePeriod())*60*60*1000);
                }else
                    break;
            }
        }catch (Exception ex){
            result=false;
        }
        return result;
    }

    public void createPayLaterRecord(Integer bookingId){
        LOGGER.info("pay later for :"+bookingId);
        BookingPayLater bookingPayLater=new BookingPayLater();
        Date date=new Date(commonService.getCurrentTime()+(3*24 * 60 * 60 * 1000));
        bookingPayLater.setBooking(bookingId);
        bookingPayLater.setExpire(false);
        bookingPayLater.setExpireDate(date);
        bookingPayLaterDao.create(bookingPayLater);
    }

    private void selectPaymentOptionAfterPayLater(Integer bookingId){
        expirePayLater(bookingId);
        BookingExpireDetails bookingExpireDetails = bookingExpireDetailsDao.getByBookingId(bookingId);
        bookingExpireDetails.setExpired(0);
        bookingExpireDetails.setExpireDate(new Date(commonService.getCurrentTime()+(24 * 60 * 60 * 1000)));
        bookingExpireDetailsDao.update(bookingExpireDetails);
        emailService.sendBookingActionMails(bookingId, EventsEnum.WAIT_FOR_PAY.value());
    }

    public void expirePayLater(Integer bookingId){
        BookingPayLater bookingPayLater=bookingPayLaterDao.read(bookingId);
        if(Optional.ofNullable(bookingPayLater).isPresent()){
            bookingPayLater.setExpire(true);
            bookingPayLaterDao.update(bookingPayLater);
        }
    }

    @Override
    public Map<String, Object> payLaterDetails(Integer bookingId) {
        Map<String,Object> response=new LinkedHashMap<>();
        Booking booking=bookingDao.read(bookingId);
        Space space=spaceDao.read(booking.getSpace());
        Set<CommonDto> slots = new HashSet<>(0);
        DateFormat df = new SimpleDateFormat(com.eventspace.util.Constants.PATTERN_11);
        DateFormat df2 = new SimpleDateFormat(Constants.PATTERN_2);
        for (BookingSlots bookingSlot : booking.getBookingSlots()) {
            CommonDto slot = new CommonDto();
            response.put("reservationDate",df2.format(bookingSlot.getFromDate()));
            slot.setFromDate(df.format(bookingSlot.getFromDate()));
            slot.setToDate(df.format(bookingSlot.getToDate()));
            slots.add(slot);
        }

        response.put("id", booking.getId());
        response.put("orderId", booking.getOrderId());
        response.put("referenceId",booking.getOrderId());
        response.put("spaceName",space.getName());
        response.put("organization",space.getAddressLine2());
        response.put("address",space.getAddressLine1());
        response.put("reservationTime",slots);
        response.put("bookingCharge",booking.getBookingCharge());
        response.put("discount",booking.getDiscount());
        response.put("isPayLaterEnabled",false);
        response.put("isUserEligibleForManualPayment",true);
        response.put("promotion",getPromoDetails(booking.getId()));
        response.put("advanceOnly",false);
        Optional.ofNullable(booking.getAdvance()).ifPresent(advance->{
            response.put("advanceOnly",true);
            response.put("advance",advance);
        });
        Optional.ofNullable(booking.getBookingPayLater()).ifPresent(bookingPayLater -> {
            response.put("isBookingTimeEligibleForManualPayment",(bookingPayLater.getExpireDate().getTime()-System.currentTimeMillis())>24*60*60*1000);
        });

        return response;
    }

    private void markAsPaid(Integer bookingId){
        BookingActionDto bookingActionDto = new BookingActionDto();
        bookingActionDto.setBooking_id(bookingId);
        bookingActionDto.setEvent(EventsEnum.PAY.value());
        bookingActionDto.setMethod("MANUAL");
        bookingActionDto.setStatus(PaySubEventEnum.PAY.value());
        try {
            bookingActions(bookingActionDto, true);
        } catch (Exception e) {
            //exception
        }
    }

    public Map<String,Object> getPromoDetails(Integer bookingId){
        Map<String,Object> response=new HashMap<>();
        Booking booking=bookingDao.read(bookingId);
        if (Optional.ofNullable(booking.getPromoDetails()).isPresent()) {
            PromoDetails promoDetails = promoDetailsDao.read(booking.getPromoDetails().getPromoDetails());
            if (Optional.ofNullable(promoDetails.getCorporatePromo()).isPresent() && promoDetails.getCorporatePromo().getActive()){
                response.put("type","CORPORATE_BOOKING");
                response.put("corporate",promoDetails.getCorporatePromo().getCorporate());
            }else{
                response.put("type","NORMAL");
            }
        }
        return response;
    }

    public Map<String,Object> markAsPaymentDone(Integer bookingId){
        Map<String,Object> response=new HashMap<>();
        Booking booking=bookingDao.read(bookingId);
        if (booking.getReservationStatus().equals(StatesEnum.INITIATED.value())){
            booking.setReservationStatus(StatesEnum.PAYMENT_DONE.value());

            bookingDao.update(booking);

            int payAtCourt=10;
            emailService.sendBookingActionMails(bookingId,payAtCourt);
            response.put("response","success");
            response.put("status",200);
        }else{
            response.put("response","failed");
            response.put("status",400);
        }

        return response;
    }


    @Override
    public AdminBookingDetailsDto getAAdminTentitiveBooking(Integer bookingId) {
        Booking booking=bookingDao.read(bookingId);

        if (Optional.ofNullable(booking).isPresent()){
            return generateAdminBookingDtoTentitive(bookingId);
        }else{
            throw new NoSuchElementException();
        }

    }
}
