package com.eventspace.dao.Impl;

import com.eventspace.dao.ReportsDao;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class ReportDaoImpl implements ReportsDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Map<String, Object>> procedureListResult(String procedureName) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query= currentSession.createSQLQuery(String.format("call  %s",procedureName));
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String,Object>> aliasToValueMapList=query.list();
        return aliasToValueMapList;
    }

    @Override
    public Map<String, Object> procedureUniqueResult(String procedureName) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query= currentSession.createSQLQuery(String.format("call  %s",procedureName));
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        Map<String,Object> response=(Map<String,Object>)query.uniqueResult();
        return response;
    }

    @Override
    public List<Map<String, Object>> queryListResults(String sqlQuery) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query= currentSession.createSQLQuery(String.format(sqlQuery));
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        List<Map<String,Object>> aliasToValueMapList=query.list();
        return aliasToValueMapList;
    }

    @Override
    public Map<String, Object> queryUniqueResults(String sqlQuery) {
        Session currentSession = sessionFactory.getCurrentSession();
        Query query= currentSession.createSQLQuery(String.format(sqlQuery));
        query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
        Map<String,Object> response=(Map<String,Object>)query.uniqueResult();
        return response;
    }
}
