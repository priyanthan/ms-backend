package com.eventspace.dao;

import com.eventspace.domain.PaymentVerify;

public interface PaymentVerifyDao extends BaseDao<PaymentVerify> {

    boolean isPaymentVerified(Integer bookingId);
    Long paymentVerifiedBookingCount();
}
