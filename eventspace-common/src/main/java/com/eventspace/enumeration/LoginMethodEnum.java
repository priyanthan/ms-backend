/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.enumeration;

import java.util.HashMap;

public enum LoginMethodEnum {

    DEFAULT(0), GOOGLE(1), FACEBOOK(2);

    private static final HashMap<Integer, LoginMethodEnum> typeValueMap = new HashMap<>();

    static {
        for (LoginMethodEnum type : LoginMethodEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    LoginMethodEnum(final int value) {
        this.value = value;
    }

    public static LoginMethodEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
