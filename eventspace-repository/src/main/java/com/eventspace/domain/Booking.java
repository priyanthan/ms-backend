/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.BookingDto;
import com.eventspace.dto.CommonDto;
import com.eventspace.util.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.eventspace.domain.Booking.*;



/**
 * The Class Amenities.
 */
@NamedNativeQueries({
        @NamedNativeQuery(
                name = SPACE_BOOKING_CHARGE_SP,
                query = "CALL bookingCharge(:bookingId)"
        ),
        @NamedNativeQuery(
                name = IS_CANCELLED_AFTER_PAID_SP,
                query = "CALL isCancelledAfterPaid(:booking_id)"
        ),
        @NamedNativeQuery(
                name = GET_CANCELLED_DATE_SP,
                query = "CALL getCancelledDate(:bookingId,:bookingStatus)"
        )
})
@Entity
@Table(name = "booking")
public class Booking implements Serializable, BaseDomain {
    public static final String SPACE_BOOKING_CHARGE_SP = "callBookingChargeStoreProcedure";

    public static final String IS_CANCELLED_AFTER_PAID_SP = "callIsCancelledAfterPaidStoreProcedure";

    public static final String GET_CANCELLED_DATE_SP = "callGetCancelledDateStoreProcedure";
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(Booking.class);

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "order_id")
    private String orderId;
    /**
     * The user.
     */
    @Column(name = "user", nullable = false)
    private Integer user;

    /**
     * The space.
     */
    @Column(name = "space", nullable = false)
    private Integer space;

    /**
     * The event sub type.
     */
    @Column(name = "event_type", nullable = false)
    private Integer eventType;

    @Column(name = "guest_count")
    private Integer guestCount;

    /**
     * The reservation status.
     */
    @Column(name = "reservation_status", nullable = false)
    private Integer reservationStatus;

    @OneToMany(mappedBy = "booking", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<BookingSlots> bookingSlots;

    @OneToMany(mappedBy = "pk1.booking", fetch = FetchType.LAZY)
    private Set<BookingSpaceExtraAmenity> bookingSpaceExtraAmenities;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private BookingHasPromo promoDetails;

    @Column(name = "seating_arrangement")
    private Integer seatingArrangement;

    @Column(name = "action_taker", nullable = false)
    private Integer actionTaker;

    @Column(name = "booking_charge")
    private Double bookingCharge;

    @Column(name = "original_charge")
    private Integer originalCharge;

    @Column(name = "discount")
    private Double discount;

    @Column(name = "advance")
    private Double advance;

    @Column(name = "refund")
    private Double refund;

    @Column(name="remarks")
    private String remarks;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private PaymentVerify paymentVerify;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private BookingPayLater bookingPayLater;

    @Column(name="device_id")
    private Integer device;

    public Set<BookingSpaceExtraAmenity> getBookingSpaceExtraAmenities() {
        return bookingSpaceExtraAmenities;
    }

    public void setBookingSpaceExtraAmenities(Set<BookingSpaceExtraAmenity> bookingSpaceExtraAmenities) {
        this.bookingSpaceExtraAmenities = bookingSpaceExtraAmenities;
    }

    /**
     * The Constructor.
     *
     * @param id the id
     */
    public Booking(int id) {
        this.id = id;
    }

    /**
     * The Constructor.
     */
    public Booking() {
    }


    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * Gets the user.
     *
     * @return the user
     */
    public Integer getUser() {
        return user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(Integer user) {
        this.user = user;
    }

    /**
     * Gets the space.
     *
     * @return the space
     */
    public Integer getSpace() {
        return space;
    }

    /**
     * Sets the space.
     *
     * @param space the new space
     */
    public void setSpace(Integer space) {
        this.space = space;
    }


    public Integer getEventType() {
        return eventType;
    }


    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    /**
     * Gets the reservation status.
     *
     * @return the reservation status
     */
    public Integer getReservationStatus() {
        return reservationStatus;
    }

    /**
     * Sets the reservation status.
     *
     * @param reservationStatus the new reservation status
     */
    public void setReservationStatus(Integer reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

	/* (non-Javadoc)
     * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
	 */

    public Integer getActionTaker() {
        return actionTaker;
    }

    public void setActionTaker(Integer actionTaker) {
        this.actionTaker = actionTaker;
    }

    public Double getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(Double bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Set<BookingSlots> getBookingSlots() {
        return bookingSlots;
    }

    public void setBookingSlots(Set<BookingSlots> bookingSlots) {
        this.bookingSlots = bookingSlots;
    }

    public Integer getSeatingArrangement() {
        return seatingArrangement;
    }

    public void setSeatingArrangement(Integer seatingArrangement) {
        this.seatingArrangement = seatingArrangement;
    }

    public Integer getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(Integer guestCount) {
        this.guestCount = guestCount;
    }

    public Double getRefund() {
        return refund;
    }

    public void setRefund(Double refund) {
        this.refund = refund;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BookingHasPromo getPromoDetails() {
        return promoDetails;
    }

    public void setPromoDetails(BookingHasPromo promoDetails) {
        this.promoDetails = promoDetails;
    }

    public PaymentVerify getPaymentVerify() {
        return paymentVerify;
    }

    public void setPaymentVerify(PaymentVerify paymentVerify) {
        this.paymentVerify = paymentVerify;
    }

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public BookingPayLater getBookingPayLater() {
        return bookingPayLater;
    }

    public void setBookingPayLater(BookingPayLater bookingPayLater) {
        this.bookingPayLater = bookingPayLater;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Integer getOriginalCharge() {
        return originalCharge;
    }

    public void setOriginalCharge(Integer originalCharge) {
        this.originalCharge = originalCharge;
    }

    @Override
    public Object build() {
        return null;
    }

    public static Booking build(BookingDto bookingDto, Integer userId) {
        Booking booking = new Booking();
        booking.setId(bookingDto.getId());
        booking.setOrderId(bookingDto.getOrderId());
        booking.setUser(userId);
        booking.setEventType(bookingDto.getEventType());
        booking.setGuestCount(bookingDto.getGuestCount());
        booking.setSeatingArrangement(bookingDto.getSeatingArrangement());
        booking.setBookingCharge(bookingDto.getBookingCharge());
        booking.setReservationStatus(bookingDto.getReservationStatus());
        booking.setSpace(bookingDto.getSpace());
        booking.setActionTaker(userId);

        Optional.ofNullable(bookingDto.getAdvance()).ifPresent(booking::setAdvance);

        if (bookingDto.getDevice()!=null) {
            if (bookingDto.getDevice().contains("okhttp")) {
                booking.setDevice(3);
            } else if (bookingDto.getDevice().contains("CFNetwork")) {
                booking.setDevice(2);
            } else {
                booking.setDevice(1);
            }

        }
        Set<BookingSlots> bookingSlots= new HashSet<>(0);

        for (CommonDto slot : bookingDto.getDates()) {
            DateFormat df=new SimpleDateFormat(Constants.PATTERN_7);
            try {
                BookingSlots bookingSlot=new BookingSlots();
                bookingSlot.setFromDate(df.parse(slot.getFromDate()) );
                bookingSlot.setToDate(df.parse(slot.getToDate()));
                bookingSlot.setBooking(booking);
                bookingSlots.add(bookingSlot);
            } catch (ParseException e) {
                LOGGER.error("build exception----->{}",e);
            }
        }
        booking.setBookingSlots(bookingSlots);
        if (!bookingSlots.isEmpty())
            return booking;
        else
            return null;
    }


  /*  public static Date convertToUtc(String date) {
        String[] time = date.split("T");
        String aFormatDate = time[0] + " " + time[1];
        String aRevisedDate = null;
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            final Date dateObj = sdf.parse(aFormatDate);
            aRevisedDate = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a Z").format(dateObj);
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a Z");
            return (Date) formatter.parse(aRevisedDate);
        } catch (ParseException e) {
            LOGGER.error("Error occurred in Parsing the Data Time Object:  " + e);
        } catch (Exception e) {
            LOGGER.error("Error occurred in Data Time Objecct:  " + e);
        }

        return null;
    }*/


}
