package com.eventspace.dao;

import com.eventspace.domain.Image;

/**
 * Created by Aux-052 on 1/2/2017.
 */
public interface ImageDao extends BaseDao<Image> {
    void removeImage(Integer imageId);
}
