package com.eventspace.dto;

import java.io.Serializable;

public class ReimbursableOptionDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;

    private String code;

    private String desc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
