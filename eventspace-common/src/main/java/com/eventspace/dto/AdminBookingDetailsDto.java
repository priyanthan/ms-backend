package com.eventspace.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Auxenta on 4/28/2017.
 */
public class AdminBookingDetailsDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;
    private String orderId;
    private Integer spaceId;
    private String name;
    private String organizationName;
    private Long eventDate;
    private Double total;
    private Integer reservatationStatus;
    private String reservatationStatusName;
    private String guestName;
    private String guestEmail;
    private String guestMobile;
    private String hostName;
    private String hostEmail;
    private String hostMobile;
    private Set<CommonDto> dates;
    private String bookedDate;
    private String date;
    private ReviewDetailsDto review;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(Integer spaceId) {
        this.spaceId = spaceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getReservatationStatus() {
        return reservatationStatus;
    }

    public void setReservatationStatus(Integer reservatationStatus) {
        this.reservatationStatus = reservatationStatus;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public Set<CommonDto> getDates() {
        return dates;
    }

    public void setDates(Set<CommonDto> dates) {
        this.dates = dates;
    }

    public String getBookedDate() {
        return bookedDate;
    }

    public void setBookedDate(String bookedDate) {
        this.bookedDate = bookedDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReservatationStatusName() {
        return reservatationStatusName;
    }

    public void setReservatationStatusName(String reservatationStatusName) {
        this.reservatationStatusName = reservatationStatusName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Long getEventDate() {
        return eventDate;
    }

    public void setEventDate(Long eventDate) {
        this.eventDate = eventDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ReviewDetailsDto getReview() {
        return review;
    }

    public void setReview(ReviewDetailsDto review) {
        this.review = review;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(String guestMobile) {
        this.guestMobile = guestMobile;
    }

    public String getHostEmail() {
        return hostEmail;
    }

    public void setHostEmail(String hostEmail) {
        this.hostEmail = hostEmail;
    }

    public String getHostMobile() {
        return hostMobile;
    }

    public void setHostMobile(String hostMobile) {
        this.hostMobile = hostMobile;
    }
}
