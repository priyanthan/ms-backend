package com.eventspace.ws.cache;

import com.eventspace.ws.dto.CachebleDTO;
import com.eventspace.ws.dto.URLDetailsDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CacheUtility {

    private static final Logger LOGGER = Logger.getLogger(CacheUtility.class);

    private List<URLDetailsDTO> cachingURLs;

    public CacheUtility() {
        cachingURLs = new ArrayList<>();
        try {
            /*
            cachingURLs.add(new URLDetailsDTO("api/common/eventTypes", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/amenities", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/rules", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/seatingArrangement", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/amenityUnits", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/cancellationPolicies", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/measurementUnit", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/chargeTypes", GET));
            cachingURLs.add(new URLDetailsDTO("api/common/blockChargeTypes", GET));
            cachingURLs.add(new URLDetailsDTO("api/space/asearch", POST));
            */
            ObjectMapper objectMapper = new ObjectMapper();
            TypeFactory typeFactory = objectMapper.getTypeFactory();

            File file = new ClassPathResource("properties/cache.configuration.json").getFile();
            cachingURLs = objectMapper.readValue(file, typeFactory.constructCollectionType(List.class, URLDetailsDTO.class));

        } catch (IOException e) {
            LOGGER.error("!!!!!!!!!!!! cache.configuration.json reading error !!!!!!!!!!!!");
        }
    }

    public CachebleDTO isCacheble(String requestURL, String method) {
        CachebleDTO cacheDTO = null;
        try {
            String requestURI = getReqeustURI(requestURL);
            cacheDTO = isCacheble(new URLDetailsDTO(requestURI, method));
        } catch (MalformedURLException e) {
           LOGGER.info("ERROR :",e);
        }

        return cacheDTO;
    }

    private CachebleDTO isCacheble(URLDetailsDTO urlDetails) {

        CachebleDTO cachebleDTO = new CachebleDTO();
        boolean isCacheble = false;

        String requestedURI = urlDetails.getUri();
        String requestedMethod = urlDetails.getMethod();

        for (URLDetailsDTO cachingURL : cachingURLs) {
            String parentURI = cachingURL.getUri();

            int position = requestedURI.toLowerCase().indexOf(parentURI.toLowerCase());

            if (position < 0)
                continue;

            String splittedParent = requestedURI.substring(0, parentURI.length());

            if (requestedMethod.equals(cachingURL.getMethod())) {
                isCacheble = splittedParent.toLowerCase().contains(parentURI.toLowerCase());
                //isCacheble = cachingURL.equals(new URLDetailsDTO(splittedParent, requestedMethod));
            }

            if (isCacheble) {
                LOGGER.info(String.format("Request URL has a cache match with [%s]", cachingURL));
                if (!requestedURI.equals(parentURI)) {
                    String splittedChild = requestedURI.substring(parentURI.length());

                    if (NumberUtils.isNumber(splittedChild)){
                        cachebleDTO.setChildKey(splittedChild);
                    }
                    else {
                        isCacheble = false;
                                break;
                    }
                }
                cachebleDTO.setParentURI(parentURI);
                break;
            }
        }
        cachebleDTO.setCacheble(isCacheble);

        if (!isCacheble) {
            LOGGER.info(String.format("Request URL [%s %s] doesn't have any cache match", requestedMethod, requestedURI));
        }

        return cachebleDTO;
    }

    private String getReqeustURI(String requestURL) throws MalformedURLException {

        URL aURL = new URL(requestURL);
        String requestURI = aURL.getPath();

        // Removing / from tail
        while (requestURI.substring(requestURI.length() - 1).equals("/")) {
            requestURI = requestURI.substring(0, requestURI.length() - 1);
        }

        // Removing / from beginning
        while (requestURI.substring(0, 1).equals("/")) {
            requestURI = requestURI.substring(1);
        }

        LOGGER.info(String.format("Reqeust URI [%s]", requestURI));

        return requestURI.trim();
    }
}