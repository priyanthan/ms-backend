package com.eventspace.dao;

import com.eventspace.domain.BlockChargeType;

import java.util.List;

public interface BlockChargeTypeDao extends BaseDao<BlockChargeType> {

    List<BlockChargeType> getAllBlockChargeTypes();
}
