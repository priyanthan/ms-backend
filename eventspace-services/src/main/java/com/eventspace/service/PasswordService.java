/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.service;

import com.eventspace.dto.PassWordResetDto;
import com.eventspace.dto.UserPasswordResetDto;

/**
 * The Interface PasswordService.
 */
public interface PasswordService {

    Boolean forgetPassword(String userMail,String host);
    Boolean resetPassword(String encrypted, PassWordResetDto passWordResetDto);
    Boolean resetUserPassword(Integer userId,UserPasswordResetDto userPasswordResetDto);
}
