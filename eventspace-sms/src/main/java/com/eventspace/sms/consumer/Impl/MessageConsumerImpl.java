package com.eventspace.sms.consumer.Impl;


import com.eventspace.exception.EventspaceException;
import com.eventspace.sms.dto.MessageMetaData;
import com.eventspace.sms.sender.MessageSender;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@Component
public class MessageConsumerImpl implements MessageListener {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(MessageConsumerImpl.class);

	@Autowired
	private MessageSender messageSender;

	@Override
	public void onMessage(final Message message) {

		logger.info("onMessage -> Get Called");

		try {
			if (message instanceof ObjectMessage) {

				MessageMetaData auditMetaData = (MessageMetaData) ((ObjectMessage) message).getObject();

				//logger.info(
				//		String.format("onMessage -> Casted the message Successfully... [%s]", smsMetaData));
				messageSender.sendSms(auditMetaData);
				
			} else {
				logger.error("onMessage -> Received message of unknown type");
				throw new EventspaceException("Message type isn't compatible");
			}
		} catch (Exception ex) {
			logger.error("onMessage -> Exception : ", ex);
		}
	}

}
