package com.eventspace.ws.controller;

import com.eventspace.dto.ReviewDetailsDto;
import com.eventspace.dto.UserContext;
import com.eventspace.dto.UserDetailsDto;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/api/mobile")
public class WebMobileController {

    private static final Log logger = LogFactory.getLog(UserController.class);

    @Autowired
    private SecurityFacade securityFacade;

    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    @GetMapping("/reviews")
    @ResponseStatus(HttpStatus.OK)
    public List<ReviewDetailsDto> get(HttpServletRequest request){
       return spaceService.getMSReviews();
    }
}
