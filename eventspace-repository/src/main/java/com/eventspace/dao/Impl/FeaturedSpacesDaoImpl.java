package com.eventspace.dao.Impl;

import com.eventspace.dao.FeaturedSpacesDao;
import com.eventspace.domain.FeaturedSpaces;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Auxenta on 8/24/2017.
 */
@Repository
public class FeaturedSpacesDaoImpl extends BaseDaoImpl<FeaturedSpaces> implements FeaturedSpacesDao {

    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedSpacesDaoImpl.class);

    @Override
    public List<FeaturedSpaces> getAll() {
        LOGGER.info("getAll-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<FeaturedSpaces>) currentSession.createCriteria(FeaturedSpaces.class).list();
    }
}
