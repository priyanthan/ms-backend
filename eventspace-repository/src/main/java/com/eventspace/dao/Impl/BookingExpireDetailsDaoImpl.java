package com.eventspace.dao.Impl;

import com.eventspace.dao.BookingExpireDetailsDao;
import com.eventspace.domain.BookingExpireDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Auxenta on 6/5/2017.
 */
@Repository
public class BookingExpireDetailsDaoImpl extends BaseDaoImpl<BookingExpireDetails> implements BookingExpireDetailsDao {
    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(BookingHistoryDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public BookingExpireDetails getByBookingId(int bookingId) {
        LOGGER.info("getByBookingId method -----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (BookingExpireDetails) currentSession.createCriteria(BookingExpireDetails.class)
                .add(Restrictions.eq("booking", bookingId))
                .uniqueResult();
    }

    public List<BookingExpireDetails> getUpcomingExpireBookings() {
        LOGGER.info("getUpcomingExpireBookings method -----> get call");
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(BookingExpireDetails.class)
                .add(Restrictions.eq("expired", 0))
                .list();
    }

}
