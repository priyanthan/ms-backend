/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;

import com.eventspace.dto.*;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.BookingService;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * The Class SpaceController.
 */
@RestController
@RequestMapping(value = "/api/space")
public class SpaceController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(SpaceController.class);
    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;
    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    @Autowired
    private BookingService bookingService;

    @Value("${cache.enable}")
    public  Boolean enableCache;

    /**
     * Creates the space.
     *
     * @param spaceDetailsDto the space details dto
     * @param request         the request
     * @return the map<string, object>
     * @throws Exception the exception
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> createSpace(@RequestBody SpaceDetailDto spaceDetailsDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("createSpace ----->\t"+spaceDetailsDto.getName()+"\tby"+user.getEmail());
        return spaceService.createSpace(spaceDetailsDto, user);
    }

    /**
     * Update the space.
     *
     * @param id              the id
     * @param spaceDetailsDto the space details dto
     * @param request         the request
     * @return the map<string, object>
     * @throws Exception the exception
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> updateSpace(@PathVariable Integer id,@RequestBody SpaceDetailDto spaceDetailsDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("updateSpace ----->\t"+spaceDetailsDto.getName()+"\tby"+user.getEmail());
        request.getAuthType();
        return spaceService.updateSpace(id, spaceDetailsDto, user.getUserId(), request.isUserInRole("ROLE_ADMIN"));
    }

    /**
     * Get the spaces.
     *
     * @return the list<space>
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<AdminSpaceDetailsDto> getAllSpaces()  {
        LOGGER.info("getAllSpaces  ----->\t");
        return spaceService.getAllSpaces();
    }

    @GetMapping("/data")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminSpaceDetailsDto> getAllSpacesSearchData()  {
        LOGGER.info("getAllSpacesSearchData  ----->\t");
        return spaceService.getSearchSpaceData();
    }
    /**
     * Get a space.
     *
     * @param id the id
     * @return the space details dto
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpace(@PathVariable Integer id,@RequestParam(required = false) Integer[] eventId) {
        Long start=System.currentTimeMillis();
        LOGGER.info("getSpace ----->\t"+id);
        SpaceDetailDto result = spaceService.getSpace(id,eventId,false);
        Long end=System.currentTimeMillis();
        LOGGER.info("GET A SPACE DURATIONMS"+(end-start));
        return result;
    }

    /**
     * Remove the space.
     *
     * @param id the id
     * @return the boolean
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Boolean removeSpace(@PathVariable Integer id) {
        LOGGER.info("removeSpace ----->\t"+id);
        return spaceService.deleteSpace(id);
    }

    /**
     * Filter spaces.
     *
     * @param filterDto the filter dto
     * @return list of space details dto
     */
 /*   @PostMapping("/filter")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> filterSpaces(@RequestBody FilterDto filterDto)  {
        LOGGER.info("filterSpaces method in SpaceController-----> get call");
        return spaceService.getFilteredSpaces(filterDto);
    }*/

    /**
     * Coordinates the space.
     *
     * @param spaceSearchDto the space search dto
     * @return list of space details dto
     */
    /*@PostMapping("/coordinates")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> coordinatesSearch(@RequestBody SpaceSearchDto spaceSearchDto){
        LOGGER.info("coordinatesSearch method in SpaceController-----> get call");
        return spaceService.searchByCoordinates(spaceSearchDto);
    }*/

    /**
     * Is space available with in selected time duration.
     *
     * @param bookingDto the booking dto
     * @return the boolean
     */
    @PostMapping("/isAvailable")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  isSpaceAvailableWIthInSelectedTimeDuration(@RequestBody BookingDto bookingDto) {
        LOGGER.info("isSpaceAvailableWIthInSelectedTimeDuration----->\t"+bookingDto.getSpace());
        return spaceService.isSpaceAvailable(bookingDto);
    }

    /**
     * Get paginated spaces.
     *
     * @param page the page
     * @return the list of space details dto
     */
    @GetMapping("/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getPaginatedSpaces(@PathVariable Integer page)  {
        Long start=System.currentTimeMillis();
        LOGGER.info("getPaginatedSpaces ----->\t"+page);
        Map<String,Object>  result= spaceService.getPaginatedSpaces(page);
        Long end=System.currentTimeMillis();
        LOGGER.info("GET PAGE DURATIONMS"+(end-start));
        return result;
    }

    /**
     * Search spaces.
     *
     * @param searchDto the search dto
     * @return list of space details dto
     */
    /*@PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> searchSpaces(@RequestBody SearchDto searchDto){
        LOGGER.info("searchSpaces method in SpaceController-----> get call");
        return spaceService.searchSpaces(searchDto);
    }*/


    /**
     * advance search.
     *
     * @param advanceSearchDto the advance search dto
     * @return list of space details dto
     */
    @PostMapping("/asearch")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> advanceSearch(@RequestBody AdvanceSearchDto advanceSearchDto)  {
        Long start=System.currentTimeMillis();
        LOGGER.info("advanceSearch ----->\t");
        Map<String,Object> result = spaceService.advanceSearch(advanceSearchDto,null);
        Long end=System.currentTimeMillis();
        LOGGER.info("ADVANCE SEARCH DURATIONMS"+(end-start));
        return result;
    }

    /**
     * paginated advance search.
     *
     * @param advanceSearchDto the advance search dto
     * @return list of space details dto
     */
    @PostMapping("/asearch/page/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> paginatedAdvanceSearch(@RequestBody AdvanceSearchDto advanceSearchDto,@PathVariable Integer page) {
        Long start=System.currentTimeMillis();
        LOGGER.info("advanceSearch ----->\t"+page);
        Map<String,Object> result =spaceService.advanceSearch(advanceSearchDto,page);
        Long end=System.currentTimeMillis();
        LOGGER.info("ADVANCE SEARCH PAGE DURATIONMS"+(end-start));
        return result;
    }

    @PostMapping("/asearch/batch")
    @ResponseStatus(HttpStatus.OK)
    public Map<Integer,Object> batchAdvanceSearch2(@RequestBody List<AdvanceSearchDto> advanceSearchDto) {
        LOGGER.info("batchAdvanceSearch ----->\t");
        return spaceService.batchAdvanceSearch(advanceSearchDto);
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.OK)
    public Map<Integer,Object> batchAdvanceSearch(@RequestBody List<AdvanceSearchDto> advanceSearchDto) {
        LOGGER.info("batchAdvanceSearch ----->\t");
        return spaceService.batchAdvanceSearch(advanceSearchDto);
    }

    @GetMapping("/featured")
    @ResponseStatus(HttpStatus.OK)
    public List<SpaceDetailDto> getFeaturedSpaces()  {
        LOGGER.info("getFeaturedSpaces ----->\t");
        return spaceService.getFeaturedSpaces();
    }

    @PostMapping("/addImages")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean addMiss(@RequestBody SpaceDetailDto spaceDetailsDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("add images  ----->\t"+spaceDetailsDto.getId()+"\tby\t"+user.getEmail());
         spaceService.addMiss(spaceDetailsDto);
         return true;
    }

    @PostMapping("/promo")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> checkPromo(@RequestBody PromoCheckDto promoCheckDto,HttpServletRequest request)  {
        LOGGER.info("checkPromo ----->\t"+promoCheckDto.getPromoCode()+"\tfor\t"+promoCheckDto.getSpace());
        UserContext user = null;
        try {
            user = securityFacade.getUserContext(request);
        } catch (Exception e) {
            LOGGER.info("user didn't login");
        }finally {
            return bookingService.checkPromoCode(promoCheckDto.getSpace(),promoCheckDto.getPromoCode(),user);
        }
    }


    @GetMapping("futureBookedDates/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Map> getFutureBookings(@PathVariable Integer id) {
        LOGGER.info("getFutureBookings ----->\t"+id);
        return spaceService.getUpcomingPaidBookingsDuration(id);
    }

    @GetMapping("calendarDetails/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpaceCalendarDetails(@PathVariable Integer id) {
        LOGGER.info("getSpaceCalendarDetails ----->\t"+id);
        return spaceService.spaceCalendarDetails(id);
    }

    @GetMapping("similarSpaces/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Set<SpaceDetailDto> getSimilarSpaces(@PathVariable Integer id,@RequestParam(required = false) Integer[] eventId) {
        LOGGER.info("getSimilarSpaces ----->\t"+id);
        return spaceService.findSimilarSpaces(id,eventId,false);
    }

    @PostMapping("/link")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean linkSpaces(@RequestBody LinkSpaceDto linkSpaceDto, HttpServletRequest request) {
        LOGGER.info("linkSpaces----->\t"+linkSpaceDto.getLinkSpace());
        //UserContext user = securityFacade.getUserContext(request);
        spaceService.setLinkedSpaces(linkSpaceDto.getOriginSpace(),linkSpaceDto.getLinkSpace());
        return true;
    }

    @PostMapping("/addChilds")
    @ResponseStatus(HttpStatus.CREATED)
    public Boolean addChilds(@RequestBody AddChildsDto addChildsDto, HttpServletRequest request){
        LOGGER.info("addChilds ----->\t"+addChildsDto.getOriginSpace());
        //UserContext user = securityFacade.getUserContext(request);
        spaceService.addChilds(addChildsDto);
        return true;
    }

    @GetMapping("images/{url}")
    @ResponseStatus(HttpStatus.OK)
    public ImageDetailsDto getImageDetails(@PathVariable String url) {
        LOGGER.info("getImageDetails ----->\t"+url);
        return spaceService.getImageDetails(url);
    }

    @PostMapping("/images")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> saveImageDetails(@RequestBody ImageDetailsListDto imageDetailsListDto) {
        LOGGER.info("linkSpaces----->\t");
        return spaceService.saveImageDetails(imageDetailsListDto.getImageDetails());
    }

    @GetMapping("/week")
    @ResponseStatus(HttpStatus.OK)
    public SpaceOfTheWeekDto getSpaceOfTheWeek() {
        LOGGER.info("getSpaceOfTheWeek ----->\t");
        return spaceService.getSpaceOfTheWeek();
    }
}
