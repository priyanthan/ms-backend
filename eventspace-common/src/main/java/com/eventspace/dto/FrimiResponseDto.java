package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FrimiResponseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer tid;

    private Integer request_id;

    private String app_id;

    private String module_id;

    private Integer req_type_id;

    private Date date_time;

    private String sender_id;

    private String body;

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getRequest_id() {
        return request_id;
    }

    public void setRequest_id(Integer request_id) {
        this.request_id = request_id;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public Integer getReq_type_id() {
        return req_type_id;
    }

    public void setReq_type_id(Integer req_type_id) {
        this.req_type_id = req_type_id;
    }

    public Date getDate_time() {
        return date_time;
    }

    public void setDate_time(Date date_time) {
        this.date_time = date_time;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

 class FrimiEncrptedDate{
    private Integer txn_code;
    private Integer frimi_txn_ref_no;
    private String merchant_ref_no;
    private Double discount_amount;
    private String description;

     public Integer getTxn_code() {
         return txn_code;
     }

     public void setTxn_code(Integer txn_code) {
         this.txn_code = txn_code;
     }

     public Integer getFrimi_txn_ref_no() {
         return frimi_txn_ref_no;
     }

     public void setFrimi_txn_ref_no(Integer frimi_txn_ref_no) {
         this.frimi_txn_ref_no = frimi_txn_ref_no;
     }

     public String getMerchant_ref_no() {
         return merchant_ref_no;
     }

     public void setMerchant_ref_no(String merchant_ref_no) {
         this.merchant_ref_no = merchant_ref_no;
     }

     public Double getDiscount_amount() {
         return discount_amount;
     }

     public void setDiscount_amount(Double discount_amount) {
         this.discount_amount = discount_amount;
     }

     public String getDescription() {
         return description;
     }

     public void setDescription(String description) {
         this.description = description;
     }


 }
