package com.eventspace.domain;


import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

public class Guest implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /** The name. */
    @Column(name = "name", nullable= false)
    private String name;

    /** The email. */
    @Column(name="email", nullable= false)
    private String email;

    /** The phone. */
    @Column(name="phone")
    private String phone;

    /** The phone. */
    @Column(name="booking")
    private Integer booking;

    @Override
    public Object build() {
        return null;
    }
}
