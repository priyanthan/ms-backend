/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.filter;

import com.eventspace.dto.AdvanceSearchDto;
import com.eventspace.ws.cache.CacheUtility;
import com.eventspace.ws.cache.MemcacheResponseWrapper;
import com.eventspace.ws.cache.MemcachedFactory;
import com.eventspace.ws.cache.ResettableStreamHttpServletRequest;
import com.eventspace.ws.dto.CachebleDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The Class LoggerFilter.
 */
public class MobileRequestLoggerFilter implements Filter {

    /**
     * The logger.
     */
    private static final Logger LOGGER = Logger.getLogger(MobileRequestLoggerFilter.class);

    private MemcachedFactory cacheFactory;

    private CacheUtility cacheUtility;

    public MobileRequestLoggerFilter() {
        try {
            cacheFactory = MemcachedFactory.getInstance();
        } catch (IOException e) {
            LOGGER.info("ERROR :",e);
        }
        cacheUtility = new CacheUtility();
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy() {
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        try {

            HttpServletRequest http_request = (HttpServletRequest) request;

            String requestMethod = http_request.getMethod();
            String reqeustURL = http_request.getRequestURL().toString();

            LOGGER.info(String.format("Request [%s] | Reqeust Method [%s]", reqeustURL, requestMethod));

            CachebleDTO cacheStatus = cacheUtility.isCacheble(reqeustURL, requestMethod);
            String cacheKey = cacheStatus.getCacheKey();

            if (cacheFactory.isCacheEnabled() && cacheStatus != null && cacheStatus.isCacheble()) {

                if ("POST".equalsIgnoreCase(requestMethod) && http_request.getContentType().equals("application/x-www-form-urlencoded") == false && !http_request.getContentType().equals("application/x-www-form-urlencoded")) {

                    ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(
                            http_request);

                    String payload = IOUtils.toString(wrappedRequest.getReader());

                    LOGGER.info(String.format("Payload - %s", payload));

                    if (cacheStatus.getParentURI().equals("api/space/asearch")) {
                        AdvanceSearchDto asearchDTO = new ObjectMapper().readValue(payload, AdvanceSearchDto.class);
                        cacheKey += String.format("_%s", asearchDTO.hashCode());
                    }
                    if (cacheStatus.getParentURI().equals("api/space/batch")) {
                        List<AdvanceSearchDto> asearchDTO = new ObjectMapper().readValue(payload, new TypeReference<List<AdvanceSearchDto>>(){});
                        cacheKey += String.format("_%s", asearchDTO.hashCode());
                    }

                    wrappedRequest.resetInputStream();

                    request = wrappedRequest;

                }

                Object cachedObject = cacheFactory.get(cacheKey);
                String value;

                // Wrapping the response in HTTPServletResponseWrapper
                // !!!!!!!!!!!!!!!!! Important !!!!!!!!!!!!!!! - This initialization should be here...
                MemcacheResponseWrapper responseWrap = new MemcacheResponseWrapper((HttpServletResponse) response);

                if (cachedObject == null) {
                    chain.doFilter(request, responseWrap);
                    value = responseWrap.getOutputStream().toString();
                    LOGGER.info(String.format("Save to cache for key [%s]", cacheKey));
                    cacheFactory.set(cacheKey, 0, value);
                } else {
                    value = ((String) cachedObject).toString();
                    response.getWriter().println(value);
                }

                LOGGER.info(String.format("\n\n\n===============================\n\n\ncacheKey [%s]\nvalue - %s\n\n\n===============================\n\n\n", cacheKey, value));

            } else {
                if("PUT".equalsIgnoreCase(requestMethod)) {
                    cacheStatus = cacheUtility.isCacheble(reqeustURL, "GET");
                    if (cacheStatus.getParentURI()!=null && cacheStatus.getParentURI().equals("api/space/")) {
                        cacheKey = cacheStatus.getCacheKey();
                        cacheFactory.delete(cacheKey);
                    }
                }
                if("POST".equalsIgnoreCase(requestMethod) && reqeustURL.contains("/api/admin/space/acceptance")){
                    cacheFactory.getCache().flush();
                }
                if("GET".equalsIgnoreCase(requestMethod) && reqeustURL.contains("/api/admin/space/clearCache")){
                    cacheFactory.getCache().flush();
                }

                chain.doFilter(request, response);
            }
        } catch (Exception e) {
            if (e instanceof ServletException) {
                throw ((ServletException) e);
            }
            if (e instanceof IOException) {
                throw ((IOException) e);
            }
            LOGGER.info("ERROR :",e);
            chain.doFilter(request, response);
        }
    }

    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }
}