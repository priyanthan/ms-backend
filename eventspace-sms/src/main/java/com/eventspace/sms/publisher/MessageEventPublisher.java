package com.eventspace.sms.publisher;


import com.eventspace.sms.dto.MessageMetaData;

public interface MessageEventPublisher {

	void proceedSampleEvent(MessageMetaData messageMetaData);

}
