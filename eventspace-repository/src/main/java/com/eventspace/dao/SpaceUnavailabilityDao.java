package com.eventspace.dao;

import com.eventspace.domain.SpaceUnavailability;

import java.util.Date;
import java.util.List;

/**
 * Created by Auxenta on 5/22/2017.
 */

/**
 * The Interface SpaceUnavailabilityDao.
 */
public interface SpaceUnavailabilityDao extends BaseDao<SpaceUnavailability> {

    List<SpaceUnavailability> getUnavaliableDurations(Integer spaceId);

    List<SpaceUnavailability> getAllUnavaliableDurations(Integer spaceId);

    SpaceUnavailability findSpaceUnavailability(Integer spaceId, Date from,Date to);

    List<SpaceUnavailability> getHostManualBookings(Integer userId);

    List<SpaceUnavailability> getSpaceManualBookings(Integer spaceId);
}
