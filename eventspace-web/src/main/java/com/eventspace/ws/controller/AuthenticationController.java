/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;

import com.eventspace.dto.UserContext;
import com.eventspace.dto.UserDetailsDto;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.UserManagementService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Class AuthenticationController.
 */
@RestController
@RequestMapping(value = "/api")
public class AuthenticationController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(AuthenticationController.class);

    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    /**
     * The user manage service.
     */
    @Autowired
    private UserManagementService userManageService;

    /**
     * Login success.
     *
     * @param response the response
     * @param request  the request
     * @return the string
     * @throws Exception the exception
     */
    @RequestMapping(value = "/login/success", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseStatus(HttpStatus.OK)
    public UserDetailsDto loginSuccess(final HttpServletResponse response,
                                       final HttpServletRequest request) throws Exception {
        LOGGER.info("/login/success get called");
        UserContext loggedUser = securityFacade.getUserContext(request);
        UserDetailsDto userDetailsDto=userManageService.getUserByEmail(loggedUser.getEmail());
        if (loggedUser.getRole().equals("ROLE_GUEST")){
            UserDetailsDto guestDetailsDto=new UserDetailsDto();
            guestDetailsDto.setId(userDetailsDto.getId());
            guestDetailsDto.setEmail(userDetailsDto.getEmail());
            guestDetailsDto.setRole("GUEST");
            userDetailsDto=guestDetailsDto;
        }
        userDetailsDto.setPassword(null);
        LOGGER.info("SUCCESS");
        return userDetailsDto;
    }

    /**
     * Logout success.
     *
     * @param response the response
     * @param request  the request
     * @return the integer
     */
    @RequestMapping(value = "/logout/success", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseStatus(HttpStatus.OK)
    public Integer logoutSuccess(final HttpServletResponse response, final HttpServletRequest request) {
        LOGGER.info("/logout/success get called");
        return 100;
    }

    /**
     * User credential not matching.
     *
     * @param response the response
     */
    @RequestMapping(value = "userCredentialNotMatching", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void userCredentialNotMatching(final HttpServletResponse response) {
        LOGGER.info("userCredentialNotMatching request proccessed");
        setStatusHeaders(response, "USER_DETAILS_NOT_MATCHING",
                "Your email/user ID or password is incorrect. Please retry or contact customer support if problem continues.");
    }

    /**
     * Sets the status headers.
     *
     * @param response the response
     * @param code     the code
     * @param message  the message
     */
    private void setStatusHeaders(final HttpServletResponse response, final String code, final String message) {
        response.setHeader("eventspace-code", code);
        response.setHeader("eventspace-message", message);
    }

}
