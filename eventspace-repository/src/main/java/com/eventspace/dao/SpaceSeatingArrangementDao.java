package com.eventspace.dao;

import com.eventspace.domain.SpaceSeatingArrangement;

/**
 * Created by Auxenta on 6/16/2017.
 */
public interface SpaceSeatingArrangementDao extends BaseDao<SpaceSeatingArrangement> {

    void removeSeatingArrangement(Integer spaceeId, Integer seatingArrangementId);
}
