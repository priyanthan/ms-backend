package com.eventspace.dao.Impl;

import com.eventspace.dao.PromoDetailsDao;
import com.eventspace.domain.PromoDetails;
import com.eventspace.enumeration.BooleanEnum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class PromoDetailsDaoImpl extends BaseDaoImpl<PromoDetails> implements PromoDetailsDao {

    @Autowired
    private SessionFactory sessionFactory;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(RulesDaoImpl.class);

    @Override
    public List<PromoDetails> getPromoDetailsOfSpace(Integer space, Date currentDate) {
        LOGGER.info("getPromoDetailsOfHost-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<PromoDetails>) currentSession.createCriteria(PromoDetails.class)
                .add(Restrictions.or(
                        Restrictions.eq("spaces.space", space),
                        Restrictions.eq("toAllSpaces", BooleanEnum.TRUE.value())
                ))
                .add(Restrictions.and(Restrictions.le("startDate",currentDate), Restrictions.ge("endDate", currentDate)))
                .list();
    }

    @Override
    public PromoDetails getPromoByCode(String promoCode) {
        Session currentSession = sessionFactory.getCurrentSession();
          return (PromoDetails)currentSession.createCriteria(PromoDetails.class).add(Restrictions.eq("promoCode",promoCode)).uniqueResult();
    }
}
