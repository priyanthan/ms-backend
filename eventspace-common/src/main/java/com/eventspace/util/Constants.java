/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.util;

/**
 * The Interface Constants.
 */
public interface Constants {


    String MOBILE_BUCKET="http://res.cloudinary.com/dgcojyezg/image/upload/v1509511785/mobile-icons/";
    /**
     * The empty.
     */
    String EMPTY = "";

    /**
     * The date time format.
     */
    String DATE_TIME_FORMAT = "MMM-dd-yyyy HH:mm";

    /**
     * The date time format.
     */
    String DATE_TIME_WITHSECOND_FORMAT = "MMM-dd-yyyy HH:mm:ss";

    /**
     * The date time format.
     */
    String DATE_FORMAT = "yyyy-MM-dd";

    String INSPECTION_DATE_FORMAT = "HH:mm dd MMM";


    String PATTERN_1= "yyyy-MM-dd'T'HH:mm'Z'";

    String PATTERN_2= "yyyy-MM-dd";

    String PATTERN_3= "MM/dd/yyyy";

    String PATTERN_4= "hh:mm:ss a";

    String PATTERN_5= "yyyy-MM-dd HH:mm:ss";

    String PATTERN_6= "yyyy-MM-dd HH:mm";

    String PATTERN_7= "yyyy-MM-dd'T'HH:mm";

    String PATTERN_8= "dd MMM yy";

    String PATTERN_9= "hh:mm a";

    String PATTERN_10= "yyyy-MM-dd HH:mm:ss";

    String PATTERN_11= "ha";

    String RESERVATION_STATUS = "reservationStatus";

    String TO_DATE="toDate";

    String FROM_DATE="fromDate";

}
