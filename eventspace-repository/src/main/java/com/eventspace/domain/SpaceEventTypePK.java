/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Embeddable
public class SpaceEventTypePK implements Serializable {

	private static final long serialVersionUID = 1L;

	@JoinColumn(name = "space", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private Space space;

	@JoinColumn(name = "event_type", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY,cascade =CascadeType.ALL)
	private EventType eventType;
	
	public SpaceEventTypePK() {}

	public SpaceEventTypePK(Space space, EventType eventType) {
		this.space = space;
		this.eventType = eventType;
	}

	public Space getSpace() {
		return space;
	}

	public EventType getEventType() {
		return eventType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((space == null) ? 0 : space.hashCode());
		result = prime * result + ((eventType == null) ? 0 : eventType.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpaceEventTypePK other = (SpaceEventTypePK) obj;
		if (space == null) {
			if (other.getSpace() != null)
				return false;
		} else if (!space.equals(other.space))
			return false;
		if (eventType == null) {
			if (other.eventType != null)
				return false;
		} else if (!eventType.equals(other.eventType))
			return false;
		return true;
	}

}
