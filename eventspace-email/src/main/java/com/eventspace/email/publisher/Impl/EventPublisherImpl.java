package com.eventspace.email.publisher.Impl;

import com.eventspace.email.dto.EmailMetaData;
import com.eventspace.email.publisher.EventPublisher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

@Component
public class EventPublisherImpl implements EventPublisher {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(EventPublisherImpl.class);

	@Autowired
	@Qualifier("emailjmsProducerTemplate")
	private final JmsTemplate jmsTemplate = null;

	@Override
	public void proceedEmailEvent(final EmailMetaData emailMetaData) {
		logger.info(String.format(
				"proceedEmailEvent -> get called with emailMetaData : Subject[%s], ToEmails[%s], VmFile[%s]",
				emailMetaData.getSubject(), emailMetaData.getToEmailAddresses(), emailMetaData.getVmFile()));

		jmsTemplate.send(new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {

				ObjectMessage objMessage = session.createObjectMessage();
				objMessage.setObject(emailMetaData);

				return objMessage;
			}
		});
	}
}
