package com.eventspace.dao.Impl;

import com.eventspace.dao.ReviewsDao;
import com.eventspace.domain.Reviews;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.eventspace.domain.Reviews.AVG_RATE_SPACE_SP;

/**
 * Event space
 * Created by Aux-054 on 12/16/2016.
 */
@Repository
public class ReviewDaoImpl extends BaseDaoImpl<Reviews> implements ReviewsDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;



    public Double getRatingBySpaceId(Integer spaceId) {
        LOGGER.info("getRatingBySpaceId: in ReviewDaoImpl.spaceId: {}",spaceId);
        Session currentSession = sessionFactory.getCurrentSession();

        Query query = currentSession.getNamedQuery(AVG_RATE_SPACE_SP)
                .setParameter("spaceId", spaceId);
        Double review= (Double)query.uniqueResult();
        Double rate =0.0;
        if (review != null)
            rate=review;
        return rate;
    }

    public Boolean checkReviewByBookingId(Integer bookingId){
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.createCriteria(Reviews.class).add(Restrictions.eq("bookingId",bookingId)) .uniqueResult() != null;
    }

    public Reviews getReviewByBookingId(Integer bookingId){
        Session currentSession = sessionFactory.getCurrentSession();
        return (Reviews) currentSession.createCriteria(Reviews.class).add(Restrictions.eq("bookingId",bookingId)).uniqueResult();
    }

    @Override
    public List<Reviews> getMSReviews() {
        Session currentSession = sessionFactory.getCurrentSession();
        return (List<Reviews>) currentSession.createCriteria(Reviews.class)
                .add(Restrictions.isNotNull("msExperience"))
                .addOrder(Order.desc("id"))
                .setFirstResult(0)
                .setMaxResults(3)
                .list();
    }
}
