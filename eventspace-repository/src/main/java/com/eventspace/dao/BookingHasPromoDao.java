package com.eventspace.dao;

import com.eventspace.domain.BookingHasPromo;

public interface BookingHasPromoDao extends BaseDao<BookingHasPromo> {
}
