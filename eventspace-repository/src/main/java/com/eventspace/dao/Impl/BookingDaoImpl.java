package com.eventspace.dao.Impl;

import com.eventspace.dao.BookingDao;
import com.eventspace.domain.Booking;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.util.Constants;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.eventspace.domain.Booking.*;

/**
 * Created by Aux-054 on 12/15/2016.
 */
@Repository
public class BookingDaoImpl extends BaseDaoImpl<Booking> implements BookingDao {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Booking> getBookings(Integer id) {
        LOGGER.info("getBookings-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        String hql = "Select b FROM Booking b , Space s WHERE s.id=b.space and (b.user=%s or s.user=%s ) and reservation_status not in("
                +StatesEnum.INITIATED.value()+","+StatesEnum.DISCARDED.value()
                +") group by b.id";
        return (List<Booking>) currentSession.createQuery(String.format(hql, id, id)).list();
    }

    @Override
    public Double getBookingCharge(Integer bookingId) {
        LOGGER.info("getBookingCharge-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        Query query = currentSession.getNamedQuery(SPACE_BOOKING_CHARGE_SP)
                .setParameter("bookingId", bookingId);
        BigDecimal charge = (BigDecimal) query.uniqueResult();
        Double bookingCharge = 0.0;
        if (charge != null)
            bookingCharge = charge.doubleValue();

        return bookingCharge;
    }

    //is cancelled after paid
    @Override
    public Boolean isCancelledAfterPaid(Integer bookingId) {
        LOGGER.info("isCancelledAfterPaid-----> get call");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery(IS_CANCELLED_AFTER_PAID_SP)
                .setParameter("booking_id", bookingId);
        return (Boolean) query.uniqueResult();
    }

    @Override
    public Date getCancelledDate(Integer id) {
        LOGGER.info("getCancelledDate-----> get call");
        Session session = sessionFactory.getCurrentSession();
        Query query = session.getNamedQuery(GET_CANCELLED_DATE_SP)
                .setParameter("bookingId", id)
                .setParameter("bookingStatus", StatesEnum.CANCELLED.value());
        return (Date) query.uniqueResult();
    }

    @Override
    public List<Booking> getBookingsBySpaceId(Integer spaceId) {
        LOGGER.info("getBookingsBySpaceId-----> get call");
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Booking.class)
                .add(Restrictions.eq("space", spaceId))
                .add(Restrictions.and(Restrictions.ne("reservationStatus",StatesEnum.INITIATED.value()),
                        Restrictions.ne("reservationStatus",StatesEnum.DISCARDED.value())))
                .list();
    }

    @Override
    public List<Booking> getBookingsEventsHeldToday() {
        LOGGER.info("getBookingsEventsHeldToday-----> get call");
        Session session = sessionFactory.getCurrentSession();

        Date today = new Date();
        DateFormat df = new SimpleDateFormat(Constants.PATTERN_6);
        //Date yesterDay = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);

        String sql = "Select b FROM Booking b , BookingSlots s WHERE b.id=s.booking and  b.reservationStatus ="
                +StatesEnum.PAYMENT_DONE.value() +" and  s.toDate < '" + df.format(today)
                +"' group by b.id";
        return (List<Booking>) session.createQuery(String.format(sql)).list();
    }

    public List<Booking> getAllBooking(Integer page) {
        LOGGER.info("getAllBooking-----> get call");
        int maxResult = 10;
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.createCriteria(Booking.class)
                .setFirstResult(page * maxResult)
                .setMaxResults(maxResult)
                .addOrder(Order.desc("id"))
                .list();
    }

    @Override
    public List<Booking> getUpcomingPaidBookingsBySpaceId(Integer spaceId) {
        LOGGER.info("getUpcomingPaidBookingsBySpaceId-----> get call");
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Booking.class)
                .add(Restrictions.eq("space", spaceId))
                .add(Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.PAYMENT_DONE.value()))
                .add(Restrictions.ge(Constants.FROM_DATE, new Date()))
                .list();
    }

    @Override
    public Boolean isUserAllowForBooking(Integer user) {
        LOGGER.info("isUserAllowForBooking-----> get call");
        boolean have = false;
        Session session = sessionFactory.getCurrentSession();
        List<Booking> bookings = session.createCriteria(Booking.class)
                .add(Restrictions.eq("user", user))
                .add(Restrictions.or(Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.PENDING_PAYMENT.value()),
                        Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.ADVANCE_PAYMENT_PENDING.value())
                        ))
                .list();
        if (bookings.isEmpty())
            have = true;
        return have;
    }

    @Override
    public List<Booking> missedDiscardBookings() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Booking.class)
                .add(Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.INITIATED.value()))
                .list();

    }

    @Override
    public List<Booking> getBookingByUserId(Integer userId) {
        LOGGER.info("getBookingByUserId-----> get call");
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Booking.class)
                .add(Restrictions.eq("user", userId))
                .add(Restrictions.and(Restrictions.ne(Constants.RESERVATION_STATUS,StatesEnum.INITIATED.value()),
                        Restrictions.ne(Constants.RESERVATION_STATUS,StatesEnum.DISCARDED.value())))
                .list();
    }

    @Override
    public List<Booking> getBookingByHostId(Integer hostId) {
        LOGGER.info("getBookingByHostId-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        String hql = "Select b FROM Booking b , Space s WHERE s.id=b.space and  s.user=%s  and reservation_status not in("
                +StatesEnum.INITIATED.value()+","+StatesEnum.DISCARDED.value()
                +") group by b.id";
        return (List<Booking>) currentSession.createQuery(String.format(hql,hostId)).list();
    }

    @Override
    public Long getAllBookingCount() {
        LOGGER.info("getAllBookingCount-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (Long)currentSession.createQuery("select count(*) from Booking ").uniqueResult();
    }

    @Override
    public List<Booking> getAllTentitiveBookings(Integer page) {
        LOGGER.info("getAllTentitiveBookings-----> get call");
        Session session = sessionFactory.getCurrentSession();
        Integer maxResult=10;
        return session.createCriteria(Booking.class)
                .add(Restrictions.or(Restrictions.eq(Constants.RESERVATION_STATUS,StatesEnum.PENDING_PAYMENT.value()),Restrictions.eq(Constants.RESERVATION_STATUS,StatesEnum.ADVANCE_PAYMENT_PENDING.value())))
                .setFirstResult(page * maxResult)
                .setMaxResults(maxResult)
                .addOrder(Order.desc("id"))
                .list();
    }

    @Override
    public List<Booking> getPaidBookings(Integer page) {
        LOGGER.info("getUpcomingPaidBookingsBySpaceId-----> get call");
        int maxResult=10;
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Booking.class)
                .add(Restrictions.or(
                        Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.PAYMENT_DONE.value()),
                        Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.CONFIRMED.value()),
                        Restrictions.eq(Constants.RESERVATION_STATUS, StatesEnum.ADVANCE_PAYMENT_DONE.value())
                ))
                .setFirstResult(page * maxResult)
                .setMaxResults(maxResult)
                .addOrder(Order.desc("id"))
                .list();
    }

    @Override
    public Long getCount(Integer bookingStatus) {
        LOGGER.info("getCount-----> get call");
        Session session = sessionFactory.getCurrentSession();

        String sql="select count(*) from Booking where reservationStatus="+bookingStatus;
        return (Long)session.createQuery(String.format(sql)).uniqueResult();
    }

    @Override
    public Booking getAUserLatestBooking(Integer userId) {
        LOGGER.info("readUserByName in  UserDaoImpl.userName is {}" , userId);
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Booking.class)
                .add(Restrictions.eq("user", userId))
                .addOrder(Order.desc("id"))
                .setFirstResult(0)
                .setMaxResults(1);
        return (Booking) criteria.uniqueResult();
    }

    @Override
    public Long getAUserBookingCount(Integer userId) {
        LOGGER.info("getAUserBookingCount-----> get call");
        Session session = sessionFactory.getCurrentSession();

        String sql="select count(*) from Booking where user="+userId+
                " and reservation_status!="+StatesEnum.INITIATED.value()+" and reservation_status!="+StatesEnum.DISCARDED.value();
        Long count=(Long)session.createQuery(String.format(sql)).uniqueResult();
        if (count==null)
            count=0L;
        return count;
    }

    @Override
    public List<Booking> getAUserBookingWithPagination(Integer userId,Integer page) {
        LOGGER.info("getAUserBookingWithPagination-----> get call");
        Session session = sessionFactory.getCurrentSession();
        int maxResult=10;
        return session.createCriteria(Booking.class)
                .add(Restrictions.eq("user", userId))
                .add(Restrictions.and(Restrictions.ne(Constants.RESERVATION_STATUS,StatesEnum.INITIATED.value()),
                        Restrictions.ne(Constants.RESERVATION_STATUS,StatesEnum.DISCARDED.value())))
                .setFirstResult(page * maxResult)
                .setMaxResults(maxResult)
                .list();
    }


    @Override
    public Integer getNoOfBookingOfSpaceForATimSlot(Integer spaceId, Date startDate, Date endDate) {
        LOGGER.info("getNoOfBookingOfSpaceForATimSlot-----> get call");
        Session session = sessionFactory.getCurrentSession();

        Date today = new Date();
        DateFormat df = new SimpleDateFormat(Constants.PATTERN_6);

        String sql = "Select count(b.*) FROM Booking b , BookingSlots s WHERE b.id=s.booking and  b.reservationStatus ="
                +StatesEnum.PAYMENT_DONE.value() +" and  s.fromDate = '" + df.format(startDate)+" and  s.toDate = '" + df.format(endDate)
                +"' group by b.id";
        return (Integer) session.createQuery(String.format(sql)).uniqueResult();
    }


    @Override
    public Integer getNoOfBookingByUserUsingAPromo(Integer userId, Integer promoCodeId) {
        LOGGER.info("getNoOfBookingByUserUsingAPromo-----> get call");
        Session session = sessionFactory.getCurrentSession();


        StringBuilder sql = new StringBuilder("Select count(*) FROM Booking b ,BookingHasPromo bp WHERE b.id=bp.booking and  b.reservationStatus in (")
                .append(StatesEnum.PAYMENT_DONE.value() ).append(",").append(StatesEnum.INITIATED.value())
                .append(",").append(StatesEnum.CONFIRMED.value()).append(",").append(StatesEnum.PENDING_PAYMENT.value())
                .append(") and bp.promoDetails=").append(promoCodeId ).append(" and b.user=").append(userId);
        return ((Long)session.createQuery(String.format(sql.toString())).uniqueResult()).intValue();
    }

    @Override
    public Integer getNoOFBookingsForAPromo(Integer promoCodeId) {
        LOGGER.info("getNoOFBookingsForAPromo-----> get call");
        Session session = sessionFactory.getCurrentSession();


        StringBuilder sql = new StringBuilder("Select count(*) FROM Booking b ,BookingHasPromo bp WHERE b.id=bp.booking and  b.reservationStatus in (")
                .append(StatesEnum.PAYMENT_DONE.value() ).append(",").append(StatesEnum.INITIATED.value())
                .append(",").append(StatesEnum.CONFIRMED.value()).append(",").append(StatesEnum.PENDING_PAYMENT.value())
                .append(") and bp.promoDetails=").append(promoCodeId );
        return ((Long)session.createQuery(String.format(sql.toString())).uniqueResult()).intValue();

    }
}
