package com.eventspace.domain;

import com.eventspace.dto.ImageDetailsDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="image_details")
public class ImageDetails implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;


    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "url", nullable = false,unique=true)
    private String url;

    @Column(name = "delete_token")
    private String deleteToken ;

    @Column(name = "public_id")
    private String publicId;

    @Column(name = "etag")
    private String etag;

    @Column(name = "signature")
    private String signature;

    @Column(name = "secure_url")
    private String secureUrl;

    @Column(name = "active")
    private String active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDeleteToken() {
        return deleteToken;
    }

    public void setDeleteToken(String deleteToken) {
        this.deleteToken = deleteToken;
    }

    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getSecureUrl() {
        return secureUrl;
    }

    public void setSecureUrl(String secureUrl) {
        this.secureUrl = secureUrl;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public ImageDetailsDto build() {
        ImageDetailsDto imageDetailsDto=new ImageDetailsDto();
        imageDetailsDto.setUrl(url);
        imageDetailsDto.setDeleteToken(deleteToken);
        imageDetailsDto.setEtag(etag);
        imageDetailsDto.setPublicId(publicId);
        imageDetailsDto.setSecureUrl(secureUrl);
        imageDetailsDto.setSignature(signature);
        return imageDetailsDto;
    }

    public static ImageDetails build(ImageDetailsDto imageDetailsDto) {
        ImageDetails imageDetails=new ImageDetails();
        imageDetails.setUrl(imageDetailsDto.getUrl());
        imageDetails.setEtag(imageDetailsDto.getEtag());
        imageDetails.setDeleteToken(imageDetailsDto.getDeleteToken());
        imageDetails.setPublicId(imageDetailsDto.getPublicId());
        imageDetails.setSecureUrl(imageDetailsDto.getSecureUrl());
        imageDetails.setSignature(imageDetailsDto.getSignature());
        return imageDetails;
    }
}
