package com.eventspace.ws.controller;

import com.eventspace.dto.SpaceDetailDto;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/calendarDetails")
public class CalendarDetailsController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(CalendarDetailsController.class);
    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    @GetMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpaceCalendarDetails(@PathVariable Integer id) {
        LOGGER.info("getSpaceCalendarDetails ----->\t"+id);
        return spaceService.spaceCalendarDetails(id);
    }

    @GetMapping("/host/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getHostSpaceCalendarDetails(@PathVariable Integer id) {
        LOGGER.info("getHostSpaceCalendarDetails ----->\t"+id);
        return spaceService.hostCalendarDetails(id);
    }


}
