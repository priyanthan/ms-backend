package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.PaySubEventEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

public class ManualBookingEventActionServiceImpl extends EventActionServices {
    private static final Log logger = LogFactory.getLog(PaymentEventActionServiceImpl.class);


    /**
     * @param bookingContextDto
     */
    public ManualBookingEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    /**
     * @param stateContext
     */
    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action MANUAL PAYMENT DONE OrderID : %s",booking.getOrderId()));

        updateBookingDetails(bookingActionDto,StatesEnum.PENDING_PAYMENT.value());
        if(bookingActionDto.getMethod().equals("MANUAL")  && bookingActionDto.getStatus().equals(PaySubEventEnum.PAY_LATER.value())) {
            logger.info("pay later :" +booking.getOrderId());
            bookingService.createPayLaterRecord(booking.getId());
            emailService.sendBookingActionMails(booking.getId(), 11);
        }else {
            emailService.sendBookingActionMails(booking.getId(), EventsEnum.WAIT_FOR_PAY.value());
        }
        //bookingService.addBookingToSpaceUnavailablity(booking.getBookingSlots());

    }

}

