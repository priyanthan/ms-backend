package com.eventspace.dao.Impl;

import com.eventspace.dao.AutoPublishDao;
import com.eventspace.domain.AutoPublish;
import org.springframework.stereotype.Repository;


/**
 * Created by Auxenta on 6/16/2017.
 */
@Repository
public class AutoPublishDaoImpl  extends BaseDaoImpl<AutoPublish> implements AutoPublishDao {
}
