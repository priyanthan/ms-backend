package com.eventspace.eventaction.dto;

import com.eventspace.dao.*;
import com.eventspace.domain.Booking;
import com.eventspace.domain.BookingSpaceExtraAmenity;
import com.eventspace.dto.BookingActionDto;
import com.eventspace.dto.BookingDto;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.email.sender.EmailSender;
import com.eventspace.service.BookingService;
import com.eventspace.service.EmailService;

import java.io.Serializable;

/**
 * Event space
 * Created by Aux-054 on 1/9/2017.
 */
public class BookingContextDto implements Serializable {

    private transient Booking booking;

    private transient BookingDao bookingDao;

    private transient BookingDto bookingDto;

    private transient UserDao userDao;

    private transient SpaceDao spaceDao;

    private transient AmenityDao amenityDao;

    private transient EventPublisher eventPublisher;

    private BookingActionDto bookingActionDto;

    private transient EmailSender emailSender;

    private transient BookingService bookingService;

    private transient EmailService emailService;

    private transient BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao;

    private BookingSpaceExtraAmenity bookingSpaceExtraAmenity;

    public BookingService getBookingService() {
        return bookingService;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public BookingDao getBookingDao() {
        return bookingDao;
    }

    public void setBookingDao(BookingDao bookingDao) {
        this.bookingDao = bookingDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public SpaceDao getSpaceDao() {
        return spaceDao;
    }

    public void setSpaceDao(SpaceDao spaceDao) {
        this.spaceDao = spaceDao;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    public void setEventPublisher(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public BookingActionDto getBookingActionDto() {
        return bookingActionDto;
    }

    public void setBookingActionDto(BookingActionDto bookingActionDto) {
        this.bookingActionDto = bookingActionDto;
    }

    public EmailSender getEmailSender() {
        return emailSender;
    }

    public void setEmailSender(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    public BookingDto getBookingDto() {
        return bookingDto;
    }

    public void setBookingDto(BookingDto bookingDto) {
        this.bookingDto = bookingDto;
    }

    public BookingSpaceExtraAmenityDao getBookingSpaceExtraAmenityDao() {
        return bookingSpaceExtraAmenityDao;
    }

    public void setBookingSpaceExtraAmenityDao(BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao) {
        this.bookingSpaceExtraAmenityDao = bookingSpaceExtraAmenityDao;
    }

    public BookingSpaceExtraAmenity getBookingSpaceExtraAmenity() {
        return bookingSpaceExtraAmenity;
    }

    public void setBookingSpaceExtraAmenity(BookingSpaceExtraAmenity bookingSpaceExtraAmenity) {
        this.bookingSpaceExtraAmenity = bookingSpaceExtraAmenity;
    }

    public AmenityDao getAmenityDao() {
        return amenityDao;
    }

    public void setAmenityDao(AmenityDao amenityDao) {
        this.amenityDao = amenityDao;
    }

    public EmailService getEmailService() {
        return emailService;
    }

    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }
}
