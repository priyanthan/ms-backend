package com.eventspace.domain;

import com.eventspace.dto.AmenityDto;

import javax.persistence.EmbeddedId;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
/**
 * The Class SpaceAmenity.
 */
//@Entity
//@Table(name = "space_has_amenity")
public class SpaceAmenity implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;


    @EmbeddedId
    private SpaceAmenityPK pk;

    //@ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "space",insertable=false,updatable=false)
    private Space space;

    //@ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "amenity",insertable=false,updatable=false)
    private Amenity amenity;


    public SpaceAmenityPK getPk() {
        if(pk==null)
            pk=new SpaceAmenityPK(space,amenity);
        return pk;
    }

    public void setPk(SpaceAmenityPK pk) {
        if(pk!=null){
            this.space=pk.getSpace();
            this.amenity=pk.getAmenity();
        }
        this.pk = pk;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Amenity getAmenity() {
        return amenity;
    }

    public void setAmenity(Amenity amenity) {
        this.amenity = amenity;
    }





    /* (non-Javadoc)
	 * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
	 */

    //set spaceAmenity to smenity
    @Override
    public AmenityDto build() {
        AmenityDto amenityDto=new AmenityDto();
        amenityDto.setId(amenity.getId());
        amenityDto.setName(amenity.getName());
        amenityDto.setAmenityUnit(amenity.getAmenity_unit());

        return amenityDto;
    }

    //set space & amenity to spaceamenity
    public static SpaceAmenity build(Space space,Amenity amenity) {
        SpaceAmenity spaceAmenity=new SpaceAmenity();
        spaceAmenity.setAmenity(amenity);
        spaceAmenity.setSpace(space);
        spaceAmenity.setPk(new SpaceAmenityPK(space, amenity));

        return spaceAmenity;
    }
}


