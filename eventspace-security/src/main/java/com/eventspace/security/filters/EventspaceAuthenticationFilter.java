/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.filters;

import com.eventspace.security.domain.EventspaceAuthenticationToken;
import org.apache.log4j.Logger;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;


/**
 * The Class EventspaceAuthenticationFilter.
 */
public class EventspaceAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

	/** The log. */
	private static Logger log = Logger.getLogger(EventspaceAuthenticationFilter.class);

	/** The is post. */
	private final boolean isPost = true;

	/** The Constant GRANT_SECURITY. */
	private static final String GRANT_SECURITY = "/api/grant_eventspace_security";

	/** The Constant USERNAME. */
	public static final String USERNAME = "username";

	/** The Constant PASSWORD. */
	public static final String PASSWORD = "password";

	/** The Constant APP_VERSION. */
	public static final String APP_VERSION = "app_version";

	/** The Constant DEVICE. */
	public static final String DEVICE = "device";

	/** The Constant AUTHENTICATION_STATUS. */
	private static final String AUTHENTICATION_STATUS = "AuthenticationStatus";

	/** The Constant AUTHENTICATION_SUCCESS. */
	private static final String AUTHENTICATION_SUCCESS = "AuthenticationSuccess";

	/** The Constant AUTHENTICATION_FAILED. */
	private static final String AUTHENTICATION_FAILED = "AuthenticationFailed";
	
	private static final String GOOGLE_TOKEN = "googleToken";
	
	private static final String FACEBOOK_TOKEN = "facebookToken";

	/** The encryptor and decryptor. */
	@Autowired
	private StandardPBEStringEncryptor encryptorAndDecryptor;


	/**
	 * The Constructor.
	 */
	protected EventspaceAuthenticationFilter() {
		super(GRANT_SECURITY);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.web.authentication.
	 * AbstractAuthenticationProcessingFilter
	 * #attemptAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(final HttpServletRequest request,
			final HttpServletResponse response) throws AuthenticationException {

		if (isPost && !request.getMethod().equals("POST")) {
			log.error("Authentication method not supported: " + request.getMethod());
			throw new AuthenticationServiceException("Authentication method not supported: "
					+ request.getMethod());
		}

		String userName = obtainUsername(request);
		String password = obtainPassword(request);
		String googleToken = obtainGoogleToken(request);
		String facebookToken = obtainFacebookToken(request);

		EventspaceAuthenticationToken restAuthenticationToken = new EventspaceAuthenticationToken(userName,
				password, googleToken,facebookToken);
		restAuthenticationToken.setDetails(authenticationDetailsSource.buildDetails(request));

		return this.getAuthenticationManager().authenticate(restAuthenticationToken);
	}

	/* * (non-Javadoc)
	 *
	 * @see org.springframework.security.web.authentication.
	 * AbstractAuthenticationProcessingFilter
	 * #successfulAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, javax.servlet.FilterChain,
	 * org.springframework.security.core.Authentication)
	 */
	@Override
	protected void successfulAuthentication(final HttpServletRequest request,
			final HttpServletResponse response, final FilterChain chain, final Authentication authResult)
			throws IOException, ServletException {
		log.debug(AUTHENTICATION_STATUS + AUTHENTICATION_SUCCESS + new Date());
		super.successfulAuthentication(request, response, chain, authResult);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.web.authentication.
	 * AbstractAuthenticationProcessingFilter
	 * #unsuccessfulAuthentication(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse,
	 * org.springframework.security.core.AuthenticationException)
	 */
	@Override
	protected void unsuccessfulAuthentication(final HttpServletRequest request,
			final HttpServletResponse response, final AuthenticationException failed) throws IOException,
			ServletException {
		log.debug(AUTHENTICATION_STATUS + AUTHENTICATION_FAILED + new Date());
		super.unsuccessfulAuthentication(request, response, failed);
	}

	/**
	 * Obtain password.
	 *
	 * @param request
	 *            the request
	 * @return the string
	 */
	protected String obtainPassword(final HttpServletRequest request) {
		if (request.getParameter(PASSWORD) != null
				&& !request.getParameter(PASSWORD).isEmpty()) {
			return request.getParameter(PASSWORD);
		} else
			return null;
	}

	/**
	 * Obtain username.
	 *
	 * @param request
	 *            the request
	 * @return the string
	 */
	protected String obtainUsername(final HttpServletRequest request) {
		return request.getParameter(USERNAME);
	}
	
	protected String obtainGoogleToken(final HttpServletRequest request){
		return request.getParameter(GOOGLE_TOKEN);
	}
	
	protected String obtainFacebookToken(final HttpServletRequest request){
		return request.getParameter(FACEBOOK_TOKEN);
	}

}
