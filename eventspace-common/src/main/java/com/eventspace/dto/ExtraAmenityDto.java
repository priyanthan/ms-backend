package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * The Class ExtraAmenityDto.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtraAmenityDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    /**
     * The amenity id.
     */
    private Integer amenityId;

    private String name;

    private Integer amenityUnit;

    private String amenityIcon;

    private String mobileIcon;

    /**
     * The extra rate.
     */
    private Double extraRate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the amenity id.
     *
     * @return the amenity id
     */
    public Integer getAmenityId() {
        return amenityId;
    }

    /**
     * Sets the amenity id.
     *
     * @param amenityId the new amenity id
     */
    public void setAmenityId(Integer amenityId) {
        this.amenityId = amenityId;
    }

    public Integer getAmenityUnit() {
        return amenityUnit;
    }

    public void setAmenityUnit(Integer amenityUnit) {
        this.amenityUnit = amenityUnit;
    }

    /**
     * Gets the extra rate.
     *
     * @return the extra rate
     */
    public Double getExtraRate() {
        return extraRate;
    }

    /**
     * Sets the extra rate.
     *
     * @param extraRate the new extra rate
     */
    public void setExtraRate(Double extraRate) {
        this.extraRate = extraRate;
    }

    public String getAmenityIcon() {
        return amenityIcon;
    }

    public void setAmenityIcon(String amenityIcon) {
        this.amenityIcon = amenityIcon;
    }

    public String getMobileIcon() {
        return mobileIcon;
    }

    public void setMobileIcon(String mobileIcon) {
        this.mobileIcon = mobileIcon;
    }
}
