package com.eventspace.domain;

import com.eventspace.dto.MenuFileDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "menu_files")
public class MenuFiles implements Serializable, BaseDomain {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "menu_id")
    private String menuId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    @Override
    public MenuFileDto build() {
        MenuFileDto menuFileDto=new MenuFileDto();
        menuFileDto.setId(id);
        menuFileDto.setUrl(url);
        menuFileDto.setMenuId(menuId);
        return menuFileDto;
    }

    public static MenuFiles build(MenuFileDto menuFileDto){
        MenuFiles menuFiles=new MenuFiles();
        menuFiles.setUrl(menuFileDto.getUrl());
        menuFiles.setMenuId(menuFileDto.getMenuId());
        return menuFiles;
    }
}
