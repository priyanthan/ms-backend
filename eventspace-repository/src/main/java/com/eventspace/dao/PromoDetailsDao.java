package com.eventspace.dao;

import com.eventspace.domain.PromoDetails;

import java.util.Date;
import java.util.List;

public interface PromoDetailsDao extends BaseDao<PromoDetails>   {

    List<PromoDetails> getPromoDetailsOfSpace(Integer space,Date currentDate);

    PromoDetails getPromoByCode(String promoCode);
}
