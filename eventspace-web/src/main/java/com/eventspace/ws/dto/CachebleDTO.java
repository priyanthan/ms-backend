package com.eventspace.ws.dto;

import com.eventspace.util.StringUtils;

public class CachebleDTO {

    private String childKey;
    private String parentURI;
    private boolean isCacheble;

    public String getChildKey() {
        return childKey;
    }

    public void setChildKey(String childKey) {
        while (!StringUtils.isEmpty(childKey) && childKey.substring(0, 1).equals("/")) {
            childKey = childKey.substring(1);
        }
        this.childKey = childKey;
    }

    public String getParentURI() {
        return parentURI;
    }

    public void setParentURI(String parentURI) {
        this.parentURI = parentURI;
    }

    public boolean isCacheble() {
        return isCacheble;
    }

    public void setCacheble(boolean cacheble) {
        isCacheble = cacheble;
    }

    public String getCacheKey() {
        String cacheKey = "";

        if (!StringUtils.isEmpty(parentURI))
            cacheKey = parentURI;

        if (!StringUtils.isEmpty(childKey))
            cacheKey += "_"+childKey;

        return cacheKey.trim();
    }
}
