package com.eventspace.domain;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Embeddable
public class BookingSpaceAmenityPK implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    private Space space;

    @ManyToOne(fetch = FetchType.LAZY)
    private Booking booking;

    @ManyToOne(fetch = FetchType.LAZY)
    private Amenity amenity;

    public BookingSpaceAmenityPK(Space space, Amenity amenity, Booking booking) {
        this.space = space;
        this.booking = booking;
        this.amenity = amenity;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public void setAmenity(Amenity amenity) {
        this.amenity = amenity;
    }

    public BookingSpaceAmenityPK(){}

    public Booking getBooking() {
        return booking;
    }

    public Space getSpace() {
        return space;
    }

    public Amenity getAmenity() {
        return amenity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((space == null) ? 0 : space.hashCode());
        result = prime * result + ((amenity == null) ? 0 : amenity.hashCode());
        result = prime * result + ((booking == null) ? 0 : booking.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BookingSpaceAmenityPK other = (BookingSpaceAmenityPK) obj;
        if (space == null) {
            if (other.space != null)
                return false;
        } else if (!space.equals(other.space))
            return false;
        if (amenity == null) {
            if (other.amenity != null)
                return false;
        } else if (!amenity.equals(other.amenity))
            return false;
        if (booking == null) {
            if (other.booking != null)
                return false;
        } else if (!booking.equals(other.booking))
            return false;
        return true;
    }


}
