package com.eventspace.domain;

import com.eventspace.dto.SpaceOfTheWeekDto;
import com.eventspace.enumeration.BooleanEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "space_of_week")
public class SpaceOfWeek implements Serializable, BaseDomain   {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "space",insertable=false,updatable=false)
    private Space space;

    @Column(name = "description")
    private String description;

    @Column(name = "img_url")
    private String imgUrl;

    @Column(name = "created_at")
    private Date createAt;

    @Column(name = "active")
    private Integer active;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public SpaceOfTheWeekDto build() {
        SpaceOfTheWeekDto spaceOfTheWeekDto=new SpaceOfTheWeekDto();
        Map<String,Object> spaceObj=new HashMap<>();
        spaceOfTheWeekDto.setId(id);
        spaceOfTheWeekDto.setSpaceId(space.getId());
        spaceObj.put("id",space.getId());
        spaceObj.put("name",space.getName());
        spaceObj.put("address",space.getAddressLine1());
        spaceObj.put("organization",space.getAddressLine2());
        spaceOfTheWeekDto.setSpace(spaceObj);
        spaceOfTheWeekDto.setDescription(description);
        spaceOfTheWeekDto.setImageUrl(imgUrl);
        return spaceOfTheWeekDto;
    }

    public static SpaceOfWeek build(SpaceOfTheWeekDto spaceOfTheWeekDto){
        SpaceOfWeek spaceOfWeek=new SpaceOfWeek();
        spaceOfWeek.setDescription(spaceOfTheWeekDto.getDescription());
        spaceOfWeek.setImgUrl(spaceOfTheWeekDto.getImageUrl());
        spaceOfWeek.setActive(BooleanEnum.TRUE.value());
        return spaceOfWeek;
    }
}
