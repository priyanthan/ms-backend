/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.audit.dto;


import java.io.Serializable;

/**
 * The Class SmsMetaData.
 */
public class AuditMetaData implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5476366337258358234L;


	/** The subject. */
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
