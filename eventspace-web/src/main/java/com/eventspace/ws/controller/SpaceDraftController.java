package com.eventspace.ws.controller;

import com.eventspace.dto.DraftSpaceDto;
import com.eventspace.dto.ResponseDto;
import com.eventspace.dto.SpaceDetailDto;
import com.eventspace.dto.UserContext;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/api/draft")
public class SpaceDraftController {

    private static final Log LOGGER = LogFactory.getLog(SpaceDraftController.class);

    @Autowired
    private SpaceService spaceService;
    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    @GetMapping("/space")
    @ResponseStatus(HttpStatus.CREATED)
    public List<DraftSpaceDto> getDraftSpace(HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        return spaceService.getDraftSpaces(user.getUserId());
    }


    @PostMapping("/space")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDto draftSpace(@RequestBody SpaceDetailDto spaceDetailsDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("draftSpace ----->\t"+spaceDetailsDto.getName()+"\tby"+user.getEmail());
        return spaceService.draftSpace(spaceDetailsDto, user.getUserId());
    }

    @PutMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDto updateDraftSpace(@RequestBody SpaceDetailDto spaceDetailsDto,@PathVariable Integer id,HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("updateDraftSpace ----->\t"+spaceDetailsDto.getName()+"\tby"+user.getEmail());
        return spaceService.updateDraftSpace(spaceDetailsDto, user.getUserId(),id);
    }

    @DeleteMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseDto removeDraftSpace(@PathVariable Integer id,HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info("removeDraftSpace -----> by"+user.getEmail());
        return spaceService.removeDraftSpace(id, user.getUserId());
    }
}
