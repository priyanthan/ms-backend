package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "external_api_code")
public class ExternalApiCode implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;


    @Column(name = "api_code", nullable = false)
    private String apiCode;


    @Column(name = "root_url", nullable = false)
    private String rootUrl;

    @OneToMany(mappedBy = "externalApiCode")
    private Set<ExternalApiCodeHasApiDetails> externalApiCodeHasApiDetails;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApiCode() {
        return apiCode;
    }

    public void setApiCode(String apiCode) {
        this.apiCode = apiCode;
    }

    public String getRootUrl() {
        return rootUrl;
    }

    public void setRootUrl(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    public Set<ExternalApiCodeHasApiDetails> getExternalApiCodeHasApiDetails() {
        return externalApiCodeHasApiDetails;
    }

    public void setExternalApiCodeHasApiDetails(Set<ExternalApiCodeHasApiDetails> externalApiCodeHasApiDetails) {
        this.externalApiCodeHasApiDetails = externalApiCodeHasApiDetails;
    }

    @Override
    public Object build() {
        return null;
    }
}
