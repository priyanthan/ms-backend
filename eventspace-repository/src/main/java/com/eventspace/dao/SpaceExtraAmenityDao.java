package com.eventspace.dao;

import com.eventspace.domain.SpaceExtraAmenity;

import java.util.List;

/**
 * Created by Aux-052 on 12/16/2016.
 */
public interface SpaceExtraAmenityDao extends BaseDao<SpaceExtraAmenity> {
    List<SpaceExtraAmenity> getAllSpaceExtraAmenities();

    void removeExtraAmenity(Integer spaceeId, Integer amenityId);

    SpaceExtraAmenity findSpaceExtraAmenity(Integer spaceId, Integer amenityId);
}
