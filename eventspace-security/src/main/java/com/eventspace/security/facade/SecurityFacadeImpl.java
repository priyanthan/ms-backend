/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.facade;

import com.eventspace.dto.UserContext;
import com.eventspace.exception.EventspaceException;
import com.eventspace.security.EventspaceSecurityContext;
import com.eventspace.security.domain.EventspaceAuthenticationToken;
import com.eventspace.security.service.cookie.CookieManagementService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * The Class SecurityFacadeImpl.
 */
@Service
public class SecurityFacadeImpl implements SecurityFacade {

	/** The cookie management service. */
	@Autowired
	private CookieManagementService cookieManagementService;

	/** The cookie name. */
	@Value("${cookie.name}")
	private String cookieName;

	/** The cookie prifix. */
	@Value("${cookie.cookiePrifix}")
	private String cookiePrifix;

	/** The Constant APPENDER. */
	private static final String APPENDER = "APP";

	/** The log. */
	private static Logger log = Logger.getLogger(SecurityFacadeImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.auxenta.localcoins.mobilegateway.ws.security.facade.SecurityFacade
	 * #getUserContext(javax.servlet.http.HttpServletRequest)
	 */
	public UserContext getUserContext(HttpServletRequest request) throws Exception {

		EventspaceSecurityContext securityCntx = (EventspaceSecurityContext) cookieManagementService
				.retrieveCookieObject(cookieName, request);

		if (securityCntx != null && securityCntx.getAuthentication() instanceof EventspaceAuthenticationToken) {

			EventspaceAuthenticationToken authToken = (EventspaceAuthenticationToken) securityCntx.getAuthentication();

			if (authToken != null) {

				UserContext userContext = new UserContext();
				userContext.setUsername(authToken.getName());
				userContext.setEmail(authToken.getEmail());
				userContext.setUserId(authToken.getUserId());

				switch (authToken.getAuthorities().size()){
					case 1:{
						userContext.setRole("ROLE_GUEST");
						break;
					}
					case 2:{
						userContext.setRole("ROLE_USER");
						break;
					}
					case 3:{
						userContext.setRole("ROLE_FINANCE");
						break;
					}
					case 4:{
						userContext.setRole("ROLE_ADMIN");
						break;
					}
					default:
						userContext.setRole("ROLE_GUEST");

				}

				return userContext;
			} else {
				log.info("Invalid RestAuthenticationToken.");
				throw new EventspaceException("Invalid RestAuthenticationToken");
			}
		}

		EventspaceSecurityContext securityCntx_old = (EventspaceSecurityContext) cookieManagementService
				.retrieveCookieObject(cookiePrifix + APPENDER, request);

		if (securityCntx_old != null && securityCntx_old.getAuthentication() instanceof EventspaceAuthenticationToken) {

			EventspaceAuthenticationToken authToken = (EventspaceAuthenticationToken) securityCntx_old
					.getAuthentication();

			if (authToken != null) {

				UserContext userContext = new UserContext();
				userContext.setUsername(authToken.getName());
				userContext.setUserId(authToken.getUserId());
				return userContext;
			}
		}

		log.info("Invalid RestSecurityContext.");
		throw new EventspaceException("Invalid RestAuthenticationToken");

	}

	public Boolean isAuthenticatedRequest(HttpServletRequest request) {
		EventspaceSecurityContext securityCntx = (EventspaceSecurityContext) cookieManagementService
				.retrieveCookieObject(cookieName, request);

		if (securityCntx != null && securityCntx.getAuthentication() instanceof EventspaceAuthenticationToken) {
			return true;
		}
		return false;
	}

}
