package com.eventspace.dto;

public class NotificationDto {
    String type;
    int bookingId;
    String action;
    int actionTakerId;
    String actionTakerName;
    int spaceId;
    String spaceName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getActionTakerId() {
        return actionTakerId;
    }

    public void setActionTakerId(int actionTakerId) {
        this.actionTakerId = actionTakerId;
    }

    public String getActionTakerName() {
        return actionTakerName;
    }

    public void setActionTakerName(String actionTakerName) {
        this.actionTakerName = actionTakerName;
    }

    public int getSpaceId() {
        return spaceId;
    }

    public void setSpaceId(int spaceId) {
        this.spaceId = spaceId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }
}
