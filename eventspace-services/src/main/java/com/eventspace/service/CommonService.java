/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.service;

import com.eventspace.dto.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The Interface SpaceService.
 */
public interface CommonService {


    List<AmenityDto> getAllAmenities();

    List<EventTypeDto> getEventTypes();

    List<AmenityUnitsDto> getAmenityUnits();

    List<CancellationPolicyDto> getCancellationPolicies();

    List<CommonDto> getAllRules();

    List<CommonDto> getAllSeatingArrangement();

    List<AmenityDto> getLatestAmenities(Date lastUpdateDate);

    List<CommonDto> getMeasurementUnits();

    MobileSyncResponseDatadto syncDataForMobile(List<MobileSyncRequestDatadto> mobileSyncRequestDatadtos);

   String getRequestServerName(final HttpServletRequest request);

   List<CommonDto> getAllChargeType();

   List<CommonDto> getAllBlockChargeTypes();

   List<CommonDto> getAllSpaceTypes();

   Long getCurrentTime();

   Map<String, Object> getSystemInfo();

   VersionDetailsDto getAndroidVersion();

   VersionDetailsDto getIosVersion();

   Long convertEpoch(Date date);

   Map<String,Object> getAllStaticData();

    List<Map<String,Object>> procedureListResult(String procedureName);

    Map<String,Object> procedureUniqueResult(String procedureName);

    List<Map<String,Object>> tableResults(String tableName);

    Map<String,Object> tableUniqueResults(String tableName,Integer id);

    List<Map<String,Object>> queryListResult(String sqlQuery);

    Map<String,Object> queryUniqueResult(String sqlQuery);

    List<Map<String,Object>> commonProcedureListResult(String procedureName);




}
