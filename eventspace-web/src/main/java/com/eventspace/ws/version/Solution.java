package com.eventspace.ws.version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Solution {

    public static void main(String[] args) {


        int[] array1={20,35,-15,7,55,-22};
        int[] array2={20,35,-15,7,55,-22};
        int[] array3={20,35,-15,7,55,-22};
        int[] array4={20,35,-15,7,55,-22};
        int[] array5={20,35,-15,7,55,-22};
        int[] array6={20,35,-15,7,55,-22};
        int[] array7={20,35,-15,7,55,-22};
        int[] array8={20,35,-15,7,55,1,-22};
       /* bubbleSort1(array1);
        bubbleSort2(array2);
        selectionSort1(array3);
        selectionSort2(array4);
        insertionSort1(array5);
        insertionSort2(array6);
        insertionSort3(array7);
        shellSort(array8);
        getDecks();*/
       test();
    }


    public static void test(){
    int N=5;
    String word="breakfast";
    String ou="";
    for(int i=N;i<word.length();i++){
        ou=ou+String.valueOf(word.charAt(i));
    }
    for(int i=0;i<(N<word.length()?N:word.length());i++){
        ou=ou+String.valueOf(word.charAt(i)); }

        char x='A';
    System.out.println((int)word.charAt(0));
    }








    private static int getDecks (){
        String[] cards={"2", "3", "4", "5", "6","7", "8", "9", "T", "J", "Q", "K", "A"};
        String[] cardaTypes={"S","C","H","D"};

        String[] input={"2S", "2C", "2D", "2H", "3S", "3C", "3D", "3H", "4S", "4C", "4D", "4H", "5S", "5C",
                "5D", "5H", "6S", "6C", "6D", "6H", "7S", "7C", "7D", "7H", "8S", "8C", "8D", "8H", "9S", "9C",
                "9D", "9H", "TS", "TC", "TD", "TH", "JS", "JC", "JD", "JH", "QS", "QC", "QD", "QH", "KS", "KC",
                "KD", "KH", "AS", "AC", "AD", "AH", "2S", "2C", "2D", "2H", "3S", "3C", "3D", "3H", "4S", "4C",
                "4D", "4H", "5S", "5C", "5D", "5H", "6S", "6C", "6D", "6H", "7S", "7C", "7D", "7H", "8S", "8C",
                "8D", "8H", "9S", "9C", "9D", "9H", "TS", "TC", "TD", "TH", "JS", "JC", "JD", "JH", "QS", "QC",
                "QD", "QH", "KS", "KC", "KD", "KH", "AS", "AC", "AD", "AH", "2S", "2C", "2D", "2H", "3S", "3C",
                "3D", "3H", "4S", "4C", "4D", "4H", "5S", "5C", "5D", "5H", "6S", "6C", "6D", "6H", "7S", "7C",
                "7D", "7H", "8S", "8C", "8D", "8H", "9S", "9C", "9D", "9H", "TS", "TC", "TD", "TH", "JS", "JC",
                "JD", "JH", "QS", "QC", "QD", "QH", "KS", "KC", "KD", "KH", "AS", "AC", "AD"};

        List<String> inputList=Arrays.asList(input);

        if (input.length<52){
            return 0;
        }else {
            List[] arr=new List[3];
            List<String> subList=new ArrayList<>();
            for (int x = 0; x < input.length; x++) {
              if (subList.contains(input[x])){

              }else{
                  subList.add(input[x]);
              }
            }
            return 1;
        }
    }



    private static void swap(int i,int j,int[] array){
        int temp=array[i];
        array[i]=array[j];
        array[j]=temp;
    }

    private static void bubbleSort1(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> bubble sort 1");
        for(int lastUnsortedIndex=array.length-1;lastUnsortedIndex>0;lastUnsortedIndex--){
            for (int count=0;count<lastUnsortedIndex;count++){
                /** move big one to last index LTR*/
                noOfCompare++;
                if(array[count]>array[count+1]) {
                    noOfSwap++;
                    swap(count, count + 1, array);
                }
            }
            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> bubble sort 1 finished swap:"+noOfSwap +" compares" +noOfCompare);
    }


    private static void bubbleSort2(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> bubble sort 2");
        for(int lastUnsortedIndex=0;lastUnsortedIndex<array.length;lastUnsortedIndex++){
            for (int count=array.length-1;count>lastUnsortedIndex;count--){
                /** move small one to first index RTL*/
                noOfCompare++;
                if(array[count-1]>array[count]) {
                    noOfSwap++;
                    swap(count, count - 1, array);
                }
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> bubble sort 2 finished swap:"+noOfSwap+" compares" +noOfCompare);
    }


    private static void selectionSort1(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> selection sort 1");

        for(int lastUnsortedIndex=array.length-1;lastUnsortedIndex>0;lastUnsortedIndex--){
            int large=lastUnsortedIndex;
            /** move large one to last index LTR*/
            for(int count=0;count<lastUnsortedIndex;count++){
                noOfCompare++;
                if (array[count]>array[large]){
                    large=count;
                }
            }

            if (large!=lastUnsortedIndex){
                noOfSwap++;
                swap(large,lastUnsortedIndex,array);
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> selection sort 1 finished swap:"+noOfSwap+" compares" +noOfCompare);

    }


    private static void selectionSort2(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> selection sort 2");

        for(int lastUnsortedIndex=0;lastUnsortedIndex<array.length;lastUnsortedIndex++){
            int small=lastUnsortedIndex;
            /** move small one to first index RTL*/
            for(int count=array.length-1;count>lastUnsortedIndex;count--){
                noOfCompare++;
                if (array[count]<array[small]){
                    small=count;
                }
            }

            if (small!=lastUnsortedIndex){
                noOfSwap++;
                swap(small,lastUnsortedIndex,array);
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> selection sort 2 finished swap:"+noOfSwap+" compares" +noOfCompare);

    }


    private static void insertionSort1(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> insertion sort 1");

        for(int firstUnsortedIndex=1;firstUnsortedIndex<array.length;firstUnsortedIndex++){
            /** LTR*/
            for(int count=0;count<firstUnsortedIndex;count++){
                noOfCompare++;
                if (array[count]>array[firstUnsortedIndex]){
                    noOfSwap++;
                    swap(count,firstUnsortedIndex,array);
                }
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> insertion sort 1 finished swap:"+noOfSwap+" compares" +noOfCompare);

    }

    private static void insertionSort2(int[] array){
        int noOfSwap=0;
        int noOfCompare=0;
        System.out.println(">>>> insertion sort 2");

        for(int firstUnsortedIndex=array.length-2;firstUnsortedIndex>=0;firstUnsortedIndex--){
            /** LTR*/
            for(int count=array.length-1;count>firstUnsortedIndex;count--){
                noOfCompare++;
                if (array[count]<array[firstUnsortedIndex]){
                    noOfSwap++;
                    swap(count,firstUnsortedIndex,array);
                }
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }
        System.out.println(">>>> insertion sort 2 finished swap:"+noOfSwap+" compares" +noOfCompare);

    }


    private static void insertionSort3(int[] array){
        System.out.println(">>>> insertion sort 3");
        int noOfSwap=0;

        for(int firstUnsortedIndex=1;firstUnsortedIndex<array.length;firstUnsortedIndex++){
            int newElement=array[firstUnsortedIndex];
            int position;

            for (position=firstUnsortedIndex;position>0;position--){
                if (array[position-1]>newElement) {
                    noOfSwap++;
                    array[position] = array[position - 1];
                }
            }

            array[position]=newElement;

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }

        System.out.println(">>>> insertion sort 3 finished swap:"+noOfSwap);


    }


    private static void shellSort(int[] array){
        System.out.println(">>>> shell sort ");
        int noOfSwap=0;


        for(int gap=array.length/2;gap>0;gap/=2){

            for (int i=gap;i<array.length;i++){
                int newelement=array[i];
                int j=i;

                while(j >= gap && array[j-gap] > newelement){
                    array[j]=array[j-gap];
                    j-=gap;
                    noOfSwap++;
                }

                array[j]=newelement;
            }

            for (int i:array){
                System.out.print(i+" ");
            }
            System.out.println();
        }

        System.out.println(">>>> shell sort  finished swap:"+noOfSwap);

    }




    private static int interativeFactorial(int num){
        if(num==0){
            return 1;
        }
        int res=1;
        for(int i=1;i<=num;i++){
                res*=num;
        }


        return num;
    }

    private static int recursiveFactorial(int num){
        if(num==1){
            return 1;
        }
        return num*recursiveFactorial(num-1);
    }


}



