package com.eventspace.dao;

import com.eventspace.domain.SpaceOfWeek;

public interface SpaceOfWeekDao extends BaseDao<SpaceOfWeek>{

    SpaceOfWeek getSpaceOfTheWeek();
}
