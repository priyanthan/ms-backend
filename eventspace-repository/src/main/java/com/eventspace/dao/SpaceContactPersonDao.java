package com.eventspace.dao;

import com.eventspace.domain.SpaceContactPerson;

public interface SpaceContactPersonDao extends BaseDao<SpaceContactPerson> {
}
