package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Auxenta on 6/29/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AvailablityDetailsDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;

    private Integer space;

    private Integer day;

    private String menuItems;

    private String from;

    private String to;

    private Double charge;

    private Integer isReimbursable;

    private Integer isAvailable;

    private Set<MenuFileDto> menuFiles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public String getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(String menuItems) {
        this.menuItems = menuItems;
    }

    public Set<MenuFileDto> getMenuFiles() {
        return menuFiles;
    }

    public void setMenuFiles(Set<MenuFileDto> menuFiles) {
        this.menuFiles = menuFiles;
    }

    public Integer getIsReimbursable() {
        return isReimbursable;
    }

    public void setIsReimbursable(Integer isReimbursable) {
        this.isReimbursable = isReimbursable;
    }

    public Integer getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Integer isAvailable) {
        this.isAvailable = isAvailable;
    }
}
