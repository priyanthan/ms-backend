package com.eventspace.component;

import com.eventspace.dao.BookingDao;
import com.eventspace.dao.BookingExpireDetailsDao;
import com.eventspace.domain.BookingExpireDetails;
import com.eventspace.service.BookingService;
import com.eventspace.service.SchedulerService;
import com.eventspace.service.SpaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class Startup {

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private BookingExpireDetailsDao bookingExpireDetailsDao;

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private BookingService bookingService;

    @Autowired
    private SpaceService spaceService;

    private boolean appStarted = false;

    @Value("${daily.scheduler.enable}")
    private Boolean dailySchedulerEnable;

    @Value("${million.spaces.url}")
    private String serverUrl;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(Startup.class);

    @EventListener(ContextRefreshedEvent.class)
    @Transactional
    public void contextRefreshedEvent(ContextRefreshedEvent event) {
        LOGGER.info(serverUrl);
        //LOGGER.info("-------------------------->"+event.getApplicationContext().getParent());
        LOGGER.info("-------------------------->"+event.getApplicationContext().getDisplayName());
        if (!appStarted && event.getApplicationContext().getParent()!=null) {
            LOGGER.info("app starting >>>>>>>>>>>> {}",appStarted);

            //expire the pending payment bookings
            if(dailySchedulerEnable) {
                List<BookingExpireDetails> bookingExpireDetails = bookingExpireDetailsDao.getUpcomingExpireBookings();
                for (BookingExpireDetails bookingExpireDetail : bookingExpireDetails) {
                    schedulerService.expireBooking(bookingExpireDetail.getBooking());
                }

                /*List<Booking> bookings = bookingDao.missedDiscardBookings();
                for (Booking booking : bookings) {
                    schedulerService.discardBooking(booking.getId());
                }*/
            }
            //spaceService.disapproveCalenderEndSpaces();
            appStarted=true;
        }
    }
}