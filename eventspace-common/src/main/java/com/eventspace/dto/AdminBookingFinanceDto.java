package com.eventspace.dto;

import java.io.Serializable;

public class AdminBookingFinanceDto implements Serializable {


    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;
    private String orderId;
    private Long eventDate;
    private String spaceName;
    private String organizationName;

    private Integer commissionPercentage;
    private Double hostCharge;
    private Double commission;
    private String accountHolderName;
    private String accountNumber;
    private String bank;
    private String branch;
    private Integer paymentVerified;
    private String pdf;
    private String notes;

    private String guestName;
    private Long bookingMade;
    private String cancellationPolicy;

    private Double booking;
    private Double advance;
    private Double discount;
    private Boolean isAdvance;
    private String paymentMethod;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getEventDate() {
        return eventDate;
    }

    public void setEventDate(Long eventDate) {
        this.eventDate = eventDate;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public Double getHostCharge() {
        return hostCharge;
    }

    public void setHostCharge(Double hostCharge) {
        this.hostCharge = hostCharge;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Integer getPaymentVerified() {
        return paymentVerified;
    }

    public void setPaymentVerified(Integer paymentVerified) {
        this.paymentVerified = paymentVerified;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }

    public Long getBookingMade() {
        return bookingMade;
    }

    public void setBookingMade(Long bookingMade) {
        this.bookingMade = bookingMade;
    }

    public String getCancellationPolicy() {
        return cancellationPolicy;
    }

    public void setCancellationPolicy(String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public Double getBooking() {
        return booking;
    }

    public void setBooking(Double booking) {
        this.booking = booking;
    }


    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Boolean getIsAdvance() {
        return isAdvance;
    }

    public void setIsAdvance(Boolean isAdvance) {
        isAdvance = isAdvance;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }


}