package com.eventspace.eventaction.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Auxenta on 2/13/2017.
 */
@Component
public class MailCriteriaComponents {

    @Value("${user.verification.template}")
    private String userEmailVerificationTemplateFIle;

    @Value("${welcome.email.template}")
    private String userWelcomeTemplateFIle;

    @Value("${passwordReset.email.template}")
    private String userPasswordResetTemplate;

    @Value("${manualBookingGuest.email.template}")
    private String manualBookingGuestTemplateFile;

    @Value("${manualBookingHost.email.template}")
    private String manualBookingHostTemplateFile;

    /**
     * The booking template file.
     */
    @Value("${bookingPaymentHost.email.template}")
    private String bookingConfirmHostTemplateFile;

    @Value("${bookingPaymentHost.advanced.email.template}")
    private String advancedBookingConfirmHostTemplateFile;

    @Value("${bookingPaymentHost.advanced.pending.email.template}")
    private String advancedBookingPendingPaymentHostTemplateFile;

    /**
     * The booking template file.
     */
    @Value("${bookingPaymentGuest.email.template}")
    private String bookingConfirmGuestTemplateFile;

    @Value("${bookingPaymentGuest.advanced.email.template}")
    private String advancedBookingConfirmGuestTemplateFile;

    @Value("${corporateBookingPaymentGuest.email.template}")
    private String corporateBookingConfirmGuestTemplateFile;

    @Value("${bookingPaymentGuest.advanced.pending.email.template}")
    private String advancedBookingPendingPaymentGuestTemplateFile;



    /**
     * The booking template file.
     */
    @Value("${bookingCancelHost.email.template}")
    private String bookingCancelHostTemplateFile;

    /**
     * The booking template file.
     */
    @Value("${bookingCancelGuest.email.template}")
    private String bookingCancelGuestTemplateFile;

    @Value("${bookingExpireGuest.email.template}")
    private String bookingExpireGuestEmailTemplateFile;

    @Value("${bookingExpireHost.email.template}")
    private String bookingExpireHostEmailTemplateFile;

    @Value("${requestReview.email.template}")
    private String bookingRequestReviewEmailTemplate;

    @Value("${viewReview.email.template}")
    private String viewReviewEmailTemplate;

    @Value("${update.calender.endDate.template}")
    private String updateCalenderEndDateTemplate;

    @Value("${space.deactivated.template}")
    private String spaceDeactivatedeTemplate;

    @Value("${spacePublish.email.template}")
    private String spacePublishEmailTemplate;

    @Value("${bookingPromotionGuest.email.template}")
    private String bookingPromotionGuestEmailTemplate;

    @Value("${bookingNoPromotionGuest.email.template}")
    private String bookingNoPromotionGuestEmailTemplate;

    @Value("${bookingPromotionHost.email.template}")
    private String bookingPromotionHostEmailTemplate;

    @Value("${bookingNoPromotionHost.email.template}")
    private String bookingNoPromotionHostEmailTemplate;

    @Value("${bookingPayLater.email.template}")
    private String bookingPayLaterEmailTemplate;

    @Value("${bookingPayLaterReminder.email.template}")
    private String bookingPayLaterReminderEmailTemplate;

    @Value("${bookingPayAtCourtHost.email.template}")
    private String bookingPayAtCourtHostEmailTemplate;

    @Value("${bookingPayAtCourtGuest.email.template}")
    private String bookingPayAtCourtGuestEmailTemplate;



    @Value("${user.verification.subject}")
    private String userEmailVerificationEmailSubject;

    @Value("${welcome.email.subject}")
    private String welcomeUserEmailSubject;

    @Value("${passwordReset.email.subject}")
    private String userPasswordResetSubject;

    @Value("${manualBookingGuest.email.subject}")
    private String manualBookingGuestEmailSubject;

    @Value("${manualBookingHost.email.subject}")
    private String manualBookingHostEmailSubject;

    /**
     * The booking email subject.
     */
    @Value("${bookingPaymentHost.email.subject}")
    private String bookingConfirmHostEmailSubject;

    /**
     * The booking email subject.
     */
    @Value("${bookingPaymentGuest.email.subject}")
    private String bookingConfirmGuestEmailSubject;

    /**
     * The booking email subject.
     */
    @Value("${bookingCancelHost.email.subject}")
    private String bookingCancelHostEmailSubject;

    /**
     * The booking email subject.
     */
    @Value("${bookingCancelGuest.email.subject}")
    private String bookingCancelGuestEmailSubject;

    @Value("${bookingExpireGuest.email.subject}")
    private String bookingExpireGuestEmailSubject;

    @Value("${bookingExpireHost.email.subject}")
    private String bookingExpireHostEmailSubject;

    @Value("${requestReview.email.subject}")
    private String bookingRequestReviewEmailSubject;

    @Value("${viewReview.email.subject}")
    private String viewReviewEmailSubject;

    @Value("${update.calender.endDate.subject}")
    private String updateCalenderEndDateSubject;

    @Value("${space.deactivated.subject}")
    private String spaceDeactivatedeSubject;

    @Value("${spacePublish.email.subject}")
    private String spacePublishEmailSubject;

    @Value("${bookingPromotionHost.email.subject}")
    private String bookingPromotionHostEmailSubject;

    @Value("${bookingNoPromotionHost.email.subject}")
    private String bookingNoPromotionHostEmailSubject;

    @Value("${bookingPromotionGuest.email.subject}")
    private String bookingPromotionGuestEmailSubject;

    @Value("${bookingNoPromotionGuest.email.subject}")
    private String bookingNoPromotionGuestEmailSubject;

    @Value("${bookingPayLater.email.subject}")
    private String bookingPayLaterEmailSubject;

    @Value("${bookingPayLaterReminder.email.subject}")
    private String bookingPayLaterReminderEmailSubject;

    @Value("${bookingPayAtCourtHost.email.subject}")
    private String bookingPayAtCourtHostEmailSubject;

    @Value("${bookingPayAtCourtGuest.email.subject}")
    private String bookingPayAtCourtGuestEmailSubject;



    public String getUserPasswordResetSubject() {
        return userPasswordResetSubject;
    }

    public void setUserPasswordResetSubject(String userPasswordResetSubject) {
        this.userPasswordResetSubject = userPasswordResetSubject;
    }

    public String getUserPasswordResetTemplate() {
        return userPasswordResetTemplate;
    }

    public void setUserPasswordResetTemplate(String userPasswordResetTemplate) {
        this.userPasswordResetTemplate = userPasswordResetTemplate;
    }



    public EmailCriteriaDto buildMailCriteria() {
        EmailCriteriaDto emailCriteriaDto = new EmailCriteriaDto();
        emailCriteriaDto.setUserEmailVerificationEmailSubject(userEmailVerificationEmailSubject);
        emailCriteriaDto.setUserEmailVerificationTemplateFIle(userEmailVerificationTemplateFIle);
        emailCriteriaDto.setUserWelcomeEmailSubject(welcomeUserEmailSubject);
        emailCriteriaDto.setUserWelcomeTemplateFile(userWelcomeTemplateFIle);
        emailCriteriaDto.setUserPasswordResetTemplateFile(userPasswordResetTemplate);
        emailCriteriaDto.setUserPasswordResetSubject(userPasswordResetSubject);

        emailCriteriaDto
                .setBookingPaymentGuestEmailSubject(bookingConfirmGuestEmailSubject);
        emailCriteriaDto
                .setBookingConfirmGuestTemplateFile(bookingConfirmGuestTemplateFile);
        emailCriteriaDto
                .setBookingPaymentHostEmailSubject(bookingConfirmHostEmailSubject);
        emailCriteriaDto
                .setBookingConfirmHostTemplateFile(bookingConfirmHostTemplateFile);
        emailCriteriaDto
                .setBookingCancelGuestEmailSubject(bookingCancelGuestEmailSubject);
        emailCriteriaDto
                .setBookingCancelGuestTemplateFile(bookingCancelGuestTemplateFile);
        emailCriteriaDto
                .setBookingCancelHostEmailSubject(bookingCancelHostEmailSubject);
        emailCriteriaDto
                .setBookingCancelHostTemplateFile(bookingCancelHostTemplateFile);
        emailCriteriaDto.setBookingExpireGuestEmailSubject(bookingExpireGuestEmailSubject);
        emailCriteriaDto.setBookingExpireGuestTemplateFile(bookingExpireGuestEmailTemplateFile);
        emailCriteriaDto.setBookingExpireHostEmailSubject(bookingExpireHostEmailSubject);
        emailCriteriaDto.setBookingExpireHostTemplateFile(bookingExpireHostEmailTemplateFile);
        emailCriteriaDto.setBookingRequestReviewEmailSubject(bookingRequestReviewEmailSubject);
        emailCriteriaDto.setBookingRequestReviewEmailTemplateFile(bookingRequestReviewEmailTemplate);
        emailCriteriaDto.setViewReviewEmailSubject(viewReviewEmailSubject);
        emailCriteriaDto.setViewReviewEmailTemplateFile(viewReviewEmailTemplate);
        emailCriteriaDto.setManualBookingGuestEmailSubject(manualBookingGuestEmailSubject);
        emailCriteriaDto.setManualBookingGuestTemplateFile(manualBookingGuestTemplateFile);
        emailCriteriaDto.setManualBookingHostEmailSubject(manualBookingHostEmailSubject);
        emailCriteriaDto.setManualBookingHostTemplateFile(manualBookingHostTemplateFile);
        emailCriteriaDto.setUpdateCalenderEndDateSubject(updateCalenderEndDateSubject);
        emailCriteriaDto.setUpdateCalenderEndDateTemplate(updateCalenderEndDateTemplate);
        emailCriteriaDto.setSpaceDeactivatedSubject(spaceDeactivatedeSubject);
        emailCriteriaDto.setSpaceDeactivatedTemplate(spaceDeactivatedeTemplate);
        emailCriteriaDto.setSpacePublishEmailSubject(spacePublishEmailSubject);
        emailCriteriaDto.setSpacePublishEmailTemplate(spacePublishEmailTemplate);
        emailCriteriaDto.setBookingPromotionGuestEmailSubject(bookingPromotionGuestEmailSubject);
        emailCriteriaDto.setBookingPromotionGuestEmailTemplate(bookingPromotionGuestEmailTemplate);
        emailCriteriaDto.setBookingPromotionHostEmailSubject(bookingPromotionHostEmailSubject);
        emailCriteriaDto.setBookingPromotionHostEmailTemplate(bookingPromotionHostEmailTemplate);
        emailCriteriaDto.setBookingNoPromotionGuestEmailSubject(bookingNoPromotionGuestEmailSubject);
        emailCriteriaDto.setBookingNoPromotionGuestEmailTemplate(bookingNoPromotionGuestEmailTemplate);
        emailCriteriaDto.setBookingNoPromotionHostEmailSubject(bookingNoPromotionHostEmailSubject);
        emailCriteriaDto.setBookingNoPromotionHostEmailTemplate(bookingNoPromotionHostEmailTemplate);
        emailCriteriaDto.setBookNowPayLaterGuestEmailSubject(bookingCancelGuestEmailSubject);
        emailCriteriaDto.setBookNowPayLaterGuestEmailTemplate(bookingPayLaterEmailTemplate);
        emailCriteriaDto.setBookNowPayLaterReminderGuestEmailSubject(bookingPayLaterEmailSubject);
        emailCriteriaDto.setBookNowPayLaterReminderGuestEmailTemplate(bookingPayLaterReminderEmailTemplate);
        emailCriteriaDto.setCorporateBookingConfirmGuestEmailTemplate(corporateBookingConfirmGuestTemplateFile);
        emailCriteriaDto.setAdvancedBookingConfirmGuestTemplateFile(advancedBookingConfirmGuestTemplateFile);
        emailCriteriaDto.setAdvancedBookingConfirmHostTemplateFile(advancedBookingConfirmHostTemplateFile);
        emailCriteriaDto.setBookingPayAtCourtGuestEmailSubject(bookingPayAtCourtGuestEmailSubject);
        emailCriteriaDto.setBookingPayAtCourtGuestEmailTemplate(bookingPayAtCourtGuestEmailTemplate);
        emailCriteriaDto.setBookingPayAtCourtHostEmailSubject(bookingPayAtCourtHostEmailSubject);
        emailCriteriaDto.setBookingPayAtCourtHostEmailTemplate(bookingPayAtCourtHostEmailTemplate);
        emailCriteriaDto.setAdvancedBookingPendingGuestTemplateFile(advancedBookingPendingPaymentGuestTemplateFile);
        emailCriteriaDto.setAdvancedBookingPendingHostTemplateFile(advancedBookingPendingPaymentHostTemplateFile);
        return emailCriteriaDto;
    }


}
