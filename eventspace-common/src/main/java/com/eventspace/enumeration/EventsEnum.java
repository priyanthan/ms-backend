package com.eventspace.enumeration;

import java.util.HashMap;

/**
 * Created by Aux-052 on 1/6/2017.
 */
public enum EventsEnum {
    PAY(1), UNDO_PAYMENT(2), CANCEL(3), CONFIRM(4), EXPIRE(5),DISCARD(6),WAIT_FOR_PAY(7),WAIT_FOR_PAY_ADVANCE(8),PAY_ADVANCE(9);

    private static final HashMap<Integer, EventsEnum> typeValueMap = new HashMap<>();

    static {
        for (EventsEnum type : EventsEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    EventsEnum(final int value) {
        this.value = value;
    }

    public static EventsEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
