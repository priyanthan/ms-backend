/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.sms.sender;

import com.eventspace.sms.dto.MessageMetaData;
import com.eventspace.sms.publisher.MessageEventPublisher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageSender {

	/** The Constant logger. */
	private static final Log LOGGER = LogFactory.getLog(MessageSender.class);

	/** The Constant ENCODE_TYPE. */
	protected static final String ENCODE_TYPE = "UTF-8";


	/** The sender email. */
	@Value("${sms.api}")
	private String apiCode;


	@Autowired
	MessageEventPublisher eventPublisher;


	/**
	 * Send email.
	 *
	 * @param metaData
	 *            the meta data
	 */
	public void sendSms(final MessageMetaData metaData) {
		try {

			LOGGER.info(String.format("sendSms -> get called with [%s]", metaData));

			RestTemplate restTemplate = new RestTemplate();
			List<MediaType> acceptableMediaTypes = new ArrayList<>();
			acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(acceptableMediaTypes);
			HttpEntity entity = new HttpEntity(headers);
			String url = "https://cpsolutions.dialog.lk/index.php/cbs/sms/send?destination="+metaData.getPhoneNumber()+"&message="+metaData.getMessage()+"&q="+apiCode;
			HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity,String.class);
			LOGGER.info("sms gateway response "+response.getBody());

		} catch (Exception ex) {
			LOGGER.error("Exception occured." + ex);
		}
	}
	

}
