package com.eventspace.domain;

import com.eventspace.dto.PromoDetailDto;
import com.eventspace.enumeration.BooleanEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "promo_details",uniqueConstraints = {@UniqueConstraint(columnNames = "promo_code")})
public class PromoDetails implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "promo_code", nullable = false, unique = true)
    private String promoCode;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "promo_details_has_space", joinColumns = {
            @JoinColumn(name = "promo_details", nullable = false, updatable = false)}, inverseJoinColumns = {
            @JoinColumn(name = "space", nullable = false, updatable = false)})
    private Set<Space> spaces = new HashSet<>(0);

    @Column(name = "to_all_spaces")
    private Integer toAllSpaces;

    @Column(name = "discount", nullable = false)
    private Integer discount;

    @Column(name = "is_discount_flat", nullable = false)
    private Integer isFlat;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "total_count")
    private Integer totalCount;

    @Column(name = "user_count")
    private Integer userCount;

    @Column(name = "is_time_block")
    private Integer isTimeBlock;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private CorporatePromo corporatePromo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }


    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Set<Space> getSpaces() {
        return spaces;
    }

    public void setSpaces(Set<Space> spaces) {
        this.spaces = spaces;
    }

    public Integer getToAllSpaces() {
        return toAllSpaces;
    }

    public void setToAllSpaces(Integer toAllSpaces) {
        this.toAllSpaces = toAllSpaces;
    }

    public Integer getIsFlat() {
        return isFlat;
    }

    public void setIsFlat(Integer isFlat) {
        this.isFlat = isFlat;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getIsTimeBlock() {
        return isTimeBlock;
    }

    public void setIsTimeBlock(Integer isTimeBlock) {
        this.isTimeBlock = isTimeBlock;
    }

    public CorporatePromo getCorporatePromo() {
        return corporatePromo;
    }

    public void setCorporatePromo(CorporatePromo corporatePromo) {
        this.corporatePromo = corporatePromo;
    }

    @Override
    public PromoDetailDto build() {
        PromoDetailDto promoDetailDto=new PromoDetailDto();
        promoDetailDto.setId(id);
        Set<Integer> promoSpaces=new HashSet<>();
        for (Space space:spaces){
            promoSpaces.add(space.getId());
        }
        promoDetailDto.setSpaces(promoSpaces);
        promoDetailDto.setToAllSpace(toAllSpaces);
        promoDetailDto.setPromoCode(promoCode);
        promoDetailDto.setDiscount(discount);
        promoDetailDto.setIsFlatDiscount(isFlat);
        promoDetailDto.setStartDate(startDate.getTime());
        promoDetailDto.setEndDate(endDate.getTime());
        promoDetailDto.setCreateDate(createDate.getTime());
        promoDetailDto.setUserCount(userCount);
        promoDetailDto.setTotalCount(totalCount);
        return promoDetailDto;
    }

    public static PromoDetails build(PromoDetailDto promoDetailDto){
        PromoDetails promoDetails=new PromoDetails();
        promoDetails.setPromoCode(promoDetailDto.getPromoCode());
        if (promoDetailDto.getSpaces()==null && promoDetailDto.getToAllSpace().equals(BooleanEnum.TRUE.value())){
            promoDetails.setToAllSpaces(BooleanEnum.TRUE.value());
        }
        else {
            promoDetails.setToAllSpaces(BooleanEnum.FALSE.value());
        }
        promoDetails.setDiscount(promoDetailDto.getDiscount());
        promoDetails.setIsFlat(promoDetailDto.getIsFlatDiscount());
        promoDetails.setStartDate(new Date(promoDetailDto.getStartDate()));
        promoDetails.setEndDate(new Date(promoDetailDto.getEndDate()));
        promoDetails.setCreateDate(new Date());
        promoDetails.setTotalCount(promoDetailDto.getTotalCount());
        promoDetails.setUserCount(promoDetailDto.getUserCount());
        return promoDetails;
    }



}
