package com.eventspace.eventaction.impl;

import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.EventActionServices;
import com.eventspace.eventaction.dto.BookingContextDto;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.StateContext;

/**
 * Created by Auxenta on 6/9/2017.
 */
public class PaymentUndoEventActionServiceImpl extends EventActionServices {

    private static final Log logger = LogFactory.getLog(PaymentUndoEventActionServiceImpl.class);
    /**
     * @param bookingContextDto
     */
    public PaymentUndoEventActionServiceImpl(BookingContextDto bookingContextDto) {
        super(bookingContextDto);
    }

    /**
     * @param stateContext
     */
    @Override
    public void execute(StateContext<StatesEnum, EventsEnum> stateContext) {
        logger.info(String.format("BOOKING action PAYMENT UNDO OrderID : %s",booking.getOrderId()));
        updateBookingDetails(bookingActionDto,StatesEnum.PENDING_PAYMENT.value());
        emailService.sendBookingActionMails(booking.getId(),EventsEnum.UNDO_PAYMENT.value());
        bookingService.removeBookingFromSpaceUnavailablity(booking.getId());
    }
}
