package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "schedule_start")
public class ScheduleStart implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * The id.
     */
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The name.
     */
    @Column(name = "is_started", nullable = false)
    private Integer isStarted;

    @Version
    @Column(name = "date_updated", nullable = false)
    private Date LastStarted;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsStarted() {
        return isStarted;
    }

    public void setIsStarted(Integer isStarted) {
        this.isStarted = isStarted;
    }

    public Date getLastStarted() {
        return LastStarted;
    }

    public void setLastStarted(Date lastStarted) {
        LastStarted = lastStarted;
    }

    @Override
    public Object build() {
        return null;
    }
}
