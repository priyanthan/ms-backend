/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.domain;

import com.eventspace.dto.ReviewDetailsDto;
import com.eventspace.dto.ReviewDto;

import javax.persistence.*;
import javax.swing.text.html.Option;
import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

import static com.eventspace.domain.Reviews.AVG_RATE_SPACE_SP;


/**
 * The Class Reviews.
 */
@NamedNativeQueries({
		@NamedNativeQuery(
				name = AVG_RATE_SPACE_SP,
				query = "CALL averageRating(:spaceId)"
		),
})
@Entity
@Table(name = "reviews")
public class Reviews implements Serializable, BaseDomain {

	public static final String AVG_RATE_SPACE_SP = "callDAverageSpaceStoreProcedure";

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/** The rate. */
	@Column(name="rate", nullable = false)
	private String rate;

	/** The description. */
	@Column(name="title")
	private String title;

	/** The description. */
	@Column(name="description")
	private String description;

	/** The description. */
	@Column(name="ms_experience")
	private String msExperience;
	
	/** The created at. */
	@Column(name="created_at", nullable=false)
	private Date createdAt;
	
	/** The booking id. */
	@Column(name="booking_id", nullable= false)
	private Integer bookingId;
	
	/**
	 * Gets the rate.
	 *
	 * @return the rate
	 */
	public String getRate() {
		return rate;
	}

	/**
	 * Sets the rate.
	 *
	 * @param rate the new rate
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the created at.
	 *
	 * @return the created at
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * Sets the created at.
	 *
	 * @param createdAt the new created at
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * The Constructor.
	 *
	 * @param id
	 *            the id
	 */
	public Reviews(int id) {
		this.id = id;
	}

	/**
	 * The Constructor.
	 */
	public Reviews() {
	}


	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	
	/**
	 * Gets the booking id.
	 *
	 * @return the booking id
	 */
	public Integer getBookingId() {
		return bookingId;
	}

	/**
	 * Sets the booking id.
	 *
	 * @param bookingId the new booking id
	 */
	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() { return title; }

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) { this.title = title; }

	public String getMsExperience() {
		return msExperience;
	}

	public void setMsExperience(String msExperience) {
		this.msExperience = msExperience;
	}

	/* (non-Javadoc)
             * @see com.eventspace.domain.BaseDomain#build(java.lang.Integer)
             */
	@Override
	public ReviewDetailsDto build() {
		ReviewDetailsDto reviewDetails=new ReviewDetailsDto();
		reviewDetails.setTitle(title);
		reviewDetails.setDescription(description);
		reviewDetails.setMsExperience(msExperience);
		reviewDetails.setRate(rate);
		reviewDetails.setCreatedAt(createdAt.toString());
		return reviewDetails;
	}

	public ReviewDetailsDto buildMsReview(){
		ReviewDetailsDto reviewDetails=new ReviewDetailsDto();
		reviewDetails.setTitle(title);
		reviewDetails.setMsExperience(msExperience);
		reviewDetails.setCreatedAt(createdAt.toString());

		Optional.ofNullable(rate).ifPresent(rate1->{
			reviewDetails.setRate(""+rate.charAt(rate.length()-1));
		});

		return reviewDetails;
	}
	public static Reviews build(ReviewDto reviewDto){
		Reviews reviews = new Reviews();
		reviews.setId(reviewDto.getId());
		reviews.setTitle(reviewDto.getTitle());
		reviews.setBookingId(reviewDto.getBookingId());
		reviews.setDescription(reviewDto.getDescription());
		reviews.setMsExperience(reviewDto.getMsExperience());
		reviews.setRate(reviewDto.getRate());
		reviews.setCreatedAt(reviewDto.getCreatedAt());
		return reviews;
	}


}
