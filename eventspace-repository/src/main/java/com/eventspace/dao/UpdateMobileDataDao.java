/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.dao;

import com.eventspace.domain.BaseDomain;

import java.util.List;

/**
 * The Interface UpdateMobileDataDao.
 */
public interface UpdateMobileDataDao {

    /**
     * Gets the updated table details.
     *
     * @param <T>       the generic type
     * @param type      the type
     * @param timestamp the timestamp
     * @return the updated table details
     */
    <T> List<T> getUpdatedTableDetails(T type, Long timestamp);

    /**
     * Gets the updated table details.
     *
     * @param <T>          the generic type
     * @param type         the type
     * @param timestamp    the timestamp
     * @param enterpriseId the enterprise id
     * @return the updated table details
     */
    <T> List<T> getUpdatedTableDetails(T type, Long timestamp, Integer enterpriseId);

    /**
     * Save.
     *
     * @param domain the job
     */
    void save(BaseDomain domain);

}
