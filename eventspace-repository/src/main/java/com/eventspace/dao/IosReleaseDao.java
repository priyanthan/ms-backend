package com.eventspace.dao;

import com.eventspace.domain.IosRelease;

public interface IosReleaseDao extends BaseDao<IosRelease>  {

    IosRelease getLatestRelease();
}
