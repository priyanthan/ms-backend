package com.eventspace.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Auxenta on 7/5/2017.
 */
@Entity
@Table(name = "user_additional_details")
public class UserAdditionalDetails implements Serializable, BaseDomain {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "user", nullable = false, unique = true)
    private Integer user;

    /** The name. */
    @Column(name = "last_name")
    private String lastName;

    /** The name. */
    @Column(name = "address")
    private String address;

    /** The name. */
    @Column(name = "job")
    private String job;

    /** The name. */
    @Column(name = "company_name")
    private String companyName;


    /** The name. */
    @Column(name = "about")
    private String about;

    /** The name. */
    @Column(name = "account_holder_name")
    private String accountHolderName;

    /** The name. */
    @Column(name = "account_number")
    private String accountNumber;

    /** The name. */
    @Column(name = "bank")
    private String bank;

    /** The name. */
    @Column(name = "bank_branch")
    private String bankBranch;

    /** The name. */
    @Column(name = "commission_percentage")
    private Integer commissionPercentage;



    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    @Override
    public Object build() {
        return null;
    }
}
