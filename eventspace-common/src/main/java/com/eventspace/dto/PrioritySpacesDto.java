package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrioritySpacesDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private List<PrioritySpaceDto> spaces;

    public List<PrioritySpaceDto> getSpaces() {
        return spaces;
    }

    public void setSpaces(List<PrioritySpaceDto> spaces) {
        this.spaces = spaces;
    }
}
