package com.eventspace.dto;

import java.io.Serializable;
import java.util.Date;

public class BookingReportDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private String reffId;
    private Date eventDate;
    private String spaceName;
    private String organizationName;
    private int bookingValueBeforeDiscount;
    private int msContributedDiscount;
    private int hostContributedDiscount;
    private int cashFlowAfterDiscount;
    private Integer commissionPercentage;
    private int hostPayment;
    private String commissionRevenue;
    private String promoCode;
    private Date bookingReceivedDate;
    private Boolean reviewReceived;
    private int cumulativeCashInFlow;
    private int cumulativeMsContributedDiscount;
    private int cumulativeHostContributedDiscount;
    private int netCommissionRevenue;
    private String paymentMode;
    private String guestMobile;
    private String guestEmail;
    private String eventType;
    private Long paymentDueDate;

    public String getReffId() {
        return reffId;
    }

    public void setReffId(String reffId) {
        this.reffId = reffId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public int getBookingValueBeforeDiscount() {
        return bookingValueBeforeDiscount;
    }

    public void setBookingValueBeforeDiscount(int bookingValueBeforeDiscount) {
        this.bookingValueBeforeDiscount = bookingValueBeforeDiscount;
    }

    public int getMsContributedDiscount() {
        return msContributedDiscount;
    }

    public void setMsContributedDiscount(int msContributedDiscount) {
        this.msContributedDiscount = msContributedDiscount;
    }

    public int getHostContributedDiscount() {
        return hostContributedDiscount;
    }

    public void setHostContributedDiscount(int hostContributedDiscount) {
        this.hostContributedDiscount = hostContributedDiscount;
    }

    public int getCashFlowAfterDiscount() {
        return cashFlowAfterDiscount;
    }

    public void setCashFlowAfterDiscount(int cashFlowAfterDiscount) {
        this.cashFlowAfterDiscount = cashFlowAfterDiscount;
    }

    public Integer getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(Integer commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public int getHostPayment() {
        return hostPayment;
    }

    public void setHostPayment(int hostPayment) {
        this.hostPayment = hostPayment;
    }

    public String getCommissionRevenue() {
        return commissionRevenue;
    }

    public void setCommissionRevenue(String commissionRevenue) {
        this.commissionRevenue = commissionRevenue;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getBookingReceivedDate() {
        return bookingReceivedDate;
    }

    public void setBookingReceivedDate(Date bookingReceivedDate) {
        this.bookingReceivedDate = bookingReceivedDate;
    }

    public Boolean getReviewReceived() {
        return reviewReceived;
    }

    public void setReviewReceived(Boolean reviewReceived) {
        this.reviewReceived = reviewReceived;
    }

    public int getCumulativeCashInFlow() {
        return cumulativeCashInFlow;
    }

    public void setCumulativeCashInFlow(int cumulativeCashInFlow) {
        this.cumulativeCashInFlow = cumulativeCashInFlow;
    }

    public int getCumulativeMsContributedDiscount() {
        return cumulativeMsContributedDiscount;
    }

    public void setCumulativeMsContributedDiscount(int cumulativeMsContributedDiscount) {
        this.cumulativeMsContributedDiscount = cumulativeMsContributedDiscount;
    }

    public int getCumulativeHostContributedDiscount() {
        return cumulativeHostContributedDiscount;
    }

    public void setCumulativeHostContributedDiscount(int cumulativeHostContributedDiscount) {
        this.cumulativeHostContributedDiscount = cumulativeHostContributedDiscount;
    }

    public int getNetCommissionRevenue() {
        return netCommissionRevenue;
    }

    public void setNetCommissionRevenue(int netCommissionRevenue) {
        this.netCommissionRevenue = netCommissionRevenue;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(String guestMobile) {
        this.guestMobile = guestMobile;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public void setGuestEmail(String guestEmail) {
        this.guestEmail = guestEmail;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Long getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(Long paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }
}
