package com.eventspace.eventaction;

import com.eventspace.dao.*;
import com.eventspace.domain.Booking;
import com.eventspace.domain.BookingSpaceExtraAmenity;
import com.eventspace.domain.SpaceUnavailability;
import com.eventspace.domain.User;
import com.eventspace.dto.BookingActionDto;
import com.eventspace.dto.BookingDto;
import com.eventspace.dto.BookingWithExtraAmenityDto;
import com.eventspace.email.publisher.EventPublisher;
import com.eventspace.email.sender.EmailSender;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.enumeration.EventsEnum;
import com.eventspace.enumeration.StatesEnum;
import com.eventspace.eventaction.dto.BookingContextDto;
import com.eventspace.service.BookingService;
import com.eventspace.service.EmailService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.statemachine.action.Action;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Event space Created by Aux-054 on 1/9/2017.
 */

public abstract class EventActionServices implements
        Action<StatesEnum, EventsEnum> {

    private static final Log logger = LogFactory
            .getLog(EventActionServices.class);

    protected BookingDao bookingDao;

    protected UserDao userDao;

    protected SpaceDao spaceDao;

    protected AmenityDao amenityDao;

    protected EmailSender emailSender;

    protected Booking booking;

    protected BookingActionDto bookingActionDto;

    protected BookingSpaceExtraAmenityDao bookingSpaceExtraAmenityDao;

    protected EventPublisher eventPublisher;

    protected BookingService bookingService;

    protected EmailService emailService;

    public EventActionServices(BookingContextDto bookingContextDto) {
        this.bookingDao = bookingContextDto.getBookingDao();
        this.userDao = bookingContextDto.getUserDao();
        this.spaceDao = bookingContextDto.getSpaceDao();
        this.amenityDao = bookingContextDto.getAmenityDao();
        this.booking = bookingContextDto.getBooking();
        this.bookingActionDto = bookingContextDto.getBookingActionDto();
        this.emailSender = bookingContextDto.getEmailSender();
        this.bookingSpaceExtraAmenityDao = bookingContextDto.getBookingSpaceExtraAmenityDao();
        this.eventPublisher = bookingContextDto.getEventPublisher();
        this.bookingService = bookingContextDto.getBookingService();
        this.emailService=bookingContextDto.getEmailService();
    }

    /**
     * @param bookingActionDto
     */
    public void updateBookingDetails(BookingActionDto bookingActionDto,Integer reservationStatus) {
        logger.info("updateBookingDetails method -----> get call");
        booking.setReservationStatus(reservationStatus);
        if(bookingActionDto.getUserId()!=null)
            booking.setActionTaker(bookingActionDto.getUserId());
        if (reservationStatus.equals(StatesEnum.CANCELLED.value()))
            booking.setRefund(bookingActionDto.getRefund());
        bookingDao.update(booking);
    }

    /**
     * @param
     * @return
     */
    /*public Integer setReservationStatus(BookingActionDto bookingActionDto) {
        Integer endState = 1;
        if (bookingActionDto.getEvent() == 1) {
            endState = 2;
        } else if (bookingActionDto.getEvent() == 2) {
            endState = 3;
        } else if (bookingActionDto.getEvent() == 3
                || bookingActionDto.getEvent() == 4) {
            endState = 4;
        } else if (bookingActionDto.getEvent() == 5) {
            endState = 5;
        }
        return endState;
    }*/

    public BookingDto buildBookingDto() {
        BookingDto bookingDto = new BookingDto();
        bookingDto.setId(booking.getId());

        List<BookingSpaceExtraAmenity> bookingSpaceExtraAmenityList = bookingSpaceExtraAmenityDao.getByBookingId(booking.getId());
        Set<BookingWithExtraAmenityDto> bookingWithExtraAmenityDtoSet = new HashSet<>();//define a dto set
        for (BookingSpaceExtraAmenity bookingSpaceExtraAmenity1 : bookingSpaceExtraAmenityList) {
            BookingWithExtraAmenityDto bookingWithExtraAmenityDto1 = new BookingWithExtraAmenityDto();
            bookingWithExtraAmenityDto1.setAmenityId(bookingSpaceExtraAmenity1.getPk1().getAmenity().getId());
            bookingWithExtraAmenityDto1.setNumber(bookingSpaceExtraAmenity1.getNumber());
            bookingWithExtraAmenityDtoSet.add(bookingWithExtraAmenityDto1);
        }
        bookingDto.setBookingWithExtraAmenityDtoSet(bookingWithExtraAmenityDtoSet);
        bookingDto.setEventType(booking.getEventType());
        //bookingDto.setFromDate(booking.getFromDate().toString());
        bookingDto.setReservationStatus(booking.getReservationStatus());
        bookingDto.setSpace(booking.getSpace());
        bookingDto.setUser(booking.getUser());
        return bookingDto;

    }

    protected SpaceUnavailability generateSpaceUnavilablity(){
        User user=userDao.read(booking.getUser());
        SpaceUnavailability spaceUnavailablity =new SpaceUnavailability();
        spaceUnavailablity.setSpace(booking.getSpace());
        spaceUnavailablity.setIsBlocked(BooleanEnum.TRUE.value());
        spaceUnavailablity.setGuestContactNumber(user.getPhone());
        spaceUnavailablity.setGuestEmail(user.getEmail());
        spaceUnavailablity.setGuestName(user.getName());


        return spaceUnavailablity;

    }

}
