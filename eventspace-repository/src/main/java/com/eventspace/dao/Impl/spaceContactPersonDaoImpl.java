package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceContactPersonDao;
import com.eventspace.domain.SpaceContactPerson;
import org.springframework.stereotype.Repository;

@Repository
public class spaceContactPersonDaoImpl extends BaseDaoImpl<SpaceContactPerson> implements SpaceContactPersonDao {
}
