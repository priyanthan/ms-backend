/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.cryptography;

/**
 * This class facilitates encryption and decryption services.
 *
 * @author Auxenta Inc
 *
 */
public interface CryptographyProvider {

	/**
	 * Encrypt the given raw content.
	 *
	 * @param rawContent the raw content
	 *
	 * @return encrypted string
	 */
    String encryptContent(final String rawContent);

	/**
	 * Decrypt the given encrypted content.
	 *
	 * @param encryptedContent
	 *
	 * @return decrypted string
	 */
    String decryptContent(final String encryptedContent);

}
