package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by Aux-052 on 1/6/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingActionDto implements Serializable {

    private Integer booking_id;

    private Integer event;

    private String method;

    private Integer status;

    private Integer paymentSuccessCode;

    private Integer userId;

    private Double refund;

    private String proofFromGuest;

    private String ipgResponseCode;

    private String ipgName;

    public Integer getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Integer booking_id) {
        this.booking_id = booking_id;
    }

    public Integer getEvent() {
        return event;
    }

    public void setEvent(Integer event) {
        this.event = event;
    }

    public Integer getPaymentSuccessCode() {
        return paymentSuccessCode;
    }

    public void setPaymentSuccessCode(Integer paymentSuccessCode) {
        this.paymentSuccessCode = paymentSuccessCode;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getRefund() {
        return refund;
    }

    public void setRefund(Double refund) {
        this.refund = refund;
    }

    public String getProofFromGuest() {
        return proofFromGuest;
    }

    public void setProofFromGuest(String proofFromGuest) {
        this.proofFromGuest = proofFromGuest;
    }

    public String getIpgResponseCode() {
        return ipgResponseCode;
    }

    public void setIpgResponseCode(String ipgResponseCode) {
        this.ipgResponseCode = ipgResponseCode;
    }

    public String getIpgName() {
        return ipgName;
    }

    public void setIpgName(String ipgName) {
        this.ipgName = ipgName;
    }
}
