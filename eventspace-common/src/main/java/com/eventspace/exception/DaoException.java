package com.eventspace.exception;


public class DaoException extends EventspaceException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7281598284158247875L;


    /**
     * Instantiates a new lead m dao exception.
     *
     * @param exceptionMsgVal the exception msg val
     * @param e               the e
     */
    public DaoException(final String exceptionMsgVal, final Throwable e) {
        super(exceptionMsgVal, e);
    }


    /**
     * Instantiates a new lead m dao exception.
     *
     * @param exceptionMsgVal the exception msg val
     */
    public DaoException(final String exceptionMsgVal) {
        super(exceptionMsgVal);
    }

}
