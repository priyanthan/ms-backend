package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpaceAmenityDetailDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7797455140996929742L;

    private int space;

    private Integer[] amenity;

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    public Integer[] getAmenity() {
        return amenity;
    }

    public void setAmenity(Integer[] amenity) {
        this.amenity = amenity;
    }
}
