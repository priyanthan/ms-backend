package com.eventspace.service.impl;

import com.eventspace.dto.NotificationDto;
import com.eventspace.exception.EventspaceException;
import com.eventspace.service.NotificationService;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


@Service
public class NotificationServiceImpl implements NotificationService {

    @Override
    public Object sendNotification(NotificationDto notificationDto) {
        RestTemplate restTemplate = new RestTemplate();
        List<MediaType> acceptableMediaTypes = new ArrayList<>();
        acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<?> entity = new HttpEntity<Object>(notificationDto,headers);

        String url="http://localhost:3000/notify/";
        ResponseEntity<Object> response;
        try {
            response = restTemplate.exchange(url,HttpMethod.POST,entity, Object.class);
            response.getStatusCode();
            return response;
        } catch (Exception e) {
            throw new EventspaceException("api call failed ");
        }
    }
}
