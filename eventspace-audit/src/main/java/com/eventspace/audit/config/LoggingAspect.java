package com.eventspace.audit.config;

import com.eventspace.audit.dto.AuditMetaData;
import com.eventspace.audit.publisher.Impl.AuditEventPublisherImpl;
import com.eventspace.exception.EventspaceException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


//@Component
//@Aspect
public class LoggingAspect {

    @Autowired
    private AuditEventPublisherImpl auditEventPublisher;

    @Around("within(com.eventspace.service..*) && " + "execution(public * *(..))")
    public Object aroundExecution(ProceedingJoinPoint jp)  {

        AuditMetaData auditMetaData=new AuditMetaData();
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

        String message;
        Object result=null;
        try {
            message="Class :"+jp.getTarget().getClass().getSimpleName()
                    +"\nMethod :"+jp.getSignature().getName()
                    +"\nPayload"+ mapper.writeValueAsString(jp.getArgs());

            result = jp.proceed();
            message=message+"\nSuccess: " + true+"\nResult: " + mapper.writeValueAsString(result);
        } catch (Throwable e) {
            throw new EventspaceException(e.getMessage());
        }

        auditMetaData.setMessage(message);
        auditEventPublisher.proceedAuditEvent(auditMetaData);
        return result;

    }

}