package com.eventspace.ws.cache;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WrappedOutputStream extends ServletOutputStream {

    private StringBuffer originalOutput = new StringBuffer();
    private HttpServletResponse originalResponse;

    public WrappedOutputStream(HttpServletResponse response) {
        this.originalResponse = response;
    }

    @Override
    public String toString() {
        return this.originalOutput.toString();
    }

    @Override
    public void write(int arg0) throws IOException {

        originalOutput.append((char) arg0);
        originalResponse.getOutputStream().write(arg0);
    }
}
