package com.eventspace.enumeration;

import java.util.HashMap;

public enum CancellationPolicyEnum {


    FLEXIBLE(1),MODERATE(2),STRICT(3);

    private static final HashMap<Integer, CancellationPolicyEnum> typeValueMap = new HashMap<>();

    static {
        for (CancellationPolicyEnum type : CancellationPolicyEnum.values()) {
            typeValueMap.put(type.value, type);
        }
    }

    private int value;

    CancellationPolicyEnum(final int value) {
        this.value = value;
    }

    public static CancellationPolicyEnum getInstanceFromValue(final int codeValue) {
        return typeValueMap.get(codeValue);
    }

    public int value() {
        return value;
    }
}
