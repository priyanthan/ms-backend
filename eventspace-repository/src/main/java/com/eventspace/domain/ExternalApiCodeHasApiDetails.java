package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "external_api_code_has_api_details")
public class ExternalApiCodeHasApiDetails implements Serializable, BaseDomain{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    //@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    //@JoinColumn(name = "external_api_code",insertable=false,updatable=false)
    @Column(name = "external_api_code", nullable = false, unique = true)
    private Integer externalApiCode;

    //@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    //@JoinColumn(name = "api_details",insertable=false,updatable=false)
    @Column(name = "api_details", nullable = false, unique = true)
    private Integer apiDetails;

    //@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    //@JoinColumn(name = "api_request_method",insertable=false,updatable=false)
    @Column(name = "api_request_method", nullable = false)
    private Integer apiRequestMethod;

    @Column(name = "api_url", nullable = false)
    private String api_url;

    @Column(name = "external_space_id", nullable = false)
    private Integer externalSpaceId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getExternalApiCode() {
        return externalApiCode;
    }

    public void setExternalApiCode(Integer externalApiCode) {
        this.externalApiCode = externalApiCode;
    }

    public Integer getApiDetails() {
        return apiDetails;
    }

    public void setApiDetails(Integer apiDetails) {
        this.apiDetails = apiDetails;
    }

    public Integer getApiRequestMethod() {
        return apiRequestMethod;
    }

    public void setApiRequestMethod(Integer apiRequestMethod) {
        this.apiRequestMethod = apiRequestMethod;
    }

    public String getApi_url() {
        return api_url;
    }

    public void setApi_url(String api_url) {
        this.api_url = api_url;
    }

    public Integer getExternalSpaceId() {
        return externalSpaceId;
    }

    public void setExternalSpaceId(Integer externalSpaceId) {
        this.externalSpaceId = externalSpaceId;
    }

    @Override
    public Object build() {
        return null;
    }
}
