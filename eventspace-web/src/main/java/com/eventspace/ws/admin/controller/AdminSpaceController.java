package com.eventspace.ws.admin.controller;

import com.eventspace.dto.*;
import com.eventspace.enumeration.BooleanEnum;
import com.eventspace.service.SpaceService;
import com.eventspace.ws.controller.BookingController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class AdminSpaceController.
 */
@RestController
@RequestMapping(value = "/api/admin/space")
public class AdminSpaceController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(BookingController.class);

    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    /**
     * Get all spaces.
     *
     * @return the list< spaces>
     */

    @GetMapping("/details/{id}")
    @ResponseStatus(HttpStatus.OK)
    public SpaceDetailDto getSpace(@PathVariable Integer id) {
        SpaceDetailDto result = spaceService.getSpace(id,null,true);
        return result;
    }

    @GetMapping("/{page}")
    @ResponseStatus(HttpStatus.OK)
    public List<AdminSpaceDetailsDto>  getAllSpaces(@PathVariable Integer page){
        LOGGER.info("getAllSpaces  ----->\t"+page);
        return spaceService.getAdminSpaces(page);
    }

    @GetMapping("/new/{page}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getAllSpaces1(@PathVariable Integer page){
        LOGGER.info("getAllSpaces  ----->\t"+page);
        Map<String,Object>  result=new HashMap<>();
        result.put("count",spaceService.getAllSpacesCount());
        result.put("spaces",spaceService.getAdminSpaces(page));
        return result;
    }

    @GetMapping("/active/{active}")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object>  getFilterSpaces(@PathVariable Integer active,@RequestParam Integer page){
        LOGGER.info("getAllSpaces  ----->\t"+page);
        Map<String,Object>  result=new HashMap<>();
        result.put("count",spaceService.getAdminFilterSpacesCount(active));
        result.put("spaces",spaceService.getAdminFilterSpaces(page,active));
        return result;
    }

    /**
     * Approve a space.
     *
     * @param space the space
     * @return the int
     */
    @PostMapping("/acceptance")
    @ResponseStatus(HttpStatus.OK)
    public int approveSpace(@RequestBody AdminSpaceDetailsDto space){
        LOGGER.info("approveSpace ----->\t"+space.getId());
        return spaceService.adminApproveSpace(space.getId());
    }

    /**
     * enable auto publish.
     *
     * @param requestDto the AdminAutoPublishRequestDto
     * @return the int
     */
    @PostMapping("/auto_publish")
    @ResponseStatus(HttpStatus.OK)
    public Integer enableAutoPublish(@RequestBody AdminAutoPublishRequestDto requestDto){
        LOGGER.info("enableAutoPublish ----->\t");
        return spaceService.enableAutoPublish(requestDto.isEnable());
    }

    @GetMapping("/featured")
    @ResponseStatus(HttpStatus.OK)
    public List<FeaturedSpacesDto> getAllFeaturedSpaces(){
        LOGGER.info("getAllFeaturedSpaces----->");
        return spaceService.getAdminFeaturedSpaces();
    }

    @PostMapping("/feature")
    @ResponseStatus(HttpStatus.OK)
    public String enableFeaturedSpaces(@RequestBody FeaturedSpacesDto featuredSpacesDto){
        LOGGER.info("enableFeaturedSpaces ----->");
        spaceService.saveFeaturedSpaces(featuredSpacesDto);
        return "success";
    }

    @PostMapping("/unfeature")
    @ResponseStatus(HttpStatus.OK)
    public String dinableFeaturedSpaces(@RequestBody CommonDto id){
        LOGGER.info("disableFeaturedSpaces ----->\t");
        spaceService.unFeaturedSpace(id.getId());
        return "success";
    }

    @GetMapping("/clearCache")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> clearCache(){
        LOGGER.info("clearCache  ----->\t");
        Map<String,Object>  result=new HashMap<>();
        result.put("response","OK");
        return result;
    }

    @PostMapping("/priority")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> savePrioritySpaces(@RequestBody PrioritySpacesDto prioritySpacesDto){
        LOGGER.info("savePrioritySpaces ----->");
        return spaceService.savePrioritySpaces(prioritySpacesDto.getSpaces());
    }

    @PutMapping("/priority")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> updatePrioritySpaces(@RequestBody PrioritySpacesDto prioritySpacesDto){
        LOGGER.info("updatePrioritySpaces ----->");
        return spaceService.updatePrioritySpaces(prioritySpacesDto.getSpaces());
    }

    @GetMapping("/priority")
    @ResponseStatus(HttpStatus.OK)
    public PrioritySpacesDto getPrioritySpaces(){
        LOGGER.info("getPrioritySpaces ----->");
        return spaceService.getPrioritySpaces();
    }

    @GetMapping("/host/{spaceId}")
    @ResponseStatus(HttpStatus.OK)
    public HostChangeDto spaceHostDetails(@PathVariable Integer spaceId){
        LOGGER.info("changeHostOfSpace ----->");
        return spaceService.spaceHostDetails(spaceId);
    }


    @PostMapping("/changeHost")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> changeHostOfSpace(@RequestBody HostChangeDto hostChangeDto){
        LOGGER.info("changeHostOfSpace ----->");
        return spaceService.changeHostOfSpace(hostChangeDto);
    }

    @PostMapping("/changeExpireDate")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> changeExpireDateOfSpace(@RequestBody HostChangeDto hostChangeDto){
        LOGGER.info("changeExpireDateOfSpace ----->");
        return spaceService.changeExpireDateOfSpace(hostChangeDto);
    }

    @PostMapping("/addPrimaryEvent")
    @ResponseStatus(HttpStatus.OK)
    public Map<String,Object> addPrimaryEventType(@RequestBody AddPrimaryEventTypeDto addPrimaryEventTypeDto){
        LOGGER.info("addPrimaryEventType ----->");
        return spaceService.addPrimaryEventType(addPrimaryEventTypeDto);
    }

}
