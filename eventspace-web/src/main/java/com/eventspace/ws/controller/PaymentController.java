/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.ws.controller;

import com.eventspace.dto.CommonDto;
import com.eventspace.dto.PaymentDetailsDto;
import com.eventspace.dto.PaymentSummaryDto;
import com.eventspace.dto.UpayResponseDto;
import com.eventspace.service.PaymentService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * The Class PaymentController.
 */
@RestController
@RequestMapping("/api/payment")
public class PaymentController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(PaymentController.class);

    /**
     * The payment service.
     */
    @Autowired
    private PaymentService paymentService;

    /**
     * Process payment.
     *
     * @param encryptedString the encrypted string
     * @return the payment details dto
     */
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public PaymentDetailsDto processPayment(@RequestParam String encryptedString) {
        LOGGER.info("processPayment method in PaymentController -----> get call");
        return paymentService.payments(encryptedString);

    }

    @PostMapping(value = "/summary", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public PaymentSummaryDto getSummary(CommonDto commonDto) {
        LOGGER.info("processPayment method in PaymentController -----> get call");
        return paymentService.getPaymentSummary(commonDto.getId());

    }

    @PostMapping("/paycrop")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> intiateIpgPayCorp(@RequestBody CommonDto booking) {
        LOGGER.info("Controller| intiateIpgPayCorp get called");
        return paymentService.initiatedPaycorp(booking);
    }

    @RequestMapping(value = "/paycrop/response", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void proceedIpgPayCorpResponse(HttpServletRequest request, HttpServletResponse response){
        paymentService.completePaycorp(request, response);
    }

    @PostMapping("/upay/response")
    @ResponseStatus(HttpStatus.OK)
    public void proceedUpayResponse(@RequestBody UpayResponseDto upayResponseDto,HttpServletResponse response){
        paymentService.completeUpay(upayResponseDto,response);
    }
}