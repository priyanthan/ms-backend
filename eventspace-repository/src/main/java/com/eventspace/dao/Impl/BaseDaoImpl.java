/*
 * Nadee GFHGFJ
 */
package com.eventspace.dao.Impl;

import com.eventspace.dao.BaseDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * The Class BaseDaoImpl.
 *
 * @param <T>
 *            the generic type
 */
public abstract class BaseDaoImpl<T> implements BaseDao<T> {

	// /** The type. */
	/** The type. */
	Class<T> type;

	/** The session factory. */
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HibernateTemplate hibernateTemplate;

	/**
	 * Instantiates a new base dao impl.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BaseDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		// T's actual type. This is only used when type obj
		// cannot be sent in the method parameter
		type = (Class) pt.getActualTypeArguments()[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sl.com.base.dao.BaseDao#creat(java.lang.Object)
	 */
	@Override
	public void create(T model) {
		getCurrentSession().save(model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sl.com.base.dao.BaseDao#read(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public T read(Integer id) {
		return (T) getCurrentSession().get(type, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sl.com.base.dao.BaseDao#update(java.lang.Object)
	 */
	@Override
	public void update(T model) {
		getCurrentSession().update(model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.auxenta.localcoins.dao.BaseDao#delete(java.lang.Object)
	 */
	@Override
	public void delete(T model) {
		getCurrentSession().delete(model);
	}

	@Override
	public void merge(T model) {
		 getCurrentSession().merge(model);
	}

	/**
	 * Gets the current session.
	 *
	 * @return the current session
	 */
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

}
