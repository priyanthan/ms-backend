/*
 *================================================================
 * Copyright  (c)     : 2014 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security.service.cookie;

import com.eventspace.security.cryptography.CryptographyProvider;
import com.eventspace.util.StringUtils;
import com.thoughtworks.xstream.XStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class CookieManagementServiceImpl implements CookieManagementService {
	
    /** The crypto provider. */
    private CryptographyProvider cryptoProvider;

    /** The is cookie secure. */
    @Value("${secure.cookie}")
    private Boolean isCookieSecure;

	/**
	 * {@inheritDoc}
	 */
	public Object retrieveCookieObject(String key,
			HttpServletRequest request) {

		String decryptedCookieXml = retrieveCookieValue(key, request);
		if (StringUtils.isEmpty(decryptedCookieXml)) {
			return null;
		}
		
		XStream xStream = new XStream();
        return xStream.fromXML(decryptedCookieXml);
		
	}

	/**
	 * {@inheritDoc}
	 */
	public String retrieveCookieValue(String key,
			HttpServletRequest request) {
		
		if ( request == null || key == null) {
			return "";
		}
		Cookie cookie = getCookie(key, request);
		if (cookie == null) {
			return "";
		}
		String encryptedCookie = cookie.getValue();
		if (StringUtils.isEmpty(encryptedCookie)) {
			return "";
		}

		return this.cryptoProvider.decryptContent(encryptedCookie);
	}
	
	 /**
     * {@inheritDoc}
     */
    public void addOrUpdateCookie(Object object, String name, int maxAge, HttpServletRequest request,
            HttpServletResponse response) {

        if (!StringUtils.isEmpty(name)) {

            XStream xStream = new XStream();
            String str = xStream.toXML(object);
            
            if (!StringUtils.isEmpty(str)) {
                addOrUpdateCookieContent(name, maxAge, str, null, request, response);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public void addOrUpdateCookieContent(String name, int maxAge, String content, String path, HttpServletRequest request,
            HttpServletResponse response) {

        String trimmedContent = StringUtils.removeCarriageReturnAndLineFeed(content);

        String strEncoded = cryptoProvider.encryptContent(trimmedContent);
        Cookie cookie = getCookie(name, request);
        if (cookie != null) {
            cookie.setValue(strEncoded);
        } else {
            cookie = new Cookie(name, strEncoded);
        }
        cookie.setMaxAge(maxAge);
        cookie.setPath(StringUtils.isEmpty(path) ? "/" : path);

        responseAppender(cookie, response);
    }
    
    /**
     * {@inheritDoc}
     */
    public void removeCookie(String name, HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = getCookie(name, request);

        if (cookie != null) {
            cookie.setMaxAge(0);
            cookie.setPath("/");
            responseAppender(cookie, response);
        }

    }
    
    /**
     * Add the cookie to response.
     *
     * @param cookie the cookie
     * @param response the response
     */
    private void responseAppender(Cookie cookie, HttpServletResponse response) {
        if (this.isCookieSecure != null && this.isCookieSecure.booleanValue()) {
            cookie.setSecure(this.isCookieSecure.booleanValue());
        }
        response.addCookie(cookie);
    }
    
    
    /**
     * Gets cookie.
     *
     * @param name the name
     * @param request the request
     * @return Cookie
     */
    private Cookie getCookie(String name, HttpServletRequest request) {
    	name = name.trim();
        if (request.getCookies() != null && request.getCookies().length > 0) {
            for (Cookie cookie : request.getCookies()) {
                if (cookie.getName().equalsIgnoreCase(name))
                    return cookie;
            }
        }
        return null;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void expireCookie(String name, HttpServletRequest request, HttpServletResponse response) {

        Cookie cookie = getCookie(name, request);

        if (cookie != null) {
            cookie.setMaxAge(0);
            cookie.setPath("/");
            responseAppender(cookie, response);
        }

    }
    
    /**
     * Gets the cryptography provider.
     *
     * @return cryptography provider
     */
    public CryptographyProvider getCryptographyProvider() {
        return cryptoProvider;
    }

    /**
     * Sets the cryptography provider.
     *
     * @param cryptoProvider the new cryptography provider
     */
    public void setCryptographyProvider(CryptographyProvider cryptoProvider) {
        this.cryptoProvider = cryptoProvider;
    }

}
