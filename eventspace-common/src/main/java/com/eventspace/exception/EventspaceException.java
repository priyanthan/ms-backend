/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */

package com.eventspace.exception;


/**
 * The Class EventspaceException.
 */
public class EventspaceException extends RuntimeException {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8318987323162221832L;

    /**
     * The exception msg.
     */
    private String exceptionMsg;

    /**
     * The Constructor.
     *
     * @param exceptionMsgVal the exception msg val
     * @param e               the e
     */
    EventspaceException(final String exceptionMsgVal, final Throwable e) {
        super(exceptionMsgVal, e);
        this.exceptionMsg = exceptionMsgVal;
    }

    /**
     * The Constructor.
     *
     * @param exceptionMsgVal the exception msg val
     */
    public EventspaceException(final String exceptionMsgVal) {
        this.exceptionMsg = exceptionMsgVal;
    }

    /**
     * Gets the exception msg.
     *
     * @return the exception msg
     */
    public String getExceptionMsg() {
        return this.exceptionMsg;
    }

    /**
     * Sets the exception msg.
     *
     * @param exceptionMsgVal the new exception msg
     */
    public void setExceptionMsg(final String exceptionMsgVal) {
        this.exceptionMsg = exceptionMsgVal;
    }
}
