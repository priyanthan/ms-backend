package com.eventspace.ws.exception;

import com.eventspace.dto.ExceptionDto;
import com.eventspace.exception.EventspaceException;
import com.eventspace.service.impl.SpaceServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
public class MyControllerAdvice {


    private static final Log LOGGER = LogFactory.getLog(SpaceServiceImpl.class);

    @ExceptionHandler
    public ResponseEntity<ExceptionDto> handleException(HttpServletRequest request, Exception exception) {
        return build(exception,exception.toString(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(EventspaceException.class)
    public ResponseEntity<ExceptionDto> handleEventSpaceException(HttpServletRequest request, EventspaceException exception) {
       return build(exception,exception.getExceptionMsg(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ExceptionDto> build(Exception exception, String message,HttpStatus httpStatus) {
        StringBuilder bld = new StringBuilder();
        bld.append(exception.getMessage());

        for (StackTraceElement l : exception.getStackTrace()) {
            if (l.getClassName().startsWith("com.eventspace"))
                bld.append( "\n\t\t\t\t"+l);
        }

        LOGGER.error(exception.getMessage() + bld.toString());

        ExceptionDto error = new ExceptionDto();
        error.setCode(httpStatus.value());
        error.setError(message);
        return new ResponseEntity<>(error,httpStatus);
    }


}
