package com.eventspace.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Auxenta on 7/26/2017.
 */
@Entity
@Table(name = "booking_slots")
@AssociationOverrides({
        @AssociationOverride(name="booking"
                ,joinColumns=@JoinColumn(name="booking", nullable = false))
})
public class BookingSlots  implements Serializable, BaseDomain{

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The booking.
     */
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "booking", updatable=false)
    private Booking booking;

    /**
     * The from date.
     */
    @Column(name = "from_date", nullable = false)
    private Date fromDate;

    /**
     * The to date.
     */
    @Column(name = "to_date", nullable = false)
    private Date toDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    @Override
    public Object build() {
        return null;
    }
}
