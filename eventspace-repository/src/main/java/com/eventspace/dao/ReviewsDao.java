package com.eventspace.dao;

import com.eventspace.domain.Reviews;

import java.util.List;


/**
 * Event space
 * Created by Aux-054 on 12/16/2016.
 */
public interface ReviewsDao extends BaseDao<Reviews> {

    Double getRatingBySpaceId(Integer spaceId);

    Boolean checkReviewByBookingId(Integer bookingId);

    Reviews getReviewByBookingId(Integer bookingId);

    List<Reviews> getMSReviews();

}
