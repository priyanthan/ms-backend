package com.eventspace.service;

/**
 * Created by Auxenta on 6/7/2017.
 */
public interface CryptographyService {

    String encrypt(String originalString);
    String decrypt(String encryptedString);
    String encryption(Integer key);
    Integer decryption(String encrypted);
}
