/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.service;

import com.eventspace.domain.Booking;
import com.eventspace.domain.BookingSlots;
import com.eventspace.dto.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The Interface BookingService.
 */
public interface BookingService {

    Booking bookSpace(BookingDto bookingDto, Integer userId);

    String addReview(ReviewDto reviewDto,Integer userId);

    Map<String,Object> addReviewByEmail(String key,ReviewDto reviewDto);

    Map<String, Object> bookingActions(BookingActionDto bookingActionDto,boolean isAdmin)
            throws Exception;

    List<BookingDetailsDto> getBookings(Integer userId);

    List<BookingDetailsDto> getUserBooking(Integer userId);

    List<BookingDetailsDto> getHostBooking(Integer userId);

    List<BookingDetailsDto> getSpaceBooking(Integer spaceId,Integer userId);

    BookingDetailsDto getABooking(BookingCommonDto bookingCommonDto,Integer userId);

    Double getHiringCharge(Integer bookingId);

    BookingDetailsDto getBooking(Integer bookingId, Integer userId);

    Boolean isCancelledAfterPaid(Integer bookingId);

    List<ReservationStatusDto> getAllreservationStatus();

    String markAsSeenNotification(Integer bookingId,Integer reservationStatus);

    List<AdminBookingDetailsDto> getAllBookings(int page);

    AdminBookingDetailsDto generateAdminBookingDto(Integer bookingId);

    List<UserLogDetails> getUserLogs(Integer userId);

    SpaceUnavailabilityDto getManualBooking(Integer id,Integer userId, boolean isAdmin);

    Integer addHostManualBookingDetails(SpaceUnavailabilityDto spaceUnavailabilityDto, Integer userId, boolean isAdmin);

    Integer editHostManualBookingDetails(SpaceUnavailabilityDto spaceUnavailabilityDto, Integer userId, boolean isAdmin);

    Integer deleteHostManualBookingDetails(Integer manualBookingId, Integer userId, boolean isAdmin);

    void expireBooking(Integer bookingId);

    String paymentCallback(PaymentCallbackDto paymentCallbackDto,boolean isMobile);

    void addBookingToSpaceUnavailablity(Integer bookingId,Set<BookingSlots> bookingSlotsSet,Integer spaceId);

    void removeBookingFromSpaceUnavailablity(Integer bookingId);

    Map<String, Object> addRemark(RemarkDto remarkDto, Integer userId);

    Map<String,Object>  checkPromoCode(Integer spaceId, String promoCode,UserContext userId);

    Boolean isValidPromo(Integer spaceId, String promoCode,Integer userId);

    PromoDetailDto getPromoDetails(String promoCode);

    void confirmBookings();

    Long getAllBookingCount();

    List<AdminBookingDetailsDto> getAllTentitiveBookings(Integer page);

    ChargeDetailsDto calculateBookingCharge(BookingDto bookingDto);

    int findRefund(Integer bookingId);

    boolean isUserAllowForManualPayment(Integer user);

    boolean isTimeAllowForManualPayment(Set<BookingSlots>  bookingSlots);

    List<AdminPaidBookingDetails> getAdminPaidBookings(Integer page);

    Long getBookingsCount(Integer bookingStatus);

    AdminPaidBookingDetails setAdminPaidBookingDetails(Integer bookingId);

    Integer markAsPaymentReceive(PaymentVerifyDto paymentVerifyDto);

    BookingDetailsDto generateSimpleBookingDetails(Integer bookingId);

    Map<String,Object> getAUserBookingWithPagination(Integer userId,Integer page);

    Long getVerifiedBookingCount();

    Integer verifyPaymentReceiveFromGuest(PaymentVerifyDto paymentVerifyDto);

    Integer verifyPaymentSendToHost(PaymentVerifyDto paymentVerifyDto);

    Integer getAUserBookingUserAPromoCode(Integer userId,Integer promoId);

    Boolean isPayLaterEnabled(Integer bookingId);

    void expirePayLater(Integer bookingId);

    void createPayLaterRecord(Integer bookingId);

    Map<String,Object> payLaterDetails(Integer bookingId);

    Map<String,Object> getPromoDetails(Integer bookingId);

    Map<String,Object> markAsPaymentDone(Integer bookingId);

    public Integer getBookingIdFromOrderId(String orderId);

    AdminBookingDetailsDto getAAdminTentitiveBooking(Integer bookingId);

    List<AdminBookingFinanceDto> getFinanceDetails(Integer page);


}
