package com.eventspace.audit.publisher;


import com.eventspace.audit.dto.AuditMetaData;

public interface AuditEventPublisher {

	void proceedAuditEvent(AuditMetaData smsMetaData);

}
