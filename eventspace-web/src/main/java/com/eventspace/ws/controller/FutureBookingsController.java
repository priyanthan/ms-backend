package com.eventspace.ws.controller;

import com.eventspace.service.SpaceService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/futureBookedDates")
public class FutureBookingsController {

    /**
     * The Constant LOGGER.
     */
    private static final Log LOGGER = LogFactory.getLog(FutureBookingsController.class);
    /**
     * The space service.
     */
    @Autowired
    private SpaceService spaceService;

    @GetMapping("/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Object getFutureBookings(@PathVariable Integer id) {
        LOGGER.info("getFutureBookings ----->\t"+id);
        return spaceService.getUpcomingPaidBookingsDuration(id);
    }
}
