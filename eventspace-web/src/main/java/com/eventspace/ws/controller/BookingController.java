/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.ws.controller;

import com.eventspace.domain.Booking;
import com.eventspace.dto.*;
import com.eventspace.enumeration.EventsEnum;
import com.eventspace.security.facade.SecurityFacade;
import com.eventspace.service.*;
import com.eventspace.util.LogMessages;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class BookingController.
 */
@RestController
@RequestMapping(value = "/api/book")
public class BookingController {

    /**
     * The Constant logger.
     */
    private static final Log LOGGER = LogFactory
            .getLog(BookingController.class);
    /**
     * The booking service.
     */
    @Autowired
    private BookingService bookingService;

    /**
     * The email service.
     */
    @Autowired
    private EmailService emailService;
    /**
     * The security facade.
     */
    @Autowired
    private SecurityFacade securityFacade;

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private SpaceService spaceService;

    @Autowired
    private CommonService commonService;


    @GetMapping(value = "/space/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BookingSpaceDto getBookingSpaceDetail(@PathVariable Integer id,HttpServletRequest request) throws Exception  {
        UserContext user = securityFacade.getUserContext(request);
        BookingSpaceDto bookingSpaceDto=spaceService.getBookingSpace(id);
        bookingSpaceDto.setUserDro(user);
        return bookingSpaceDto;
    }
    /**
     * Book space.
     *
     * @param bookingDto the booking dto
     * @param request    the request
     * @return the Mamapp<string, object>
     * @throws Exception the exception
     */
    @PostMapping(value = "/space")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> bookSpace(@RequestBody BookingDto bookingDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info(String.format(LogMessages.BC_BOOK_SPACE, bookingDto.getSpace(), user.getEmail()));
        Map<String, Object> response = new HashMap<>();
        if (bookingDto.getPromoCode() == null || (bookingDto.getPromoCode() != null && bookingService.isValidPromo(bookingDto.getSpace(), bookingDto.getPromoCode(), user.getUserId()))) {
            if (spaceService.isNotBlockTime(bookingDto)) {
                String browserType = request.getHeader("User-Agent");
                bookingDto.setDevice(browserType);
                Booking booking = bookingService.bookSpace(bookingDto, user.getUserId());

                if (booking != null) {
                    LOGGER.info(String.format(LogMessages.BC_BOOK_SPACE_BOOKING_SUCCESS, booking.getOrderId()));
                    response = bookingService.payLaterDetails(booking.getId());
                    response.put("isUserEligibleForManualPayment", bookingService.isUserAllowForManualPayment(booking.getUser()));
                    response.put("isBookingTimeEligibleForManualPayment", bookingService.isTimeAllowForManualPayment(booking.getBookingSlots()));
                    response.put("isPayLaterEnabled", bookingService.isPayLaterEnabled(booking.getId()));
                    emailService.sendBookingActionMails(booking.getId(), 0);

                } else {
                    LOGGER.info(String.format(LogMessages.BC_BOOK_SPACE_BOOKING_FAILED));
                    response.put("error", "This booking not allowed");
                }
            } else {
                LOGGER.info(String.format(LogMessages.BC_BOOK_SPACE_TIME_UNAVAILABLE));
                response.put("error", "You just missed it! Sorry, your booking time is unavailable.");
            }

        } else {
            LOGGER.info(String.format(LogMessages.BC_BOOK_SPACE_WRONG_PROMO));
            response.put("error", "Wrong promo code");
        }
        return response;
    }

    /**
     * Add review.
     *
     * @param reviewDto the review dto
     * @param request   the request
     * @return the string
     * @throws Exception the exception
     */
    @PostMapping(value = "/addReview")
    @ResponseStatus(HttpStatus.CREATED)
    public String addReview(@RequestBody ReviewDto reviewDto, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_REVIEW_ADD, reviewDto.getBookingId()));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.addReview(reviewDto, user.getUserId());
    }

    @PutMapping(value = "/addRemark")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, Object> addRemark(@RequestBody RemarkDto remarkDto, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_REMARK_ADD, remarkDto.getBookingId()));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.addRemark(remarkDto, user.getUserId());
    }

    /**
     * Booking actions.
     *
     * @param bookingActionDto the booking action dto
     * @param request          the request
     * @return the map<string, object>
     * @throws Exception the exception
     */
    @PutMapping(value = "/space")
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String, Object> bookingActions(@RequestBody BookingActionDto bookingActionDto, HttpServletRequest request) throws Exception {
        UserContext user = securityFacade.getUserContext(request);
        LOGGER.info(String.format(LogMessages.BC_BOOKING_ACTION, bookingActionDto.getBooking_id(), EventsEnum.getInstanceFromValue(bookingActionDto.getEvent()), user.getEmail()));
        bookingActionDto.setUserId(user.getUserId());
        Map<String, Object> result = bookingService.bookingActions(bookingActionDto, request.isUserInRole("ROLE_ADMIN"));
        return result;
    }

    /**
     * Is canceled after paid.
     *
     * @param id the id
     * @return the boolean
     */
    @GetMapping(value = "/isPaidCancel/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Boolean isCanceledAfterPaid(@PathVariable Integer id) {
        LOGGER.info(String.format(LogMessages.BC_CHECK_BOOKING_CALCEL_AFTER_PAID, id));
        return bookingService.isCancelledAfterPaid(id);
    }

    /**
     * Get reservation statuses.
     *
     * @return the list<reservation status>
     */
    @GetMapping(value = "/reservationStatuses")
    @ResponseStatus(HttpStatus.OK)
    public List<ReservationStatusDto> getReservationStatuses() {
        LOGGER.info(String.format(LogMessages.BC_RESERVATION_STATUS_GET_ALL));
        return bookingService.getAllreservationStatus();
    }

    /**
     * mark as seen notification.
     *
     * @param bookingId         the booking id
     * @param reservationStatus the reservation status
     * @return the string
     */
    @GetMapping(value = "/user/notification_seen")
    @ResponseStatus(HttpStatus.OK)
    public String markAsSeenNotification(@RequestParam("bookingId") Integer bookingId, @RequestParam("reservationStatus") Integer reservationStatus) {
        LOGGER.info(String.format(LogMessages.BC_NOTIFICATION_MARK_AS_SEEN, bookingId, reservationStatus));
        return bookingService.markAsSeenNotification(bookingId, reservationStatus);
    }

    @GetMapping(value = "/manual/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public SpaceUnavailabilityDto getManualBooks(@PathVariable Integer id, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_MANUAL_GET, id));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.getManualBooking(id, user.getUserId(), request.isUserInRole("ROLE_ADMIN"));
    }

    /**
     * add manual books .
     *
     * @param spaceUnavailabilityDto the space unavailability dto
     * @param request                the request
     * @return integer
     * @throws Exception the exception
     */
    @PostMapping(value = "/manual")
    @ResponseStatus(HttpStatus.CREATED)
    public Integer addManualBooks(@RequestBody SpaceUnavailabilityDto spaceUnavailabilityDto, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_MANUAL_ADD, spaceUnavailabilityDto.getSpace()));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.addHostManualBookingDetails(spaceUnavailabilityDto, user.getUserId(), request.isUserInRole("ROLE_ADMIN"));
    }

    /**
     * update manual books .
     *
     * @param spaceUnavailabilityDto the space unavailability dto
     * @param request                the request
     * @return integer
     * @throws Exception the exception
     */
    @PutMapping(value = "/manual")
    @ResponseStatus(HttpStatus.CREATED)
    public Integer updateManualBooks(@RequestBody SpaceUnavailabilityDto spaceUnavailabilityDto, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_MANUAL_UPDATE, spaceUnavailabilityDto.getId()));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.editHostManualBookingDetails(spaceUnavailabilityDto, user.getUserId(), request.isUserInRole("ROLE_ADMIN"));
    }

    /**
     * delete manual books .
     *
     * @param id      the  id
     * @param request the request
     * @return integer
     * @throws Exception the exception
     */

    @DeleteMapping(value = "/manual/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Integer deleteManualBooks(@PathVariable("id") int id, HttpServletRequest request) throws Exception {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_MANUAL_DELETE, id));
        UserContext user = securityFacade.getUserContext(request);
        return bookingService.deleteHostManualBookingDetails(id, user.getUserId(), request.isUserInRole("ROLE_ADMIN"));
    }

    @PostMapping(value = "/bookingCharge")
    @ResponseStatus(HttpStatus.CREATED)
    public ChargeDetailsDto findBookingCharge(@RequestBody BookingDto bookingDto) {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_CALCULATE_CHARGE, bookingDto.getSpace()));
        return bookingService.calculateBookingCharge(bookingDto);
    }

    @GetMapping(value = "/refund/{id}")
    public Map<String, Object> findRefund(@PathVariable Integer id) {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_CALCULATE_REFUND, id));
        Map<String, Object> response = new HashMap<>();
        response.put("refund", bookingService.findRefund(id));
        response.put("time", commonService.getCurrentTime());
        return response;
    }

    @GetMapping(value = "/paylater/{id}")
    public Map<String, Object> payLaterDetails(@PathVariable Integer id) {
        LOGGER.info(String.format(LogMessages.BC_BOOKING_GET_PAY_LATER_DETAILS, id));
        return bookingService.payLaterDetails(id);
    }
}
