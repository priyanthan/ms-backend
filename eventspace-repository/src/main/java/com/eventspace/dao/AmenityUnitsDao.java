package com.eventspace.dao;

import com.eventspace.domain.AmenityUnit;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
public interface AmenityUnitsDao extends BaseDao<AmenityUnit> {

    List<AmenityUnit> getAllAmenityUnits();

}
