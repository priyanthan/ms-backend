package com.eventspace.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class UserStatsDto implements Serializable {

    private static final long serialVersionUID = 7797455140996929742L;

    private Date date;
    private int total;
    private int userCount;
    private int guestCount;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(int guestCount) {
        this.guestCount = guestCount;
    }
}
