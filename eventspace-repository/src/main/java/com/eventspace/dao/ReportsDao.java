package com.eventspace.dao;

import java.util.List;
import java.util.Map;

public interface ReportsDao {
     List<Map<String,Object>> procedureListResult(String procedureName);

     Map<String,Object> procedureUniqueResult(String procedureName);

     List<Map<String,Object>> queryListResults(String sqlQuery);

     Map<String,Object> queryUniqueResults(String sqlQuery);

}
