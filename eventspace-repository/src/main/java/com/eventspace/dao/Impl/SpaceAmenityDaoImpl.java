package com.eventspace.dao.Impl;

import com.eventspace.dao.SpaceAmenityDao;
import com.eventspace.domain.SpaceAmenity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Aux-052 on 12/14/2016.
 */
@Repository
public class SpaceAmenityDaoImpl extends BaseDaoImpl<SpaceAmenity> implements SpaceAmenityDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SpaceAmenityDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public List<SpaceAmenity> getAllSpaceAmenities() {
        LOGGER.info("getAllSpaceAmenities method in SpaceAmenityDaoImpl-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<SpaceAmenity> spaceAmenityList =currentSession.createCriteria(SpaceAmenity.class).list();
        return spaceAmenityList;
    }
}

