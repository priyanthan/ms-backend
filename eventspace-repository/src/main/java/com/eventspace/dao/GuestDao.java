package com.eventspace.dao;

import com.eventspace.domain.Guest;

public interface GuestDao  extends BaseDao<Guest> {
}
