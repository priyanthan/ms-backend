package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BookingSpaceDto implements Serializable {
    private static final long serialVersionUID = 7797455140996929742L;

    private Integer id;
    private String name;
    private String addressLine1;
    private String organizationName;
    private String calendarEnd;
    private Integer minParticipantCount;
    private Integer advanceOnlyEnable;
    private String availabilityMethod;
    private Integer participantCount;
    private Integer chargeType;
    private Integer blockChargeType;
    private List<AmenityDto> amenity;
    private List<EventTypeDto> eventType;
    private List<SeatingArrangementDto> seatingArrangements;
    private List<ExtraAmenityDto> extraAmenity;
    private SpaceOwnerDto userDro;
    private SpaceOwnerDto spaceOwnerDto;
    private Set<AvailablityDetailsDto> availability;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getCalendarEnd() {
        return calendarEnd;
    }

    public void setCalendarEnd(String calendarEnd) {
        this.calendarEnd = calendarEnd;
    }

    public Integer getMinParticipantCount() {
        return minParticipantCount;
    }

    public void setMinParticipantCount(Integer minParticipantCount) {
        this.minParticipantCount = minParticipantCount;
    }

    public Integer getAdvanceOnlyEnable() {
        return advanceOnlyEnable;
    }

    public void setAdvanceOnlyEnable(Integer advanceOnlyEnable) {
        this.advanceOnlyEnable = advanceOnlyEnable;
    }

    public String getAvailabilityMethod() {
        return availabilityMethod;
    }

    public void setAvailabilityMethod(String availabilityMethod) {
        this.availabilityMethod = availabilityMethod;
    }

    public Integer getParticipantCount() {
        return participantCount;
    }

    public void setParticipantCount(Integer participantCount) {
        this.participantCount = participantCount;
    }

    public Integer getChargeType() {
        return chargeType;
    }

    public void setChargeType(Integer chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getBlockChargeType() {
        return blockChargeType;
    }

    public void setBlockChargeType(Integer blockChargeType) {
        this.blockChargeType = blockChargeType;
    }

    public List<AmenityDto> getAmenity() {
        return amenity;
    }

    public void setAmenity(List<AmenityDto> amenity) {
        this.amenity = amenity;
    }

    public List<EventTypeDto> getEventType() {
        return eventType;
    }

    public void setEventType(List<EventTypeDto> eventType) {
        this.eventType = eventType;
    }

    public List<SeatingArrangementDto> getSeatingArrangements() {
        return seatingArrangements;
    }

    public void setSeatingArrangements(List<SeatingArrangementDto> seatingArrangements) {
        this.seatingArrangements = seatingArrangements;
    }

    public List<ExtraAmenityDto> getExtraAmenity() {
        return extraAmenity;
    }

    public void setExtraAmenity(List<ExtraAmenityDto> extraAmenity) {
        this.extraAmenity = extraAmenity;
    }

    public SpaceOwnerDto getSpaceOwnerDto() {
        return spaceOwnerDto;
    }

    public void setSpaceOwnerDto(SpaceOwnerDto spaceOwnerDto) {
        this.spaceOwnerDto = spaceOwnerDto;
    }

    public Set<AvailablityDetailsDto> getAvailability() {
        return availability;
    }

    public void setAvailability(Set<AvailablityDetailsDto> availability) {
        this.availability = availability;
    }

    public SpaceOwnerDto getUserDro() {
        return userDro;
    }

    public void setUserDro(UserContext  userContext) {
        SpaceOwnerDto spaceOwnerDto=new SpaceOwnerDto();
        spaceOwnerDto.setId(userContext.getUserId());
        spaceOwnerDto.setName(userContext.getUsername());
        this.userDro = spaceOwnerDto;
    }
}
