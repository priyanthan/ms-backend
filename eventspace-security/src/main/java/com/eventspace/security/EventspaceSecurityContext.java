/*
 *================================================================
 * Copyright  (c)     : 2016 Auxenta Inc, All Rights Reserved
 *================================================================
 */
package com.eventspace.security;

import com.eventspace.security.domain.EventspaceAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;


/**
 * The Class EventspaceSecurityContext.
 */
public class EventspaceSecurityContext implements SecurityContext {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6605385617777114115L;

	/** The token. */
	private EventspaceAuthenticationToken token;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.security.core.context.SecurityContext#getAuthentication
	 * ()
	 */
	@Override
	public Authentication getAuthentication() {
		return this.token;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.security.core.context.SecurityContext#setAuthentication
	 * (org.springframework.security.core.Authentication)
	 */
	@Override
	public void setAuthentication(final Authentication authenticationVal) {
		this.token = (EventspaceAuthenticationToken) authenticationVal;
	}

}
