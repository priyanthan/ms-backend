package com.eventspace.dao.Impl;

import com.eventspace.dao.GuestDao;
import com.eventspace.domain.Guest;
import org.springframework.stereotype.Repository;

@Repository
public class GuestDaoImpl extends BaseDaoImpl<Guest> implements GuestDao {
}
