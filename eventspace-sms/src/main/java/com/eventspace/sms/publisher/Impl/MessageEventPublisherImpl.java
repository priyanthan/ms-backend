package com.eventspace.sms.publisher.Impl;

import com.eventspace.sms.dto.MessageMetaData;
import com.eventspace.sms.publisher.MessageEventPublisher;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

@Component
public class MessageEventPublisherImpl implements MessageEventPublisher {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(MessageEventPublisherImpl.class);

	@Autowired
	@Qualifier("messagejmsProducerTemplate")
	private  final JmsTemplate jmsTemplate = null;

	@Override
	public void proceedSampleEvent(final MessageMetaData messageMetaData) {
		logger.info(String.format(
				"proceedAuditEvent -> get called with auditMetaData : message[%s]",
				messageMetaData.getMessage()));


			jmsTemplate.send(new MessageCreator() {
				@Override
				public Message createMessage(Session session) throws JMSException {

					ObjectMessage objMessage = session.createObjectMessage();
					objMessage.setObject(messageMetaData);

					return objMessage;
				}
			});
	}
}
