package com.eventspace.dao.Impl;

import com.eventspace.dao.ImageDao;
import com.eventspace.domain.Image;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Aux-052 on 1/2/2017.
 */
@Repository
public class ImageDaoImpl extends BaseDaoImpl<Image> implements ImageDao {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;
    @Override
    public void removeImage(Integer imageId) {
        LOGGER.info("removeImage:{}     -----> get call",imageId);
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.createSQLQuery(String.format("DELETE  FROM image where id=%s",imageId)).executeUpdate();

    }
}

