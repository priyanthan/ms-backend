package com.eventspace.domain;

import com.eventspace.dto.AvailablityDetailsDto;
import com.eventspace.enumeration.BooleanEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;

/**
 * Created by Auxenta on 7/11/2017.
 */
@Entity
@Table(name = "per_hour_availablity")
public class PerHourAvailablity implements Serializable, BaseDomain{

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    /**
     * The space.
     */
    @ManyToOne(fetch=FetchType.LAZY) @JoinColumn(name="space", nullable = false)
    private Space space;

    @Column(name = "day")
    private Integer day;

    /**
     * The from date.
     */
    @Column(name = "from_time")
    private Time from;

    /**
     * The to date.
     */
    @Column(name = "to_time")
    private  Time to;

    /** The refundAmount. */
    @Column(name = "charge")
    private Double charge;

    /** The notificationSeen. */
    @Column(name = "active", nullable = false)
    private Integer active;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Time getFrom() {
        return from;
    }

    public void setFrom(Time from) {
        this.from = from;
    }

    public Time getTo() {
        return to;
    }

    public void setTo(Time to) {
        this.to = to;
    }

    public Double getCharge() {
        return charge;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public AvailablityDetailsDto build() {
        AvailablityDetailsDto availablityDetailsDto =new AvailablityDetailsDto();
        availablityDetailsDto.setId(id);
        availablityDetailsDto.setSpace(space.getId());
        availablityDetailsDto.setFrom(from.toString());
        availablityDetailsDto.setTo(to.toString());
        availablityDetailsDto.setDay(day);
        availablityDetailsDto.setCharge(charge);
        availablityDetailsDto.setIsAvailable(active);
        return availablityDetailsDto;
    }

    public static PerHourAvailablity build(Space space ,AvailablityDetailsDto availablityDetailsDto){
        PerHourAvailablity perHourAvailablity=new PerHourAvailablity();
        perHourAvailablity.setSpace(space);
        perHourAvailablity.setFrom(Time.valueOf(availablityDetailsDto.getFrom()));
        perHourAvailablity.setTo(Time.valueOf(availablityDetailsDto.getTo()));
        perHourAvailablity.setDay(availablityDetailsDto.getDay());
        perHourAvailablity.setCharge(availablityDetailsDto.getCharge());
        if (availablityDetailsDto.getIsAvailable()!=null)
            perHourAvailablity.setActive(availablityDetailsDto.getIsAvailable());
        else
            perHourAvailablity.setActive(BooleanEnum.TRUE.value());
        return perHourAvailablity;
    }
}
