package com.eventspace.dao;

import com.eventspace.domain.PerHourAvailablity;

import java.sql.Time;

/**
 * Created by Auxenta on 7/12/2017.
 */
public interface PerHourAvailablityDao extends BaseDao<PerHourAvailablity> {

    PerHourAvailablity findTheAvailablity(Integer spaceId, Integer day, Time fromTime, Time toTime);
}
