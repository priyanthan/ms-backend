package com.eventspace.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Aux-054 on 12/15/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookingDto implements Serializable {

    private Integer id;

    private String orderId;

    private Integer user;

    private Integer space;

    private Integer eventType;

    private Integer guestCount;

    private Integer seatingArrangement;

    private Double bookingCharge;

    private Double advance;

    private String promoCode;

    private Integer reservationStatus;

    private Boolean advanceOnly;

    private String method;

    private Set<CommonDto> dates;

    private Set<BookingWithExtraAmenityDto> bookingWithExtraAmenityDtoSet;

    public Set<BookingWithExtraAmenityDto> getBookingWithExtraAmenityDtoSet() {
        return bookingWithExtraAmenityDtoSet;
    }

    private String device;

    public void setBookingWithExtraAmenityDtoSet(Set<BookingWithExtraAmenityDto> bookingWithExtraAmenityDtoSet) {
        this.bookingWithExtraAmenityDtoSet = bookingWithExtraAmenityDtoSet;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSpace() {
        return space;
    }

    public void setSpace(Integer space) {
        this.space = space;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(Integer reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Double getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(Double bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Set<CommonDto> getDates() {
        return dates;
    }

    public void setDates(Set<CommonDto> dates) {
        this.dates = dates;
    }

    public Integer getSeatingArrangement() {
        return seatingArrangement;
    }

    public void setSeatingArrangement(Integer seatingArrangement) {
        this.seatingArrangement = seatingArrangement;
    }

    public Integer getGuestCount() {
        return guestCount;
    }

    public void setGuestCount(Integer guestCount) {
        this.guestCount = guestCount;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Double getAdvance() {
        return advance;
    }

    public void setAdvance(Double advance) {
        this.advance = advance;
    }

    public Boolean getAdvanceOnly() {
        return advanceOnly;
    }

    public void setAdvanceOnly(Boolean advanceOnly) {
        this.advanceOnly = advanceOnly;
    }
}
