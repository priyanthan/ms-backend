package com.eventspace.dao;

import com.eventspace.domain.CorporatePromo;

public interface CorporatePromoDao extends BaseDao<CorporatePromo> {
}
