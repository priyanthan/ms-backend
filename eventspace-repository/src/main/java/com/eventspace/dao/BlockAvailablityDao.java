package com.eventspace.dao;

import com.eventspace.domain.BlockAvailablity;

import java.sql.Time;

/**
 * Created by Auxenta on 6/29/2017.
 */
public interface BlockAvailablityDao extends BaseDao<BlockAvailablity> {


    BlockAvailablity findTheAvailablity(Integer spaceId,Integer day,Time fromTime,Time toTime);
}
