package com.eventspace.dao.Impl;

import com.eventspace.dao.SpacePromotionDao;
import com.eventspace.domain.SpacePromotion;
import com.eventspace.enumeration.BooleanEnum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class SpacePromotionDaoImpl extends BaseDaoImpl<SpacePromotion> implements SpacePromotionDao {


    private static final Logger LOGGER = LoggerFactory.getLogger(SpacePromotionDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SpacePromotion getSpacePromotion(Integer spaceId,Date startDate,Date endDate) {
        LOGGER.info("getPromoDetailsOfHost-----> get call");
        Session currentSession = sessionFactory.getCurrentSession();
        return (SpacePromotion) currentSession.createCriteria(SpacePromotion.class)
                .add(Restrictions.eq("space.id", spaceId))
                .add(Restrictions.eq("active", BooleanEnum.TRUE.value()))
                .add(Restrictions.le("startDate",startDate))
                .add(Restrictions.ge("endDate",endDate))
                .uniqueResult();
    }
}
